import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,Linking,TouchableOpacity
} from 'react-native'
import Hyperlink from 'react-native-hyperlink';
import { CheckBox, Button } from 'react-native-elements';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';

type Props = {
    title: string,
    onButton: Function,
}
export default Table = (self,data,id) => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                {
                    (id==0) && <Text style={styles.nguoitg}>Cá nhân</Text>
                }
                {
                    (id==1) && <Text style={styles.nguoitg}>Người tham gia</Text>
                }
                <Text style={styles.nhiemvu}>Nhiệm vụ</Text>
                <Text style={styles.minhchung}>Ghi chú</Text>
                <Text style={styles.danhgia}>Xác nhận</Text>
            </View>
            {
                data.map((value, index) => {
                    return (
                        <View style={styles.row}>
                            <View style={styles.cntPersion}>
                                <Text style={styles.text}>{value.hoTen}</Text>
                                <Text style={styles.text}>{value.chucVu}</Text>
                            </View>
                            <View style={styles.cntNhiemvu}>
                                <Text style={styles.text}>{value.nhiemVu}</Text>
                            </View>
                            <View style={styles.cntMinhChung}>
                                <Hyperlink
                                    linkStyle={{ color: colors.bgBlue, textDecorationLine: 'underline' }}
                                    onPress={(url, text) => Linking.openURL(url)}>
                                    <Text style={styles.text}>{value.minhChung}</Text>
                                </Hyperlink>
                                {
                                    self.state.note[index+id].text != "" &&
                                    <View style={{ flex: 0, marginVertical: 8 }}>
                                        <Text style={[styles.text, { color: "red", fontWeight: 'bold' }]}>Ghi chú thêm:</Text>
                                        <Text style={styles.text}>{self.state.note[index+id].text}</Text>
                                    </View>
                                }
                                {
                                    self.state.note[index+id].image != "" &&
                                    <View style={{ flex: 0, marginVertical: 8 }}>
                                        <Text style={[styles.text, { color: "red", fontWeight: 'bold' }]}>{"Tệp đính kèm: "}</Text>
                                        <Text style={[styles.text, { color: colors.bgBlue }]}>{
                                            self.state.note[index+id].image.fileName
                                        }</Text>
                                    </View>
                                }
                                <TouchableOpacity
                                    style={styles.btnNote}
                                    onPress={() => {
                                        self.setState({ idNote: index+id })
                                        self.onChangeScreen("ADD_NOTE")
                                    }}
                                >
                                    <Text
                                        style={[styles.text, { color: "red", fontWeight: 'bold' }]}>
                                        Thêm ghi chú
                                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.cntDanhGia}>
                                <CheckBox
                                    checkedColor={colors.green}
                                    checked={self.state.checked[index+id].present}
                                    onPress={() => self.changeCheck(index+id)}
                                />
                            </View>
                        </View>
                    )
                })
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems:'center',
        borderRadius:3,
        marginBottom: 10*STYLES.heightScreen/896,
      },
      row:{
         flex:0,
         width:STYLES.widthScreen*0.95,
         backgroundColor:'#fff',
         //marginHorizontal:STYLES.widthScreen*0.025,
         flexDirection:'row',
      },
      header:{
         width:STYLES.widthScreen*0.95,
         backgroundColor:'#fff',
         //marginHorizontal:STYLES.widthScreen*0.025,
         flexDirection:'row'
      },
      cntViewPDF:{
        flex:1,
      },
      btnNote:{
         flex:0,
         marginTop:10,
      },
      cntMain: {
        flex:1,
        backgroundColor: colors.lightGrayEEE,
       //  alignItems:'center',
      },
      cntPersion:{
       width: STYLES.widthScreen*0.3,
       minHeight:0.35*STYLES.heightScreen,
       paddingHorizontal:5*STYLES.widthScreen/360,
       borderColor: colors.grayNew,
       borderWidth:0.5,
       justifyContent:'center',
       alignItems:'center',
     },
     cntNhiemvu:{
       width: STYLES.widthScreen*0.25,
       minHeight:0.35*STYLES.heightScreen,
       paddingHorizontal:5*STYLES.widthScreen/360,
       borderColor: colors.grayNew,
       borderWidth:0.5,
       justifyContent:'center',
       alignItems:'center',
     },
     cntMinhChung:{
       width: STYLES.widthScreen*0.25,
       minHeight:0.35*STYLES.heightScreen,
       paddingHorizontal:5*STYLES.widthScreen/360,
       borderColor: colors.grayNew,
       borderWidth:0.5,
       justifyContent:'center',
       alignItems:'center',
     },
     cntDanhGia:{
       width: STYLES.widthScreen*0.15,
       minHeight:0.35*STYLES.heightScreen,
       paddingHorizontal:5*STYLES.widthScreen/360,
       borderColor: colors.grayNew,
       borderWidth:0.5,
       justifyContent:'center',
       alignItems:'center',
     },
      nguoitg:{
       color: colors.colorBlack,
       fontSize: STYLES.fontSizeNormalText,
       fontFamily: 'Roboto-Regular',
       width: STYLES.widthScreen*0.3,
       textAlign:'center',
       textAlignVertical: 'center',
       borderColor: colors.grayNew,
       borderWidth:0.5,
       fontWeight:'bold',
      }, 
      text:{
       color: colors.colorBlack,
       fontSize: STYLES.fontSizeNormalText,
       fontFamily: 'Roboto-Regular',
      },
      nhiemvu:{
       color: colors.colorBlack,
       fontSize: STYLES.fontSizeNormalText,
       fontFamily: 'Roboto-Regular',
       width: STYLES.widthScreen*0.25,
       textAlign:'center',
       textAlignVertical: 'center',
       borderColor: colors.grayNew,
       borderWidth:0.5,
       fontWeight:'bold',
      },
      minhchung:{
       color: colors.colorBlack,
       fontSize: STYLES.fontSizeNormalText,
       fontFamily: 'Roboto-Regular',
       width: STYLES.widthScreen*0.25,
       textAlign:'center',
       textAlignVertical: 'center',
       borderColor: colors.grayNew,
       borderWidth:0.5,
       fontWeight:'bold',
      },
      danhgia:{
       color: colors.colorBlack,
       fontSize: STYLES.fontSizeNormalText,
       fontFamily: 'Roboto-Regular',
       width: STYLES.widthScreen*0.15,
       textAlign:'center',
       textAlignVertical: 'center',
       borderColor: colors.grayNew,
       borderWidth:0.5,
       fontWeight:'bold',
      },
      cntPDF: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex:1,
        width:STYLES.widthScreen,
    },
    btnSendList:{
     width:STYLES.widthScreen,
     justifyContent:'center',
     alignItems:'center',
     marginVertical: 20*STYLES.heightScreen/640,
 },
 buttonSendList:{
   width:0.55*STYLES.widthScreen,
   elevation:3,
   borderRadius: 0.1*STYLES.widthScreen,
 }
})

