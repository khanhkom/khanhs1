import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
  title:string,
  onChange:Function,
  stateInput:any,
}
export default NoteForAddEvent = (props: any) => {
    const {title,onChange,stateInput}= props;  
    return (
     <View style={styles.container}>
           <TextInput
                    style={styles.textEmail}
                    placeholder={title}
                    returnKeyType='next'
                    numberOfLines={5}
                    underlineColorAndroid={'transparent'}
                    onChangeText={(password) => onChange(password) }
                    value={stateInput}
            />
      </View>
    )
}
const styles = StyleSheet.create({
      container: {
       flex:0,
       marginTop: 8*STYLES.heightScreen/640,
       borderRadius: 15*STYLES.heightScreen/640,
        shadowColor: '#8B8989',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 3,
        shadowRadius: 2,
        elevation: 2,
      },
      textEmail: {
        paddingTop: 25*STYLES.heightScreen/640,
        paddingLeft: 20*STYLES.widthScreen/360,
        width: 0.835*STYLES.widthScreen,
        flexWrap: 'wrap',
        height:150*STYLES.heightScreen/640,
        color: Colors.colorBlack,
        //textAlign: "center",
        textAlignVertical: "top",
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
      },
})

