import React from 'react'
import {
   View, StyleSheet,
} from 'react-native'


type Props = {
  width:string,
  height:string,
  color:Function,
}
export default Dot = (props: any) => {
    const {width,height,color}= props;  
    return (
        <View style={{
            width,
            height,
            backgroundColor:color,
            borderRadius:height*0.8
        }}>
        
      </View>
    )
}
const styles = StyleSheet.create({
    
})

