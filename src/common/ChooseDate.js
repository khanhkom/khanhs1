import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';
import DatePicker from 'react-native-datepicker';

type Props = {
    onChange: Function,
    stateDate: any,
}
export default ChooseDate = (props: any) => {
    const { onChange, stateDate } = props;
    return (
        <DatePicker
            style={{ width: 200 * STYLES.widthScreen / 360 }}
            date={stateDate}
            mode="datetime"
            placeholder="Chọn thời gian"
            format="YYYY-MM-DD HH:mm"
            minDate="1998-01-01"
            maxDate="2050-12-01"
            confirmBtnText="Xác nhận"
            cancelBtnText="Huỷ"
            onDateChange={(date) => {
                onChange(date)
            }}
        />
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

