
import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity,KeyboardAvoidingView,Keyboard
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
    title:string,
    onChange:Function,
    stateInput:Function,
    styleTextInput:any,
}
export default TextInputNew = (props: any) => {
    const {title,onChange,stateInput,styleTextInput}= props;  
    var colorSend = (stateInput !== '') ? '#16C2FA' : Colors.white;
    var colorTextSend = (stateInput !== '') ? Colors.white : Colors.gray;
    return (
      <View style={[styles.flex_1,styleTextInput]}>
          <TextInput
            ref={ref => this.inputText=ref}
            style={styles.textEmail}
            placeholder={title}
            placeholderTextColor={Colors.gray}
            returnKeyType='next'
            numberOfLines={3}
            multiline={true}
            underlineColorAndroid={'transparent'}
            onChangeText={(text) => {
             onChange(text)
            }}
            value={stateInput}
          />
          <TouchableOpacity
            disabled={(stateInput === '')}
            onPress={() => {
               Keyboard.dismiss();
               this.inputText.clear();
            }}
          >
            <Text style={[styles.textSend, { color: colorTextSend, backgroundColor: colorSend }]}>OK</Text>
          </TouchableOpacity>
      </View>
    )
}
const styles = StyleSheet.create({
    cntMess: {
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      ctn:{
        flex: 0,
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'white'
        // borderTopWidth: 0.5,
      },
      text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeChatbot,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
      textSend: {
        color: Colors.gray,
        fontWeight: 'bold',
        fontSize: STYLES.fontSizeChatbot,
        fontFamily: 'Roboto-Regular',
        borderWidth: 0.3,
        borderRadius: 3,
        borderColor: Colors.gray,
        padding: 3,
        textAlign: "center",
        textAlignVertical: "center"
      },
      iconMicro: {
        width: 30 / 360 * STYLES.widthScreen,
        height: 30 / 360 * STYLES.widthScreen,
        marginRight: 5,
      },
      textEmail: {
        width: STYLES.widthScreen * 0.85,
        maxHeight: (95 * STYLES.heightScreen / 830) * 0.85,
        paddingTop:5,
        paddingBottom:5,
        flex:0,
        paddingLeft: STYLES.widthScreen * 0.05,
        fontSize: STYLES.fontSizeText,
        color: Colors.colorBlack,
        fontFamily: 'Roboto-Regular',
      },
      flex_1: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 0.5,
        position: 'absolute',
        width:STYLES.widthScreen,
        backgroundColor:'white',
      },
})

