
import React, {Component} from 'react';
import {
View,
Text,TouchableOpacity
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../config/Colors';
import STYLES from '../config/styles.config';

export default class HeaderMail extends Component {
    render() {
        const {title,onButtonLeft,onButtonRight}=this.props;
        return (
            <View style={{height: 70 * STYLES.heightScreen / 640, 
                backgroundColor: Colors.colorPink, paddingTop: 20}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', padding: 12}}>
                   <TouchableOpacity
                        onPress={()=>onButtonLeft()}
                        style={{
                            width:55 * STYLES.heightScreen / 640,
                            flex:0,
                        }}
                   >
                        <Ionicons name="ios-arrow-back" size={24} color="white" />
                   </TouchableOpacity> 
                    <Text style={{color: 'white', fontSize: 18, fontWeight: '500'}}>{title}</Text>
                    <TouchableOpacity
                        onPress={()=>onButtonRight()}
                        style={{
                            alignItems:'flex-end',
                            width:55 * STYLES.heightScreen / 640,
                        }}
                   >
                    <Ionicons name="md-search" size={24} color="white" />
                    </TouchableOpacity> 
                </View>
            </View>
        )
    }
}