import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,Alert
} from 'react-native'
import { CheckBox ,Button} from 'react-native-elements'
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';
import {BAN_DA_HOAN_THANH_XONG_BAI_HOC} from '../config/default';

type Props = {
    content: Function,
    onResult: any,
    id: any,
}
export default class CheckBoxForTest extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            checked: [false, false, false, false]
        }
    }
    changeCheck = (idAnswer) => {
        let {content,onResult,id} = this.props;
        let checked = this.state.checked;
        checked[idAnswer] = !checked[idAnswer];
        let arr=[];
        checked.map((value,index)=>{
                if (value==true) arr.push(content.answer[index].key);
        })
        let res={
             idExercises:id,
             answer:arr
        }
        onResult(id,res);
        this.setState({ checked })

    }
    render() {
        const { content, onResult, id } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.textQuestion}>
                    Câu {id + 1} : {content.question}
                </Text>
                <View style={styles.cntCheckBox}>
                    {
                        content.answer.map((value, index) => {
                            return (
                                <CheckBox
                                    fontFamily={'Roboto-Regular'}
                                    checkedColor={Colors.green}
                                    textStyle={styles.text}
                                    title={value.content}
                                    checked={this.state.checked[index]}
                                    onPress={() => this.changeCheck(index)}
                                />
                            )
                        })
                    }
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen,
        paddingHorizontal: 20 * STYLES.widthScreen / 360,
    },
    cntCheckBox: {
        flex: 0,
    },
    textQuestion: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        flexWrap: 'wrap',
        fontWeight: 'bold',
    },
    text: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        fontWeight: 'normal',
    },
})

