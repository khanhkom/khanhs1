import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity, Dimensions
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';


const { width, height } = Dimensions.get('window');
var widthDrawer = (height / width > 1.5) ? width : 41 * width / 68;
type Props = {
    linkImage: string,
    text: string,
    onButton: Function,
}
export default ItemDrawer = (props: any) => {
    const { text, linkImage, onButton, children } = props;
    return (
        <View style={{ flex: 0 }}>
            <TouchableOpacity
                style={styles.viewSample}
                onPress={() => onButton()}
            >
                {
                    linkImage!='' && <Image source={linkImage} style={styles.imageSample} />
                }
                <Text style={styles.textSample}>
                    {text}
                </Text>
                {children}
            </TouchableOpacity>
            <View style={styles.viewLine} />
        </View>
    )
}
const styles = StyleSheet.create({
    viewSample: {
        height: STYLES.heightHeader * 0.8,
        width: widthDrawer * 400 / 750,
        flexDirection: "row", alignItems: "center"
    },
    viewLine: {
        height: 1,
        width: widthDrawer * 400 / 750,
        backgroundColor: Colors.white
    },
    imageSample: {
        width: 0 * widthDrawer / 465, height: 0 * widthDrawer / 465
    },
    textSample: {
        marginLeft: 25 * widthDrawer / 750,
        color: Colors.yellowText,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily:'Roboto-Medium'
        //width: widthDrawer * 400 / 750 - 20 * widthDrawer / 465 * 0.8,
    },
})

