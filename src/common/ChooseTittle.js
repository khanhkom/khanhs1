import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,TextInput
} from 'react-native'
//import
import RNPickerSelect from 'react-native-picker-select';
import Ionicons from 'react-native-vector-icons/Ionicons';

//import
import {
    ARR_TITLE,
    ARR_OF_TITLE,  
  } from '../config/default';

import STYLES from '../config/styles.config';
import colors from '../config/Colors';

export default ChooseDate = (self: any) => {
    return (
        <View style={styles.cntTheme}>
                <Ionicons name={ARR_TITLE[0].image} size={20 * STYLES.heightScreen / 640} color={colors.lightBlack} />
                {self.state.onDefault && <RNPickerSelect
                  underline={false}
                  ref='3'
                  placeholder={{
                    label: 'Chọn tiêu đề',
                    value: "Chọn tiêu đề",
                    color: colors.lightGray
                  }}
                  items={ARR_OF_TITLE}
                  onValueChange={async (value: any) => {
                    if (value == 'Khác') {
                      await self.setState({ onDefault: false, theme: '' })
                      await self.refs["1"].focus();
                    }
                    else {
                        self.setState({
                        output: value, theme: value,
                      })
                    };
                  }}
                  style={{ ...picker }}
                  value={self.state.output}
                />}
                {!self.state.onDefault && <TextInput
                  ref="1"
                  style={styles.textEmail}
                  placeholder={ARR_TITLE[0].text}
                  returnKeyType='next'
                  underlineColorAndroid={'transparent'}
                  onChangeText={(password) => self.changeTheme(password)}
                  value={self.state.theme}
                />}
              </View>
    )
}
const styles = StyleSheet.create({
    cntTheme: {
        marginTop: 8 * STYLES.heightScreen / 640,
        height: 40 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen * 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#E2E2E2',
        borderBottomWidth: 1,
        flexDirection: 'row',
      },
      textEmail: {
        marginLeft: 10 * STYLES.widthScreen / 360,
        fontSize: STYLES.fontSizeText,
        color: colors.colorBlack,
        width: STYLES.widthScreen * 0.8,
        flexWrap: 'wrap',
        fontFamily: 'Roboto-Regular',
      }
})

export const picker = StyleSheet.create({
    viewContainer: {
      width: STYLES.widthScreen * 0.8,
      marginLeft: 10 * STYLES.widthScreen / 360,
      height: 40 * STYLES.heightScreen / 640,
      fontSize: STYLES.fontSizeText,
      justifyContent: 'center',
      //alignItems:'center'
    }
  })