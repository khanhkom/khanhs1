import React, { Component } from 'react'
import {
    Text, View, StyleSheet ,Switch
} from 'react-native'

import STYLES from '../config/styles.config';
import colors from '../config/Colors';
import { mapToYearMonthDay } from '../config/Function';
export default ViewSwitch = (self: any) => {
    return (
      <View style={styles.viewCangay}>
      <Text style={styles.text}>{'Cả ngày'}</Text>
      <Switch
        onValueChange={(value) => {
          self.setState({ fullDay: value })
          if (value == true) {
            let dateCurrent = new Date();
            if (self.state.timeEnd == '' && self.state.timeBegin == '')
            self.setState({
                timeBegin: dateCurrent.getFullYear() + '-' + (dateCurrent.getMonth() + 1) + '-'
                  + dateCurrent.getDate() + ' 00:00',
                timeEnd: dateCurrent.getFullYear() + '-' + (dateCurrent.getMonth() + 1) + '-'
                  + dateCurrent.getDate() + ' 23:59'
              })
            else {
              let dataTime = self.state.timeEnd;
              if (self.state.timeBegin != '') dataTime = self.state.timeBegin;
              let timeBD = mapToYearMonthDay(dataTime);
              self.setState({
                timeEnd: timeBD.year + "-" + timeBD.month + "-" + timeBD.day + ' 23:59',
                timeBegin: timeBD.year + "-" + timeBD.month + "-" + timeBD.day + ' 00:00',
              })
            }
          }
        }}
        value={self.state.fullDay}
      />
    </View>
    )
}
const styles = StyleSheet.create({
  text: {
    color: colors.colorBlack,
    fontSize: STYLES.fontSizeText,
    textAlign: "center",
    textAlignVertical: "center",
    fontFamily: 'Roboto-Regular',
  },
  viewCangay: {
    marginTop: 8 * STYLES.heightScreen / 640,
    height: 40 * STYLES.heightScreen / 640,
    width: STYLES.widthScreen * 0.9,
    paddingHorizontal: STYLES.widthScreen * 0.025,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: '#E2E2E2',
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
})