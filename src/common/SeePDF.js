
/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView
} from 'react-native'
import Pdf from 'react-native-pdf';

//import config
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

//import common
import HeaderReal from './HeaderReal';
type Props = {
    sourcePDF: string,
    timesRead:any,
}
export default class SeePDF extends Component<Props> {
    render() {
        let sourcePDF = this.props.navigation.getParam('content').sourcePDF;
        let title = this.props.navigation.getParam('content').title;
        return (
            <View style={styles.cntViewPDF}>
                <HeaderReal
                    title={title}
                    onButton={() => this.props.navigation.navigate('PartyDocument')} />
                <View style={styles.cntPDF}>
                    <Pdf
                        source={sourcePDF}
                        onLoadComplete={(numberOfPages, filePath) => {
                           
                        }}
                        onPageChanged={(page, numberOfPages) => {
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error) => {
                            console.log(error);
                        }}
                        style={styles.pdf} />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
      flex:1,
      paddingBottom: 20*STYLES.heightScreen/640,
    },
    cntViewPDF:{
      flex:1,
    },
    cntMain: {
      flex:1,
      alignItems:'center',
    },
    cntPDF: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
  },
  pdf: {
      flex:1,
      width:STYLES.widthScreen,
  }
})