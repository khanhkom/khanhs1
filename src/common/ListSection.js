import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity
} from 'react-native'

import { List } from 'react-native-paper';

type Props = {
listDocument:any,
 title:any,
 navigation:any,
}
export default ListSection = (props: any) => {
    const {title,listDocument,navigation}= props;  
    return (
        <List.Section title={title}>
        {
            listDocument.map((value, index) => {
                return (
                    <List.Accordion
                        titleStyle={{color:"red"}}
                        title={value.titleBig}
                        left={props => <List.Icon {...props} icon="folder" />}
                    >
                        {
                            value.describe.map((val, id) => {
                                return (
                                    <List.Item
                                        title={val.title}
                                        onPress={() => {
                                            navigation.navigate("SeePDF",{
                                                content:{
                                                    title:'Chi tiết',
                                                    sourcePDF:val.sourcePDF
                                                }
                                            })
                                        }}
                                        left={props => <List.Icon {...props} icon="chevron-right" />}
                                    />
                                )
                            })
                        }
                    </List.Accordion>
                )
            })
        }

    </List.Section>  
       
    )
}
