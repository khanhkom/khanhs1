import React from 'react'
import {
  Text, View, StyleSheet, Image,TextInput
} from 'react-native'

//import
import STYLES from '../config/styles.config';

type Props = {
  postion: string,
  password:string,
  username: string,
  onChangeUsername:Function,
  onChangePassword:Function,
}
export default FrameSignIn = (props: any) => {
  const { postion, username ,password, onChangeUsername ,onChangePassword } = props;
  return (
    <View style={[styles.frameSignIn, { top: postion }]}>
      <View style={styles.viewUser}>
        <Text style={styles.text}>
          Tài khoản
            </Text>
        <TextInput
          style={styles.textEmail}
          autoCapitalize = 'none'
          placeholder='Nhập tài khoản'
          returnKeyType='next'
          underlineColorAndroid={'transparent'}
          onChangeText={(username) => onChangeUsername(username)}
          value={username}
        />
      </View>
      <View style={styles.viewUser}>
        <Text style={styles.text}>
          Mật khẩu
            </Text>
        <TextInput
          style={styles.textEmail}
          placeholder='Nhập mật khẩu'
          returnKeyType='next'
          autoCapitalize = 'none'
          secureTextEntry={true}
          underlineColorAndroid={'transparent'}
          onChangeText={(password) =>  onChangePassword(password)}
          value={password}
        />
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  frameSignIn:{
    justifyContent: 'center',
    alignItems: 'center',
    width:298*STYLES.widthScreen/360,
    height:206*STYLES.heightScreen/640,
    position: 'absolute',
    backgroundColor:'white',
    borderRadius: 33*STYLES.widthScreen/360,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    left: 31*STYLES.widthScreen/360,
    top: 267*STYLES.heightScreen/640,
    shadowColor: '#8B8989',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 3,
    shadowRadius: 2,
    elevation: 3,
  },
  viewUser:{
    width:223*STYLES.widthScreen/360,
    height:60*STYLES.heightScreen/640,
    borderBottomWidth: 1,
    borderColor: '#8B8989',
    justifyContent: 'center',
    marginBottom:5*STYLES.heightScreen/640,
    //alignItems: 'center',
  },
  textEmail:{
    width:223*STYLES.widthScreen/360,
    height:40*STYLES.heightScreen/640,
    //textAlign:'center',
    //textAlignVertical:'center',
  }
})

