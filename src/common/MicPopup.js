
import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
} from 'react-native';
import LottieView from 'lottie-react-native';
import PopupDialog from 'react-native-popup-dialog';

import STYLES from '../config/styles.config';
import colors from '../config/Colors';
import IMAGE from '../assets/image';
type Props = {
  modalVisible: boolean,
  speakValue: string,
  onSetModal:Function,
  onSetVoice:Function
}
export default MicPoup = (props: any) => {
    return (
      <PopupDialog
        containerStyle={styles.popup}
        show={props.modalVisible}
        width={STYLES.heightScreen * (243 / 640)}
        height={STYLES.widthScreen * (217 / 360)}
        dialogStyle={styles.popup}
        onDismissed={()=> {
          props.onSetVoice();
          props.onSetModal();
          }}
        dismissOnTouchOutside={true}
        dismissOnHardwareBackPress={true}
      >
        <View style={styles.popup}>
          <View style={{ margin: 5 }}></View>
          <Text style={styles.textSpeak}>{props.speakValue}</Text>
          <LottieView
              source={IMAGE.microphone}
              autoPlay
              style={styles.microPhone}
              loop
            />
        </View>
      </PopupDialog>
    )
}

const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    zIndex: 10,
    elevation: 10,
    marginTop: STYLES.heightHeader,
  },
  popup: {
    borderRadius: 0.02 * STYLES.widthScreen,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  containerSpeak: {
    marginTop: 22,
    height: STYLES.heightScreen * (243 / 640),
    width: STYLES.widthScreen * (217 / 360),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSpeak: {
    textAlign: 'center',
    margin: 5,
    fontWeight: 'bold',
    color: 'black',
    fontSize: STYLES.fontSizeText,
    fontFamily: 'Roboto-Regular',
  },
  microPhone: {
    height: STYLES.widthScreen * (217 / 360) / 2,
    width: STYLES.widthScreen * (217 / 360) / 2,
    alignSelf: 'center',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
})
