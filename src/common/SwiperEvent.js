import React, { Component } from 'react'
import {
    View, StyleSheet, Image, TouchableOpacity, ImageBackground, Text
} from 'react-native'
import { Tile, colors } from 'react-native-elements';
import Swiper from 'react-native-swiper';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
    data: any,
    navigation: Function,
}
export default SwiperEvent = (props: any) => {
    const { data, navigation } = props;
    return (
        <View style={styles.container}>
            <View style={{
                backgroundColor: Colors.colorPink,
                width: STYLES.widthScreen,
                height: STYLES.heightScreen * 0.18,
                position: 'absolute',
                top: 0 * STYLES.heightScreen / 896,
            }}>

            </View>
            <Swiper
                style={styles.wrapper}
                autoplay
                showsButtons={true}>
                {
                    data.map((value, index) => {
                        return (
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Details', { content: value })}
                            >
                                <ImageBackground
                                    style={styles.cntEvent}
                                    source={{ uri: value.sourceImage }}
                                    imageStyle={{ borderRadius: 5 * STYLES.heightScreen / 896 }}
                                    resizeMode={'stretch'}
                                >
                                    <Text
                                        numberOfLines={2}
                                        style={styles.textTitle}>{value.title}</Text>
                                    <View style={styles.cntNote}>
                                        <Text style={styles.textAuthor}>{value.author}</Text>
                                        <Text style={styles.textTime}>{value.time}</Text>
                                    </View>
                                </ImageBackground>
                            </TouchableOpacity>
                        )
                    })
                }
            </Swiper>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        // marginTop: 15*STYLES.heightScreen/896,
        marginBottom: 49 * STYLES.heightScreen / 896,
        height: 300 * STYLES.heightScreen / 896,
        width: STYLES.widthScreen,
        paddingHorizontal: 15 * STYLES.widthScreen / 414,
    },
    text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
    wrapper: {
        height: 300 * STYLES.heightScreen / 896,
        width: 384 * STYLES.widthScreen / 414,
    },
    cntEvent: {
        height: 300 * STYLES.heightScreen / 896,
        width: 384 * STYLES.widthScreen / 414,
        justifyContent: 'center',
        paddingTop: 0.5 * 300 * STYLES.heightScreen / 896,
        paddingHorizontal: 15 * STYLES.widthScreen / 414,
    },
    textTitle: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeChatbot,
        fontFamily: 'Roboto-Regular',
        fontWeight: 'bold',
    },
    textTime: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        fontStyle: 'italic',
    },
    textAuthor: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        padding: 3,
        backgroundColor: Colors.blueNew,
        borderRadius: 2,
        marginRight: 3,
    },
    cntNote: {
        flexDirection: 'row',
        flex: 0,
        //justifyContent: 'center',
        alignItems: 'center',
    }
})

