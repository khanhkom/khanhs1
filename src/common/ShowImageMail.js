import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'
import LinearGradient from "react-native-linear-gradient";
import Ionicons from "react-native-vector-icons/Ionicons";
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';

type Props = {
    linkImage: any,
    onDelete:Function,
}
export default ShowImageMail = (props: any) => {
    const { onDelete, linkImage } = props;
    return (
        <View
            style={styles.imagePrescriptionContainer}
        >
            <Image
                source={linkImage}
                style={styles.imagePrescription}
            />
            <LinearGradient
                style={styles.linearGradient}
                colors={["#212426", "transparent"]}
            />
            <TouchableOpacity
                style={styles.deletePrescription}
                onPress={() => onDelete()}
            >
                <Ionicons name="ios-close-circle-outline" size={25} color="white" />
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    imagePrescriptionContainer: {
		width: STYLES.widthScreen*0.8,
        height: 0.35 * STYLES.heightScreen,
		marginRight: 10,
		position: "relative"
	},
	linearGradient: {
		width: STYLES.widthScreen*0.8,
		height: 0.35 * STYLES.heightScreen / 2,
		borderTopLeftRadius: 4,
		borderTopRightRadius: 4,
		position: "absolute"
	},
	imagePrescription: {
		width: STYLES.widthScreen*0.8,
        height: 0.35 * STYLES.heightScreen,
		borderColor: "white",
		borderRadius: 4,
		borderWidth: 1
    },
    deletePrescription: {
		position: "absolute",
		top: 0,
		right: 5
	}
})

