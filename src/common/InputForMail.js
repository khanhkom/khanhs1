import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';

type Props = {
  content:string,
  title:string,
  onChange:any,
}
export default InputForMail = (props: any) => {
    const {content,title,onChange}= props;  
    return (
    <View style={styles.cntUserSend}>
        <Text style={styles.textUser}>
            {title}
        </Text>  
        <TextInput
                style={styles.textEmail}
                returnKeyType='next'
                multiline={true}
                placeholderTextColor={colors.grayMail}
                underlineColorAndroid={'transparent'}
                onChangeText={(value) => onChange(value) }
                value={content}
        />
  </View>
    )
}
const styles = StyleSheet.create({
    cntUserSend:{
        width:STYLES.widthScreen,
        paddingLeft: 0.05*STYLES.widthScreen,
        flex:0,
        alignItems: 'center',
        marginTop: 8 * STYLES.heightScreen / 640,
         borderBottomColor: '#E2E2E2',
         borderBottomWidth: 1,
         flexDirection: 'row',
    },
    textEmail:{
      width:0.7*STYLES.widthScreen,
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    textUser:{
      fontWeight: '400',
      fontSize: STYLES.fontSizeText,
      marginRight: 20*STYLES.widthScreen/360,
    }
})
