import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,TextInput
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
type Props = {
    onChange: Function,
    stateInput: string,
    linkImage: string,
    title:string,
}
export default ViewTextInput = (props: any) => {
        const { onChange, stateInput ,linkImage,title} = props;
        return (
            <View style={styles.container}>
                <Ionicons name={linkImage} size={20 * STYLES.heightScreen / 640} color={colors.lightBlack} />
                <TextInput
                    style={styles.textEmail}
                    placeholder={title}
                    returnKeyType='next'
                    underlineColorAndroid={'transparent'}
                    onChangeText={(password) => onChange(password) }
                    value={stateInput}
                />
            </View>
        )
}
const styles = StyleSheet.create({
    container: {
        marginTop: 8 * STYLES.heightScreen / 640,
        height: 40 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen * 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#E2E2E2',
        borderBottomWidth: 1,
        flexDirection: 'row',
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
    textEmail:{
        marginLeft: 10*STYLES.widthScreen/360,
        fontSize: STYLES.fontSizeText,
        color: colors.colorBlack,
        width:STYLES.widthScreen*0.8,
        flexWrap: 'wrap',
        fontFamily: 'Roboto-Regular',
    }
})

