import React, { Component } from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity
} from 'react-native'
//import
import STYLES from '../config/styles.config';

type Props = {
 title:any,
 onButton:any,
}
export default Skip = (props: any) => {
    const {title,onButton}= props;  
    return (
        <View style={styles.viewSkip}>
        <TouchableOpacity 
            onPress={() => onButton()}
        >
            <Text style={[styles.text,{fontWeight:'bold'}]}>
                  {title}
            </Text>
        </TouchableOpacity>
    </View>
       
    )
}
const styles = StyleSheet.create({
    viewSkip:{
        flex:0,
        position: 'absolute',
        left: 260*STYLES.widthScreen/360,
        top: 550*STYLES.heightScreen/640,
      },
      text: {
        color: 'black',
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
    },
})
