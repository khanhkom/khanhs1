
import React, { Component } from 'react';
import {
    View,
    Text, TouchableOpacity
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../config/Colors';
import STYLES from '../config/styles.config';
import image from '../assets/image'

export default class HeaderMail extends Component {
    render() {
        const { title, onButtonLeft, onButtonRight } = this.props;
        return (
            <View style={{
                height: 46 * STYLES.heightScreen / 640,
                backgroundColor: Colors.colorPink
            }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', padding: 12 }}>
                    
                    <TouchableOpacity
                        onPress={() => onButtonLeft()}
                        style={{
                            width: 55 * STYLES.heightScreen / 640,
                            flex: 0,
                            flexDirection:'row',
                            alignItems:'center'
                        }}
                    >
                        <Image source={image.iconCheck} style={{width:12/16*STYLES.heightScreen/640,height:16*STYLES.heightScreen/640}} />
                        <Text style={{color:Colors.white,fontSize:STYLES.fontSizeNormalText,marginLeft:2}}>Trở về</Text>
                    </TouchableOpacity>
                    <Text style={{ color: 'white', fontSize: 18, fontWeight: '500' }}>{title}</Text>
                    <TouchableOpacity
                        onPress={() => onButtonRight()}
                        style={{
                            alignItems: 'center',
                            width: 55 * STYLES.heightScreen / 640,
                            flexDirection:'row'
                        }}
                    >
                        <Text style={{color:Colors.white,fontSize:STYLES.fontSizeNormalText}}>Xong</Text>
                        <Ionicons name="md-search" size={24} color="white" style={{marginLeft:3}} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}