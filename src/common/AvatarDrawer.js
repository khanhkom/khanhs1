import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity, Dimensions
} from 'react-native'


//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
    onButton: any,
    name: any,
    position: any,
}
const { width, height } = Dimensions.get('window');
var widthDrawer = (height / width > 1.5) ? width : 41 * width / 68;
export default AvatarDrawer = (props: any) => {
    const { position, name, onButton } = props;
    return (
        <View style={{ flex: 0 }}>
            <View style={styles.viewSpace} />
            <TouchableOpacity style={styles.drawerHeader}
                onPress={() => onButton()}
            >
                <View style={[styles.iconAvatar, {
                    borderRadius: 50 * widthDrawer / 450,
                    overflow: 'hidden',
                }]}>
                    <Image
                        style={styles.iconAvatar}
                        source={IMAGE.icon_Chatbot} />
                </View>

                <Text style={styles.textSmartCom}>
                    {name}
                </Text>
                <Text style={styles.textSmartCom}>
                    {position}
                </Text>
                <View style={styles.viewMid} />
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    viewSpace: {
        height: STYLES.heightHeader * 0.8
    },
    drawerHeader: Object.assign(
        STYLES.centerItem,
        { width: '100%', height: 'auto' }
    ),
    iconAvatar: {
        width: 80 * widthDrawer / 450,
        height: 80 * widthDrawer / 450,
        backgroundColor: Colors.colorWhite,
    },
    imageAvatar: {
        height: 200 * STYLES.heightHeader / 120,
        backgroundColor: Colors.colorWhite,
    },
    iconDrawer: {
        width: 25,
        height: 25,
    },
    textSmartCom: {
        color: Colors.yellowText,
        fontSize: STYLES.fontSizeNormalText,
        fontWeight: "bold",
        fontFamily: 'Roboto-Regular',
    },
    viewMid: {
        height: STYLES.heightHeader * 0.5
    },
})
