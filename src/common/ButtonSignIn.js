import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';

type Props = {
  title:string,
  onButton:Function,
}
export default ButtonSignIn = (props: any) => {
    const {title,onButton}= props;  
    return (
        <View style={styles.viewButton}>
        <TouchableOpacity 
              style={styles.button}
              onPress={()=>onButton()}
        >
              <Text style={[styles.text,{color:'white'}]}>
                  {title}
              </Text>
        </TouchableOpacity>
    </View>
    )
}
const styles = StyleSheet.create({
    viewButton:{
        width:140*STYLES.widthScreen/360,
        height:52*STYLES.heightScreen/640,
        position: 'absolute',
        backgroundColor:'white',
        borderRadius: 59*STYLES.widthScreen/360,
        left: 110*STYLES.widthScreen/360,
        top: 444*STYLES.heightScreen/640,
        shadowColor: '#8B8989',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 3,
        shadowRadius: 2,
        elevation: 5,
      },
      button:{
        width:140*STYLES.widthScreen/360,
        height:52*STYLES.heightScreen/640,
        borderRadius: 59*STYLES.widthScreen/360,
        backgroundColor:colors.colorPink,
        justifyContent: 'center',
        alignItems: 'center',
      },
      text: {
        color: 'black',
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
    },
})

