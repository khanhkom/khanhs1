

import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Loading from 'react-native-spinkit';
// styles
import STYLES from '../config/styles.config';
import colors from '../config/Colors';


export const LoadingComponent = (props: any) => {
  if (!props.isLoading) return null;
  return (
    <View style={styles.loadingContainer}>
      <Loading
        isVisible={props.isLoading}
        size={STYLES.heightHeader}
        type={'Circle'}
        color={colors.red} />
    </View>
  )
}
const styles = StyleSheet.create({
  loadingContainer: {
    width: STYLES.widthScreen,
    height: STYLES.heightScreen,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorOverlay,
  },
})