import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, ImageBackground, TouchableOpacity
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { IMPORTANT, NO_IMPORTANT, NORMAL } from '../../config/default'
import Dot from '../Dot';
import { Avatar } from 'react-native-elements';
type Props = {
  content: any,
  navigation: Function,
}
export default TypeNew = (props: any) => {
  const { content, navigation } = props;
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Details', { content: content })}
    >
      <View
        style={styles.container}
      >
        <View style={styles.cntFrameType}>
          <View style={{ flexDirection: 'row' }}>
            <Avatar
              rounded
              source={{
                uri:
                  'http://www.cotcoinox.net/contents_tinta/images/qui-dinh-ve-treo-co-to-quoc-tai-thanh-pho-ho-chi-minh-tp.jpg',
              }}
            />
            <View style={styles.cntTypeTime}>
              <Text style={styles.textTitle}>
                {content.categoryname}
              </Text>
              <Text style={styles.textTime}>
                {content.time}
              </Text>
            </View>
          </View>

          <Dot
            width={20 * STYLES.widthScreen / 360}
            height={20 * STYLES.widthScreen / 360}
            color={
              content.format == IMPORTANT ? Colors.red : (content.format == NORMAL ? Colors.yellowCalendar : Colors.greenCalendar)
            }
          />
        </View>
        <View style={{ flex: 0, width: 0.98 * STYLES.widthScreen, alignItems: 'center' }}>
          <Image
            source={{ uri: content.sourceImage }}
            resizeMode={'stretch'}
            style={styles.cntImage}
          />
        </View>
        <View style={{ flex: 0, width: 0.98 * STYLES.widthScreen, alignItems: 'center' }}>
          <View style={styles.details}>
            <Text style={styles.textContent}>
              {content.title}
            </Text>
            <Text style={styles.textAuthor}>
              {content.author}
            </Text>
          </View>
        </View>

      </View>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  container: {
    width: STYLES.widthScreen * 0.98,
    marginTop: 10 * STYLES.heightScreen / 640,
    borderRadius: 12 * STYLES.heightScreen / 640,
    paddingBottom:12 * STYLES.heightScreen / 640,
    backgroundColor: '#ffff',
    shadowColor: '#ffff',
    shadowOffset: { width: 3, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 2.5,
  },
  textTitle: {
    color: Colors.colorBlack,
    fontSize: STYLES.fontSizeText,
    fontWeight: '500',
    fontFamily: 'Roboto-Medium'
  },
  textTime: {
    color: Colors.gray,
    fontSize: STYLES.fontSizeNormalText,
    fontStyle: 'italic',
    fontFamily: 'Roboto-Medium'
  },
  cntTypeTime: {
    flex: 0,
    paddingLeft: 8 * STYLES.widthScreen / 360,
  },
  details: {
    width: STYLES.widthScreen * 0.93,
    paddingTop:10*STYLES.heightScreen/896,
    backgroundColor: Colors.lightGrayEEE,
    borderBottomLeftRadius: 12 * STYLES.heightScreen / 640,
    borderBottomRightRadius: 12 * STYLES.heightScreen / 640,
    paddingHorizontal: 16 * STYLES.widthScreen / 360,
    paddingBottom: 10 * STYLES.heightScreen / 640,
    shadowColor: '#ffff',
    shadowOffset: { width: 3, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 1,
  },
  cntFrameType: {
    paddingTop: 15 * STYLES.heightScreen / 640,
    paddingBottom: 5 * STYLES.heightScreen / 640,
    paddingLeft: 10 * STYLES.widthScreen / 360,
    paddingRight: 10 * STYLES.widthScreen / 360,
    borderTopLeftRadius: 12 * STYLES.heightScreen / 640,
    borderTopRightRadius: 12 * STYLES.heightScreen / 640,
    width: STYLES.widthScreen * 0.98,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textAuthor: {
    color: Colors.gray,
    fontSize: STYLES.fontSizeText,
    marginTop: 5 * STYLES.heightScreen / 640,
    fontStyle: 'italic',
    fontFamily: 'Roboto-Medium'
  },
  textContent: {
    color: Colors.colorBlack,
    fontSize: STYLES.fontSizeText,
    fontFamily: 'Roboto-Medium'
  },
  cntImage: {
    width: STYLES.widthScreen * 0.93,
    minHeight: STYLES.heightScreen * 0.35,
    marginBottom: 8*STYLES.heightScreen/896,
    flex: 0,
  }

})

