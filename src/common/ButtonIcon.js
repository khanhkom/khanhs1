import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';
import Entypo from 'react-native-vector-icons/Entypo';
import Colors from '../config/Colors';

type Props = {
  onButton:Function,
  title:string,
  nameIcon:string,
}
export default ButtonIcon = (props: any) => {
    const {title,nameIcon,onButton}= props;  
    return (
        <TouchableOpacity 
        onPress={()=> onButton()}
        style={styles.cnt}>
                <Entypo name={nameIcon} size={20 * STYLES.heightScreen / 640} color="#0000" />
                <Text style={styles.text}>
                    {title}
                </Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    cnt:{
        paddingVertical: 3*STYLES.heightScreen/640,
        justifyContent:'center',
        alignItems: 'center',
        width:168*STYLES.widthScreen/360,
        borderColor: Colors.backGray,
        borderWidth: 1,
        flexDirection: 'row',
        borderRadius: 4*STYLES.widthScreen/360,
    },
    text:{
        marginLeft: 5*STYLES.widthScreen/360,
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
    }
})

