import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar } from 'react-native-elements';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';

type Props = {
    index: any,
    time:any,
    month:any,
    title:any,
    onButton: Function,
    backGround:any,
}
var dataImage = [IMAGE.bookmac, IMAGE.bookmac1, IMAGE.lich1];
export default ItemEvent = (props) => {
    const { index, onButton,time ,title,month,backGround} = props;
    return (
        <TouchableOpacity
            style={[styles.viewDay,{backgroundColor:backGround}]}
            onPress={() => onButton()}
        >
            <View style={styles.viewsChung}>
                <View style={styles.viewImage}>
                    <Avatar
                        size="medium"
                        rounded
                        source={IMAGE.codang}
                    />
                </View>
                <View style={styles.viewTitle}>
                    <Text style={[styles.text, { flexWrap: 'wrap', fontWeight: 'bold', width: 225 * STYLES.widthScreen / 360 }]}>{title}</Text>
                    <View style={styles.viewTime}>
                        <FontAwesome name="calendar" size={13 * STYLES.heightScreen / 640} color={colors.lightBlack} />
                        <Text style={[styles.text, { marginLeft: 6 * STYLES.widthScreen / 360 }]}>{time} Tháng {month}</Text>
                    </View>
                </View>
            </View>
            <Ionicons name="ios-arrow-forward" size={25 * STYLES.widthScreen / 360} color={colors.lightGray} />
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 0,

    },
    ctnScroll: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.lightBlack,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        // textAlign: "center",
        // textAlignVertical: "center"
    },
    viewsChung: {
        flex: 0,
        height: 60 * STYLES.heightScreen / 640,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewDay: {
        marginTop: 10 * STYLES.heightScreen / 640,
        paddingVertical: 10 * STYLES.heightScreen / 896,
        width: STYLES.widthScreen * 0.95,
        paddingHorizontal: 10 * STYLES.widthScreen / 360,
        justifyContent: 'space-between',
        backgroundColor: colors.lightGrayEEE,
        alignItems: 'center',
        flexDirection: 'row',
        // borderWidth: 0.8,
        // borderColor: '#E5E5E5',
        borderRadius: 10 * STYLES.widthScreen / 360,
        shadowColor: colors.colorGray,
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 3,
        shadowRadius: 2,
        elevation: 3,
    },
    viewImage: {
        height: 45 * STYLES.heightScreen / 640,
        width: 45 * STYLES.heightScreen / 640,
        justifyContent: 'center',
        marginRight: 15 * STYLES.widthScreen / 360,
    },
    imageMain: {
        height: 45 * STYLES.heightScreen / 640,
        width: 45 * STYLES.heightScreen / 640,
    },
    viewTime: {
        flex: 0,
        flexDirection: 'row',
        //justifyContent: 'center',
        alignItems: 'center'
    },
    viewTitle: {
        flex: 0,
        marginLeft:10 * STYLES.widthScreen / 360,
        height: 40 * STYLES.heightScreen / 640,
        justifyContent: 'space-between',
    }
})



