import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity,StatusBar
} from 'react-native'

//import from Lib
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
  navigation:Function,
  linkImage:string,
  title:string,
  onButton:Function,
}
export default Header = (props: any) => {
    const {navigation,title,onButton,linkImage}= props;  
    return (
     <View style={styles.header}>
      <StatusBar backgroundColor={Colors.colorPink} />
      <TouchableOpacity
        onPress={()=>navigation.goBack()}
        style={styles.menu}
      >
          <Ionicons name="ios-arrow-back" size={26 * STYLES.heightScreen / 640} color="#fff" />
      </TouchableOpacity>      
        <View style={styles.cntTextInput}>
                <Text style={[styles.text,{fontSize:STYLES.fontSizeLabel,fontWeight:'bold'}]}>
                    {title}
                </Text>
                {/* <Text style={[styles.text,{fontSize:STYLES.fontSizeNormalText,fontStyle:'italic'}]}>
                  A.I Chatbot for digital government!
                </Text> */}
            </View>         
          <TouchableOpacity
            onPress={()=>onButton()}
             style={[styles.menu,{alignItems:'flex-end'}]}
          >
          <Feather name={linkImage} size={21 * STYLES.heightScreen / 640} color="#fff" />
      </TouchableOpacity>   
      </View>
    )
}
const styles = StyleSheet.create({
    header: {
        height: (55 * STYLES.heightScreen / 830),
        width: STYLES.widthScreen,
        flexDirection: 'row',
        backgroundColor: Colors.colorPink,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 5,
        paddingLeft: 5,
        elevation: 4,
      },
      menu:{
        flex: 0,
        paddingLeft: 8,
        width:55 * STYLES.heightScreen / 640
      },
      logo: {
        width: STYLES.widthScreen * (50 / 360),
        height: STYLES.heightScreen * (35 / 640),
      },
      cntlogoStart:{
        width: STYLES.widthScreen * (35 / 360),
        height: STYLES.heightScreen * (35 / 640),
      },
      logoStart: {
        width: STYLES.widthScreen * (35 / 360),
        height: STYLES.heightScreen * (25 / 640),
      },
      text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily:'Roboto-Medium'
      },
      textMini:{
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeMiniText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
      cntTextInput:{
        justifyContent:'center',
        alignItems: 'center',
        flex:0,
        //height: (30 * STYLES.heightScreen / 640),
    },
      textInput:{
        width: STYLES.widthScreen * 0.53,
        height: (30 * STYLES.heightScreen / 640),
        backgroundColor: Colors.header,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        marginLeft: 5,
        //textAlign:'center',
        paddingTop: 0, paddingBottom: 0,
        textAlignVertical:'center',
        color: Colors.white,
      }
})
