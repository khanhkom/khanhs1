import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity
} from 'react-native'

import { Avatar } from 'react-native-elements'

//import
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
 image:any,
 title:any,
 sub_title:any,
 onButton:any,
}
export default ListItem = (props: any) => {
    const {image,title,onButton,sub_title}= props;  
    return (
      <TouchableOpacity
        onPress={()=>onButton()}
        style={styles.container}
      >
            <Avatar
                small
                rounded
                source={{uri: image}}
                onPress={() => console.log("Works!")}
                activeOpacity={0.7}
            />
            <View style={styles.cntText}>
                <Text style={styles.text}>{title}</Text>
                <Text style={styles.text}>{sub_title}</Text>
            </View>
      </TouchableOpacity>      
       
    )
}
const styles = StyleSheet.create({
    container: {
        height: (65 * STYLES.heightScreen / 830),
        width: STYLES.widthScreen,
        paddingLeft:20*STYLES.widthScreen/360,
        flexDirection: 'row',
        borderBottomColor: Colors.backGray,
        borderWidth: 0.6,
        alignItems:'center',
      },
      cntText:{
          marginLeft: 10*STYLES.widthScreen/360,
          justifyContent:'center',
      },
      text:{
          fontSize:STYLES.fontSizeNormalText,
          color:Colors.colorBlack,
          fontFamily: 'Roboto-Regular',
      }
})
