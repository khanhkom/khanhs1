import React, { Component } from 'react'
import {
    Text, View, StyleSheet, ScrollView,TextInput
} from 'react-native'
//import
import {
    ARR_TIME,
    GHI_CHU, CAC_CONG_VIEC_CHINH,
     ARR_END, 
  } from '../config/default';

import STYLES from '../config/styles.config';
import colors from '../config/Colors';

import ViewTextInput from './ViewTextInput';
import ChooseDate from './ChooseDate';
import ViewSwitch from './ViewSwitch';
import ChooseTittle from './ChooseTittle';
import NoteForAddEvent from './NoteForAddEvent';
import LineForAddEvent from './LineForAddEvent';

export default MainAddEvent = (self: any) => {
    return (
        <ScrollView
            style={{ flex: 1 }}>
            <View style={styles.container}>
              {
                ChooseTittle(self)
              }
              <ViewTextInput
                title={'Địa điểm'}
                onChange={self.changeLocation}
                stateInput={self.state.location}
                linkImage={'ios-home'}
              />
              {
                ViewSwitch(self)
              }
              {
                ARR_TIME.map((value, index) => {
                  return (
                    <View
                      key={index}
                      style={styles.viewCangay}>
                      <Text style={styles.text}>{value}</Text>
                      {
                        <ChooseDate
                          onChange={index == 0 ? self.onChangeTimeBegin : self.onChangeTimeEnd}
                          stateDate={index == 0 ? self.state.timeBegin : self.state.timeEnd}
                        />
                      }
                    </View>
                  )
                })
              }
              {
                ARR_END.map((value, index) => {
                  return (
                    <LineForAddEvent
                      key={index}
                      titleFirst={value.titleFirst}
                      titleSecond={self.outLineEnd(index).stateInput} 
                      linkImage={'ios-arrow-forward'}
                      onButton={() => self.onButtonEnd(value, index)}
                    />
                  )
                })
              }
              <View style={styles.viewCangay}>
                <Text style={styles.text}>{GHI_CHU}</Text>
              </View>
              <NoteForAddEvent
                title={CAC_CONG_VIEC_CHINH}
                onChange={self.onChangeNote}
                stateInput={self.state.note}
              />
            </View>
          </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 20 * STYLES.heightScreen / 640
      },
      text: {
          color: colors.colorBlack,
          fontSize: STYLES.fontSizeText,
          fontFamily: 'Roboto-Regular',
          textAlign: "center",
          textAlignVertical: "center"
        },
      textNoBold: {
        marginRight: 10 * STYLES.widthScreen / 640,
        color: colors.lightBlack,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
      viewDay: {
        flex: 0,
        width: 47 * STYLES.widthScreen / 360,
      },
      cntTheme: {
        marginTop: 8 * STYLES.heightScreen / 640,
        height: 40 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen * 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#E2E2E2',
        borderBottomWidth: 1,
        flexDirection: 'row',
      },
      textEmail: {
        marginLeft: 10 * STYLES.widthScreen / 360,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        color: colors.colorBlack,
        width: STYLES.widthScreen * 0.8,
        flexWrap: 'wrap',
      },
      viewCangay: {
          marginTop: 8 * STYLES.heightScreen / 640,
          height: 40 * STYLES.heightScreen / 640,
          width: STYLES.widthScreen * 0.9,
          paddingHorizontal: STYLES.widthScreen * 0.025,
          justifyContent: 'space-between',
          alignItems: 'center',
          borderBottomColor: '#E2E2E2',
          borderBottomWidth: 1,
          flexDirection: 'row',
        },
})
