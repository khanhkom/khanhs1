import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, TextInput, TouchableOpacity,StatusBar
} from 'react-native'

//import from Lib
import Ionicons from 'react-native-vector-icons/Ionicons';

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
  onBack: string,
  onAttach: Function,
  onSend: Function,
}
export default HeaderForEmail = (props: any) => {
  const { onSend, onAttach, onBack } = props;
  return (
    <View style={styles.header}>
      <StatusBar backgroundColor={Colors.white} />
      <TouchableOpacity onPress={() => onBack()}>
        <Ionicons name='ios-arrow-back' color='gray' size={24} />
      </TouchableOpacity>
      <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
        <TouchableOpacity onPress={() => onAttach()}>
          <Ionicons name='md-attach' color='gray' size={24} style={{ paddingRight: 24 }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => onSend()}>
          <Ionicons name='md-send' color='gray' size={24} />
        </TouchableOpacity>

      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  header: {
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: Colors.lightGrayEEE,
    borderBottomWidth: 0.7
  }
})
