import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';

type Props = {
  titleFirst:string,
  titleSecond:string,
  onButton:Function,
  linkImage:string,
}
export default LineForAddEvent = (props: any) => {
    const {titleFirst,titleSecond,linkImage,onButton}= props;  
    return (
        <View style={styles.viewCangay}>
        <Text style={styles.text}>{titleFirst}</Text>
        <TouchableOpacity
          style={{flexDirection:'row',flex:0,
          justifyContent:'center',alignItems:'center'}}
          onPress={() => onButton()}
        >
          <Text style={styles.textNoBold}>{titleSecond}</Text>
          <Ionicons name={linkImage}
          size={15 * STYLES.heightScreen / 640} color = {colors.lightBlack} />
        </TouchableOpacity>
      </View>
    )
}
const styles = StyleSheet.create({
    text: {
        color: colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
      viewCangay: {
        marginTop: 8 * STYLES.heightScreen / 640,
        height: 40 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen * 0.9,
        paddingHorizontal: STYLES.widthScreen * 0.025,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomColor: '#E2E2E2',
        borderBottomWidth: 1,
        flexDirection: 'row',
      },
      textNoBold: {
        marginRight: 10*STYLES.widthScreen/640,
        color: colors.lightBlack,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
      },
})

