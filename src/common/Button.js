import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';

type Props = {
  title:string,
}
export default Button = (props: any) => {
    const {title}= props;  
    return (
     <View style={styles.container}>
            <TouchableOpacity
                onPress={()=>onButton()}
                style={styles.menu}
              >
                    <Text>
                        {title}
                    </Text>
                    <View style={{
                        backgroundColor:Colors.white,
                        height
                    }}>

                    </View>
            </TouchableOpacity>   
      </View>
    )
}
const styles = StyleSheet.create({
      container: {
        flex:1,
      },
      text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
})

