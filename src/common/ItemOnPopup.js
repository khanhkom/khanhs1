import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, TouchableOpacity
} from 'react-native'
import {BoxShadow} from 'react-native-shadow'
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';

type Props = {
  content: string,
  onButton: Function,
}
export default ItemOnPopUp = (props: any) => {
  const { onButton, content, children } = props;
  return (
    <View style={styles.cnt}>
      <TouchableOpacity
        onPress={() => onButton()}
        style={styles.container}>
        {content.title!="" && <View style={{ 
         // elevation: 10, 
          borderRadius:30*STYLES.heightScreen/896,
          backgroundColor:colors.lightGray}}>
          <Image
            resizeMode={'contain'}
            source={content.sourceImage}
            style={styles.imageSample} 
            />
        </View>}
        {
          content.title=="" && <View style={styles.imageSample}></View>
        }
        <Text style={styles.text}>{content.title}</Text>
        {children}
      </TouchableOpacity>
    </View>

  )
}
const styles = StyleSheet.create({
  cnt: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: STYLES.widthScreen*0.3,
  },
  container: {
    //flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  imageSample: {
    width: 66 * STYLES.heightScreen / 896,
    height: 66 * STYLES.heightScreen / 896,
   
  },
  text: {
    marginTop: 5 * STYLES.heightScreen / 640,
    color: 'rgb(40,53,76)',
    fontSize: 13,
    width: 80 * STYLES.widthScreen / 414,
    flexWrap: 'wrap',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: 'Roboto-Regular',
  },
})

