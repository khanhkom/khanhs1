import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity,StatusBar
} from 'react-native'
import { Avatar,Badge } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/Colors';
import Colors from '../config/Colors';

type Props = {
    title: string,
    onButton: Function,
    account: any,
}
export default HeaderHomeNew = (props: any) => {
    const { title, onButton, account } = props;
    let StaffName=account!=undefined ? account.StaffName : "";
    let StaffMobile=account!=undefined ? account.StaffMobile : "";
    return (
        <View style={styles.container}>
             <StatusBar backgroundColor={Colors.colorPink} />
            <View style={styles.cntTitle}>
                {account!=undefined && <Avatar
                    size="medium"
                    rounded
                    source={{ uri: 'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/37011634_10155962444234234_4275637632475594752_n.jpg?_nc_cat=106&_nc_oc=AQkKl_lD3WgQJmWnpdcdDUodjdWJ5pZM_HbzOWHOan9srFW2jcu2oBYoe2cr_XmLsIM&_nc_ht=scontent.fhan2-2.fna&oh=ce583989d704c8b4b74c77830333db01&oe=5D065DDF' }}
                />}
                {account==undefined && <Avatar
                    size="medium"
                    rounded
                    source={IMAGE.icon_Chatbot}
                />}
                {account!=undefined && <View style={{ flex: 0 }}>
                    <Text style={styles.text}>{StaffName}</Text>
                    <Text style={styles.text}>{"Đảng viên"}</Text>
                    <Text style={styles.text}>{"SĐT: "+ StaffMobile}</Text>
                </View>}
            </View>
            <TouchableOpacity>
                {account!=undefined &&<Badge 
                    containerStyle={{ elevation:2,position: 'absolute', top: -4, right: -4 }}
                    status="success" />}
                <Ionicons name="ios-notifications" size={26 * STYLES.heightScreen / 640} color={colors.yellowText} />
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.colorPink,
        width: STYLES.widthScreen,
        height: 100 * STYLES.heightScreen / 896,
        flexDirection: 'row',
        paddingHorizontal: 15 * STYLES.widthScreen / 414,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    cntTitle: {
        flex: 0,
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center'
    },
    text: {
        marginLeft: 15 * STYLES.widthScreen / 414,
        color: Colors.yellowText,
        fontSize: STYLES.fontSizeNormalText,
        // textAlign: "center",
        // textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

