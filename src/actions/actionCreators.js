
export function addMail(arr) {
    return { type: 'ADD_MAIL', arr};
}

export function addEvent(arr) {
    return { type: 'ADD_EVENT', arr};
}
export function setAccount(arr) {
    return { type: 'SET_ACCOUNT', arr};
}
export function setCompleteLesson(id) {
    return { type: 'SET_COMPLETE_LESSSON',id};
}


export const setLessonReducer = (arr) => {
    return { type: 'SET_LESSSON', arr};
}
export const setLesson = (arr) => {
    return (dispatch) => {
        dispatch(setLessonReducer(arr));
    }
}