const defaultState = {
	MailBox: [],
	DataEvent: [],
	Account: {},
	DataExercises: [],
};
export default (state = defaultState, action) => {
	switch (action.type) {
		case 'ADD_MAIL': return {
			...state,
			MailBox: action.arr,
		};
		case 'SET_ACCOUNT': return {
			...state,
			Account: action.arr,
		};
		case 'ADD_EVENT': return {
			...state,
			DataEvent: action.arr,
		};
		case 'SET_LESSSON': return {
			...state,
			DataExercises: action.arr,
		};
		case 'SET_COMPLETE_LESSSON': {
			let arr = state.DataExercises;
			arr.map((value, index) => {
				if (value.idLesson == action.id) {
					arr[index].complete = 1;
				}
			})
			return {
				...state,
				DataExercises: arr,
			};
		}

		default:
			break;
	}
	return state;
};