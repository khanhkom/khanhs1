import IMAGE from '../assets/image';
import STYLES from './styles.config';
import colors from './Colors';

export const NAME_PART1 = 'Part 1: Vocabulary review';
export const NAME_PART2 = 'Part 2: Listening';
export const NAME_PART3 = 'Part 3: Dictation';
export const NAME_PART4 = 'Part 4: Pronunciation';
export const NAME_PART5 = 'Part 5: Grammar';
export const REQUEST_MATCH_PICTUE = 'Kéo từ thả vào tranh';
export const REQUEST_LISTEN_WRITE = 'Nghe và chép các câu mô tả bức tranh';
export const REQUEST_LISTEN_WRITE_VN = 'Dictation EASY';
export const REQUEST_TAP_PICTUE = 'Nghe và chọn bức tranh đúng';
export const REQUEST_LISTEN_WRITE_ALPHABET = 'Hãy nghe và viết lại các từ sau';
export const REQUEST_SOV_UNDER = 'Kéo thả xác định thành phần câu';
export const REQUEST_SOV_ABOVE = 'Kéo thả từ vào ô để xây dựng câu theo cấu trúc cho trước';
export const REQUEST_PRONUNCIATION = 'Xác định các chỗ nối âm';
export const REQUEST_SENTENCE = 'Listen and sentence formation';
export const REQUEST_LESSON_SVO = 'Bài giảng SVO, SVC';
export const INIT_STORGE = '@ACCOUNT';
export const MAIL_BOX = '@MAILBOX';
export const ADD_EVENT = '@ADD_EVENT';
export const SET_LESSSON = '@SET_LESSSON';
export const QUAN_TRI_DANG_VIEN = 'Quản trị Đảng viên';
export const TIN_HOT = 'Dư luận xã hội';
export const LIST_NEWS = [
  {
    title: 'Quản trị Đảng viên',
    icon: "supervisor-account"
  },
  {
    title: 'Kiểm tra thực hiện Nghị quyết',
    icon: "today"
  },
  {
    title: 'Đánh giá chi bộ',
    icon: "event-note"
  },
  {
    title: 'Dư luận xã hội',
    icon: "local-library"
  }
];
export const LIST_INFOR_CT = [
  {
    title: 'ĐV chính thức',
    soluong: "150",
    color: ['rgb(45,161,248)', 'rgb(25,145,235)']
  },
  {
    title: 'ĐV dự bị',
    soluong: "20",
    color: ['rgb(246,171,47)', 'rgb(250,207,85)']
  },
  {
    title: 'Tổng đảng phí',
    soluong: "150 Triệu đồng",
    color: ['rgb(145,92,244)', 'rgb(103,88,243)']
  },
]

export const CT_KINH_TE = [
  {
    type: 'column3d',
    width: '100%',
    height: '100%',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Chỉ tiêu thu nhập bình quân đầu người trong 3 tháng đầu năm",
        "yAxisName": "Đơn vị (Triệu đồng)",
        "theme": "fusion"
      },
      "data": [
        {
          "label": "Tháng 1",
          "value": "4200000"
        },
        {
          "label": "Tháng 2",
          "value": "4500000"
        },
        {
          "label": "Tháng 3",
          "value": "6000000"
        }
      ]
    }
  },
  {
    type: 'pie2d',
    width: '100%',
    height: '100%',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Chỉ tiêu kinh tế về các ngành",
        "subCaption": "Năm 2019",
        "showPercentInTooltip": "0",
        "decimals": "1",
        "useDataPlotColorForLabels": "1",
        "theme": "fusion"
      },
      "data": [
        {
          "label": "Du lịch",
          "value": "13504"
        },
        {
          "label": "Nông nghiệp",
          "value": "14633"
        },
        {
          "label": "Công nghiệp",
          "value": "10507"
        },
        {
          "label": "Lâm nghiệp",
          "value": "3507"
        },
        {
          "label": "Các ngành khác",
          "value": "10235"
        },
      ]
    }
  },
  {
    type: 'candlestick',
    width: '100%',
    height: '100%',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "Chỉ tiêu bình ổn giá của các mặt hàng xuất khẩu trong 3 tháng tới",
        "numberprefix": "$",
        "vNumberPrefix": " ",
        "pyaxisname": "Giá",
        "vyaxisname": "Volume (In Millions)",
        "theme": "fusion"
      },
      "categories": [
        {
          "category": [
            {
              "label": "Tháng 4",
              "x": "1"
            },
            {
              "label": "Tháng 5",
              "x": "31"
            },
            {
              "label": "Tháng 6",
              "x": "60"
            }
          ]
        }
      ],
      "dataset": [
        {
          "data": [
            {
              "open": "18.74",
              "high": "19.16",
              "low": "18.67 ",
              "close": "18.99",
              "x": "1",
              "volume": "4991285"
            },
            {
              "open": "18.74",
              "high": "19.06",
              "low": "18.54",
              "close": "18.82",
              "x": "2",
              "volume": "3615889"
            },
            {
              "open": "19.21",
              "high": "19.3",
              "low": "18.59 ",
              "close": "18.65",
              "x": "3",
              "volume": "4749586"
            },
            {
              "open": "19.85",
              "high": "19.86",
              "low": "19.12",
              "close": "19.4",
              "x": "4",
              "volume": "4366740"
            },
            {
              "open": "20.19",
              "high": "20.21",
              "low": "19.57",
              "close": "19.92",
              "x": "5",
              "volume": "3982709"
            },
            {
              "open": "20.47",
              "high": "20.64",
              "low": "20.15",
              "close": "20.16",
              "x": "6",
              "volume": "2289403"
            },
            {
              "open": "20.36",
              "high": "20.52",
              "low": "20.29",
              "close": "20.48",
              "x": "7",
              "volume": "1950919"
            },
            {
              "open": "20.21",
              "high": "20.25",
              "low": "19.91",
              "close": "20.15",
              "x": "8",
              "volume": "2391070"
            },
            {
              "open": "19.46",
              "high": "20.54",
              "low": "19.46",
              "close": "20.22",
              "x": "9",
              "volume": "4548422"
            },
            {
              "open": "19.24",
              "high": "19.5",
              "low": "19.13",
              "close": "19.44",
              "x": "10",
              "volume": "1889811"
            },
            {
              "open": "19.25",
              "high": "19.41",
              "low": "18.99",
              "close": "19.22",
              "x": "11",
              "volume": "2543355"
            },
            {
              "open": "18.85",
              "high": "19.45",
              "low": "18.8",
              "close": "19.24",
              "x": "12",
              "volume": "2134393"
            },
            {
              "open": "18.97",
              "high": "19.01",
              "low": "18.68",
              "close": "18.95",
              "x": "13",
              "volume": "1740852"
            },
            {
              "open": "18.69",
              "high": "19",
              "low": "18.35",
              "close": "18.97",
              "x": "14",
              "volume": "2701392"
            },
            {
              "open": "19.02",
              "high": "19.1",
              "low": "18.68",
              "close": "18.7",
              "x": "15",
              "volume": "2198755"
            },
            {
              "open": "19.29",
              "high": "19.38",
              "low": "18.88",
              "close": "19.05",
              "x": "16",
              "volume": "2464958"
            },
            {
              "open": "18.64",
              "high": "19.35",
              "low": "18.53",
              "close": "19.33",
              "x": "17",
              "volume": "2962994"
            },
            {
              "open": "18.14",
              "high": "18.58",
              "low": "18.08",
              "close": "18.52",
              "x": "18",
              "volume": "1964932"
            },
            {
              "open": "18.49",
              "high": "18.92",
              "low": "18.19",
              "close": "18.26",
              "x": "19",
              "volume": "3013102"
            },
            {
              "open": "18.71",
              "high": "18.84",
              "low": "18",
              "close": "18.51",
              "x": "20",
              "volume": "4435894"
            },
            {
              "open": "19.17",
              "high": "19.35",
              "low": "18.61",
              "close": "18.66",
              "x": "21",
              "volume": "3245851"
            },
            {
              "open": "19.12",
              "high": "19.41",
              "low": "18.92",
              "close": "19.2",
              "x": "22",
              "volume": "2259792"
            },
            {
              "open": "19.43",
              "high": "19.58",
              "low": "19.16",
              "close": "19.33",
              "x": "23",
              "volume": "3531327"
            },
            {
              "open": "19.72",
              "high": "19.81",
              "low": "19.04",
              "close": "19.27",
              "x": "24",
              "volume": "5834733"
            },
            {
              "open": "19.7",
              "high": "19.94",
              "low": "19.49",
              "close": "19.77",
              "x": "25",
              "volume": "2009987"
            },
            {
              "open": "19.84",
              "high": "19.98",
              "low": "19.39",
              "close": "19.88",
              "x": "26",
              "volume": "2767592"
            },
            {
              "open": "20.71",
              "high": "20.73",
              "low": "19.16",
              "close": "19.63",
              "x": "27",
              "volume": "673358"
            },
            {
              "open": "21.14",
              "high": "21.14",
              "low": "20.55",
              "close": "20.65",
              "x": "28",
              "volume": "3164006"
            },
            {
              "open": "21.5",
              "high": "21.86",
              "low": "21.2",
              "close": "21.33",
              "x": "29",
              "volume": "7986466"
            },
            {
              "open": "20.45",
              "high": "21.08",
              "low": "20.1",
              "close": "20.56",
              "x": "30",
              "volume": "5813040"
            },
            {
              "open": "20.07",
              "high": "20.69",
              "low": "20.04",
              "close": "20.36",
              "x": "31",
              "volume": "3440002"
            },
            {
              "open": "19.88",
              "high": "20.11",
              "low": "19.51",
              "close": "20.03",
              "x": "32",
              "volume": "2779171"
            },
            {
              "open": "19.76",
              "high": "20.13",
              "low": "19.65",
              "close": "19.88",
              "x": "33",
              "volume": "2918115"
            },
            {
              "open": "19.77",
              "high": "19.97",
              "low": "19.27",
              "close": "19.9",
              "x": "34",
              "volume": "3850357"
            },
            {
              "open": "19.43",
              "high": "19.72",
              "low": "18.88",
              "close": "19.5",
              "x": "35",
              "volume": "5047378"
            },
            {
              "open": "19.69",
              "high": "19.84",
              "low": "19.17",
              "close": "19.43",
              "x": "36",
              "volume": "3479017"
            },
            {
              "open": "19.59",
              "high": "20.02",
              "low": "19.02",
              "close": "19.41",
              "x": "37",
              "volume": "5749874"
            },
            {
              "open": "20.95",
              "high": "21.09",
              "low": "19.64",
              "close": "19.83",
              "x": "38",
              "volume": "6319111"
            },
            {
              "open": "20.52",
              "high": "21.03",
              "low": "20.45",
              "close": "21",
              "x": "39",
              "volume": "4412413"
            },
            {
              "open": "20.36",
              "high": "20.96",
              "low": "20.2",
              "close": "20.44",
              "x": "40",
              "volume": "5948318"
            },
            {
              "open": "21.45",
              "high": "21.48",
              "low": "19.63",
              "close": "20.3",
              "x": "41",
              "volume": "11935440"
            },
            {
              "open": "23.49",
              "high": "23.57",
              "low": "21.12",
              "close": "21.63",
              "x": "42",
              "volume": "10523910"
            },
            {
              "open": "24.04",
              "high": "24.21",
              "low": "23.04",
              "close": "23.28",
              "x": "43",
              "volume": "3843797"
            },
            {
              "open": "23.6",
              "high": "24.065",
              "low": "23.51",
              "close": "23.94",
              "x": "44",
              "volume": "3691404"
            },
            {
              "open": "22.87",
              "high": "23.49",
              "low": "22.86",
              "close": "23.48",
              "x": "45",
              "volume": "3387393"
            },
            {
              "open": "22.35",
              "high": "22.89",
              "low": "22.35",
              "close": "22.74",
              "x": "46",
              "volume": "3737330"
            },
            {
              "open": "22.11",
              "high": "22.5",
              "low": "21.9",
              "close": "22.24",
              "x": "47",
              "volume": "4630397"
            },
            {
              "open": "22.58",
              "high": "22.80",
              "low": "22.25",
              "close": "22.42",
              "x": "48",
              "volume": "3024711"
            },
            {
              "open": "23.54",
              "high": "23.76",
              "low": "22.6",
              "close": "22.68",
              "x": "49",
              "volume": "3984508"
            },
            {
              "open": "23.66",
              "high": "24.09",
              "low": "23.09",
              "close": "23.46",
              "x": "50",
              "volume": "3420204"
            },
            {
              "open": "24.36",
              "high": "24.42",
              "low": "22.90",
              "close": "23.6",
              "x": "51",
              "volume": "5151096"
            },
            {
              "open": "24.34",
              "high": "24.6",
              "low": "23.73",
              "close": "24.15",
              "x": "52",
              "volume": "5999654"
            },
            {
              "open": "23.38",
              "high": "24.8",
              "low": "23.36",
              "close": "24.1",
              "x": "53",
              "volume": "5382049"
            },
            {
              "open": "23.76",
              "high": "23.84",
              "low": "23.23",
              "close": "23.47",
              "x": "54",
              "volume": "3508510"
            },
            {
              "open": "23.64",
              "high": "23.94",
              "low": "23.48",
              "close": "23.76",
              "x": "55",
              "volume": "2718428"
            },
            {
              "open": "23.99",
              "high": "24.18",
              "low": "23.59",
              "close": "23.66",
              "x": "56",
              "volume": "2859391"
            },
            {
              "open": "23.32",
              "high": "24.26",
              "low": "23.32",
              "close": "23.79",
              "x": "57",
              "volume": "4138618"
            },
            {
              "open": "24.08",
              "high": "24.4",
              "low": "23.26",
              "close": "23.39",
              "x": "58",
              "volume": "4477478"
            },
            {
              "open": "22.84",
              "high": "23.96",
              "low": "22.83",
              "close": "23.88",
              "x": "59",
              "volume": "4822378"
            },
            {
              "open": "23.38",
              "high": "23.78",
              "low": "22.94",
              "close": "23.01",
              "x": "60",
              "volume": "4037312"
            },
            {
              "open": "23.97",
              "high": "23.99",
              "low": "23.14",
              "close": "23.32",
              "x": "61",
              "volume": "4879546"
            }
          ]
        }
      ]
    }
  }
];
export const LIST_CHECK = [
  {
    title: 'Các Nghị quyết định kỳ',
    icon: "supervisor-account"
  },
  {
    title: 'Các Nghị quyết Trung ương',
    icon: "event-note"
  },
  {
    title: 'Các Nghị quyết theo tháng',
    icon: "today"
  },
  {
    title: 'Các Nghị quyết theo năm',
    icon: "local-library"
  }
];

export const DANH_SACH_THANH_VIEN_THAM_GIA = 'Danh sách Đảng viên tham gia: ';
export const LIST_DINH_KY = [
  {
    title: "Họp chi bộ định kỳ tháng 4",
    timeStart: "15:00",
    dayStart: "05",
    monthStart: "04",
    yearStart: "2019",
    timeEnd: "16:00",
    dayEnd: "05",
    monthEnd: "04",
    yearEnd: "2019",
    details: "Phổ biến cho các đảng viên về các nghị quyết mới được ban hành, đồng thời lên kế hoạch thực hiện nghiêm túc nghị quyết.",
    result: "Đã hoàn thành được một số chỉ tiêu đề ra.",
  },
  {
    title: "Họp chi bộ định kỳ tháng 3",
    timeStart: "15:00",
    dayStart: "05",
    monthStart: "03",
    yearStart: "2019",
    timeEnd: "16:00",
    dayEnd: "05",
    monthEnd: "03",
    details: "Họp để giải quyết các vấn đề còn tồn đọng trong tháng trước, cùng với đó lên kế hoạch để cải thiện mức độ hiệu quả của các cấp.",
    result: "Đã hoàn thành được một số chỉ tiêu đề ra.",
  },
  {
    title: "Họp chi bộ định kỳ tháng 2",
    timeStart: "15:00",
    dayStart: "05",
    monthStart: "02",
    yearStart: "2019",
    timeEnd: "16:00",
    dayEnd: "05",
    monthEnd: "02",
    details: "Họp để giải quyết các vấn đề còn tồn đọng trong tháng trước, cùng với đó lên kế hoạch để cải thiện mức độ hiệu quả của các cấp.",
    result: "Đã hoàn thành được một số chỉ tiêu đề ra.",
  },
  {
    title: "Họp chi bộ định kỳ tháng 1",
    timeStart: "15:00",
    dayStart: "05",
    monthStart: "01",
    yearStart: "2019",
    timeEnd: "16:00",
    dayEnd: "05",
    monthEnd: "01",
    details: "Họp để giải quyết các vấn đề còn tồn đọng trong tháng trước, cùng với đó lên kế hoạch để cải thiện mức độ hiệu quả của các cấp.",
    result: "Đã hoàn thành được một số chỉ tiêu đề ra.",
  },
]
export const LIST_LICH_KHAC = [
  {
    title: "Họp đột xuất",
    timeStart: "15:00",
    dayStart: "03",
    monthStart: "04",
    yearStart: "2019",
    timeEnd: "16:00",
    dayEnd: "03",
    monthEnd: "04",
    details: "Lên kế hoach thực hiện các nhiệm vụ được giao từ trung ương nhằm giải quyết các vấn đề về tôn giáo trong tỉnh.",
    result: "Đã hoàn thành được một số chỉ tiêu đề ra.",
  },
  {
    title: "Học tập Nghị quyết Trung ương",
    timeStart: "15:00",
    dayStart: "11",
    monthStart: "04",
    yearStart: "2019",
    timeEnd: "16:00",
    dayEnd: "11",
    monthEnd: "04",
    details: "Toàn bộ Đảng viên có mặt ở nhà A3 để học tập Nghị quyết Trung ương 8 khoá XII. Đề nghị học tập nghiêm túc, không làm việc riêng trong giờ học.",
    result: "",
  }
]
export const LIST_ATTENT = [
  {
    title: "Họp chi bộ định kỳ tháng 3",
    time: "29",
    month: "3",
  },
  {
    title: "Họp chi bộ định kỳ tháng 2",
    time: "29",
    month: "2",
  },
  {
    title: "Họp chi bộ định kỳ tháng 1",
    time: "29",
    month: "1",
  },
  {
    title: "Họp tập Nghị quyết TW 4 khoá XII",
    time: "16",
    month: "1",
  },
  {
    title: "Họp tập Nghị quyết TW 3 khoá XII",
    time: "10",
    month: "1",
  },
  {
    title: "Họp tập Nghị quyết TW 2 khoá XII",
    time: "06",
    month: "1",
  },
]

export const LIST_CA_NHAN = [
  {
    "hoTen": "Trần Quang Mạnh",
    "chucVu": "Bí thư chi bộ",
    "nhiemVu": "Đưa ra phương hướng và giải pháp mới giúp Đảng bộ cơ sở hoàn thành các nhiệm vụ được giao",
    "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
  },
]
export const LIST_NQ_DINH_KY = [
  {
    title: "Nghị quyết chi bộ định kỳ lần thứ nhất",
    time: "18",
    month: "3",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "P.Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên các công việc cần giải quyết",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lên kế hoạch chi tiết các vấn đề cần giải quyết ",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Quản lí Đảng viên để hoàn thành các công việc được giao",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết chi bộ định kỳ lần thứ 2",
    time: "18",
    month: "2",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết chi bộ định kỳ lần thứ 3",
    time: "18",
    month: "1",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết chi bộ định kỳ lần thứ 4",
    time: "18",
    month: "1",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết chi bộ định kỳ lần thứ 5",
    time: "18",
    month: "1",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết chi bộ định kỳ lần thứ 6",
    time: "18",
    month: "1",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  }
];
export const LIST_NQ_TW = [
  {
    title: "Nghị quyết trung ương 4 Khoá XII",
    time: "29",
    month: "1",
    listAttent: [
      {
        "hoTen": "Hoàng Văn Khoa",
        "chucVu": "Phó bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết trung ương 4 đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát về tình trạng suy thoái về tư tưởng đạo đức, lối sống, những biểu hiện 'tự diễn biến', 'tự chuyển hóa'",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát một số Đảng viên có dấu hiệu suy thoái.",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết trung ương 3 Khoá XII",
    time: "15",
    month: "1",
    listAttent: [
      {
        "hoTen": "Hoàng Văn Khoa",
        "chucVu": "Phó bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết trung ương 3 đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát về tình trạng suy thoái về tư tưởng đạo đức, lối sống, những biểu hiện 'tự diễn biến', 'tự chuyển hóa'",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát một số Đảng viên có dấu hiệu suy thoái.",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết trung ương 2 Khoá XII",
    time: "05",
    month: "1",
    listAttent: [
      {
        "hoTen": "Hoàng Văn Khoa",
        "chucVu": "Phó bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết trung ương 2 đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát về tình trạng suy thoái về tư tưởng đạo đức, lối sống, những biểu hiện 'tự diễn biến', 'tự chuyển hóa'",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát một số Đảng viên có dấu hiệu suy thoái.",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  }
]
export const LIST_NQ_THANG = [
  {
    title: "Nghị quyết chi bộ tháng 1",
    time: "18",
    month: "3",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết chi bộ tháng 2",
    time: "18",
    month: "2",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
  {
    title: "Nghị quyết chi bộ tháng 3",
    time: "18",
    month: "1",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  },
]
export const LIST_NQ_NAM = [
  {
    title: "Tổng hợp các Nghị quyết chi bộ cả năm 2018",
    time: "18",
    month: "3",
    listAttent: [
      {
        "hoTen": "Đinh Văn Anh",
        "chucVu": "Bí thư chi bộ",
        "nhiemVu": "Phổ biến đến tất cả Đảng viên Nghị quyết đã ban hành",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Đinh Văn Linh",
        "chucVu": "Đảng viên",
        "nhiemVu": "Lập kế hoạch khảo sát các vấn đề đã được phổ biến",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      },
      {
        "hoTen": "Mạc Văn Khoa",
        "chucVu": "Đảng viên",
        "nhiemVu": "Thực hiện khảo sát về mức độ hiểu biết Nghị quyết của các Đảng viên",
        "minhChung": "Các tài liệu đã được gửi trong địa chỉ: https://drive.google.com/file/d/1fWrgugUbj6RAZmibe5AvWbYqT0Obd4BT/view?usp=sharing",
      }
    ]
  }
]
export const DATA_FOR_HOME_OLD = [
  {
    title: "Giới thiệu Đảng bộ",
    sourceImage: IMAGE.gioithieuDB,
  },
  {
    title: "Chỉ tiêu Kinh tế xã hội",
    sourceImage: IMAGE.chitieuCD,
  },
  {
    title: "Tin tức sự kiện",
    sourceImage: IMAGE.tintucSK,
  },
  {
    title: "Lịch công tác Đảng",
    sourceImage: IMAGE.lichCTD,
  },
  {
    title: "Học tập theo Chuyên đề",
    sourceImage: IMAGE.hoctaptheoCD,
  },
  {
    title: "Hòm thư Đảng viên",
    sourceImage: IMAGE.homthuDV,
  },
  {
    title: "Hồ sơ cá nhân Đảng viên",
    sourceImage: IMAGE.hosoDV,
  },
  {
    title: "Văn kiện Đảng bộ",
    sourceImage: IMAGE.vankienDB,
  },
  {
    title: "Quản trị Đảng viên",
    sourceImage: IMAGE.quantriDV,
  },
  {
    title: "Kiểm tra thực hiện Nghị quyết",
    sourceImage: IMAGE.ketquaTHNQ,
  },
  {
    title: "Đánh giá chi bộ",
    sourceImage: IMAGE.danhgiaCB,
  },
  {
    title: "Đăng xuất",
    sourceImage: IMAGE.dangxuat,
  },
  {
    title: "",
    sourceImage: '',
  },
  {
    title: "Dư luận xã hội",
    sourceImage: IMAGE.tintucSK,
  }
];
export const LIST_ON_POP_UP = [
  // {
  //   title:"Quản trị Đảng viên",
  //   sourceImage:IMAGE.quantriDV,
  // },
  // {
  //   title:"Kiểm tra thực hiện Nghị quyết",
  //   sourceImage:IMAGE.ketquaTHNQ,
  // },
  {
    title: "Đánh giá chi bộ",
    sourceImage: IMAGE.danhgiaCB,
  },
  {
    title: "Dư luận xã hội",
    sourceImage: IMAGE.tintucSK,
  }
]
export const DATA_FOR_HOME_NO_USER = [
  {
    title: "Giới thiệu Đảng bộ",
    sourceImage: IMAGE.gioithieuDB,
  },
  {
    title: "Tin tức sự kiện",
    sourceImage: IMAGE.tintucSK,
  },
  {
    title: "Văn kiện Đảng bộ",
    sourceImage: IMAGE.vankienDB,
  },
  {
    title: "",
    sourceImage: "",
  },
  {
    title: "Đăng nhập",
    sourceImage: IMAGE.dangxuat,
  }
]
export const DATA_FOR_HOME_NEW = [
  {
    title: "Giới thiệu Đảng bộ",
    sourceImage: IMAGE.gioithieuDB,
  },
  {
    title: "Chỉ tiêu Kinh tế xã hội",
    sourceImage: IMAGE.chitieuCD,
  },
  {
    title: "Tin tức sự kiện",
    sourceImage: IMAGE.tintucSK,
  },
  {
    title: "Lịch công tác Đảng",
    sourceImage: IMAGE.lichCTD,
  },
  {
    title: "Học tập theo Chuyên đề",
    sourceImage: IMAGE.hoctaptheoCD,
  },
  {
    title: "Hòm thư Đảng viên",
    sourceImage: IMAGE.homthuDV,
  },
  {
    title: "Hồ sơ cá nhân Đảng viên",
    sourceImage: IMAGE.hosoDV,
  },
  {
    title: "Văn kiện Đảng bộ",
    sourceImage: IMAGE.vankienDB,
  },
  {
    title: "Quản trị công tác Đảng",
    sourceImage: IMAGE.quantriDV,
  },
  {
    title: "",
    sourceImage: "",
  },
  {
    title: "Đăng xuất",
    sourceImage: IMAGE.dangxuat,
  }
];
export const LIST_VAN_KIEN = [
  {
    titleBig: 'Nghị quyết Trung ương 10 - Khóa XII',
    describe: [
      {
        title: 'Tình hình và nguyên nhân',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1dPBOe2Stuir0YexYgM3jBbwEV_C0ool3',
          cache: true
        }
      },
      {
        title: 'Quan điểm, mục tiêu',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1dPBOe2Stuir0YexYgM3jBbwEV_C0ool3',
          cache: true
        }
      },
      {
        title: 'Nhiệm vụ, giải pháp',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1dPBOe2Stuir0YexYgM3jBbwEV_C0ool3',
          cache: true
        }
      },
      {
        title: 'Tổ chức thực hiện',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1dPBOe2Stuir0YexYgM3jBbwEV_C0ool3',
          cache: true
        }
      }
    ]
  },
  {
    titleBig: 'Nghị quyết Trung ương 11 - Khóa XII',
    describe: [
      {
        title: 'Tình hình và nguyên nhân',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1FrgQKfoUrDrx2zIPonMMu8SfXh04rFTP',
          cache: true
        }
      },
      {
        title: 'Quan điểm, mục tiêu',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1FrgQKfoUrDrx2zIPonMMu8SfXh04rFTP',
          cache: true
        }
      },
      {
        title: 'Nhiệm vụ, giải pháp',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1FrgQKfoUrDrx2zIPonMMu8SfXh04rFTP',
          cache: true
        }
      },
      {
        title: 'Tổ chức thực hiện',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1FrgQKfoUrDrx2zIPonMMu8SfXh04rFTP',
          cache: true
        }
      }
    ]
  },
  {
    titleBig: 'Nghị quyết Trung ương 28 - Khóa XII',
    describe: [
      {
        title: 'Tình hình và nguyên nhân',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1T85tIkXZO6qzEyFkFyYxvK4ZqNVkzOH0',
          cache: true
        }
      },
      {
        title: 'Quan điểm, mục tiêu',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1T85tIkXZO6qzEyFkFyYxvK4ZqNVkzOH0',
          cache: true
        }
      },
      {
        title: 'Nhiệm vụ, giải pháp',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1T85tIkXZO6qzEyFkFyYxvK4ZqNVkzOH0',
          cache: true
        }
      },
      {
        title: 'Tổ chức thực hiện',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1T85tIkXZO6qzEyFkFyYxvK4ZqNVkzOH0',
          cache: true
        }
      }
    ]
  }
];

export const LIST_CONG_VAN = [
  {
    titleBig: 'Văn kiện Đảng bộ Tập 1',
    describe: [
      {
        title: 'Nội dung',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1g5B6gIRJtexMHDOY_r4xY7sP0qQtqKAj',
          cache: true
        }
      }
    ]
  },
  {
    titleBig: 'Văn kiện Đảng bộ Tập 2',
    describe: [
      {
        title: 'Nội dung',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1h1QzVPx1gcLVC6GhYjQBy9ltMRk6RPcZ',
          cache: true
        }
      }
    ]
  },
  {
    titleBig: 'Văn kiện Đảng bộ Tập 3',
    describe: [
      {
        title: 'Nội dung',
        sourcePDF: {
          uri: 'https://drive.google.com/uc?export=download&id=1RSyrsTm9TEwdBSRUWo2968Tbk6NZtkEr',
          cache: true
        }
      }
    ]
  },
];
export const LIST_TARGET = [
  {
    sourceImage: IMAGE.tichcuc,
    title: 'Đánh giá về mức độ hạn chế và hiệu quả của một số phương pháp làm mới trong Đảng bộ cơ sở',
  },
  {
    sourceImage: IMAGE.danhgia,
    title: 'Đánh giá về mức độ hiệu quả của các Nghị quyết dưới cấp độ cơ sở',
  },
  {
    sourceImage: IMAGE.ykien,
    title: 'Các phương pháp dự kiến áp dụng để phổ biến các vấn đề tới Đảng viên.',
  },
];

export const HOC_MOI_LUC = 'Học mọi lúc, mọi nơi';
export const TRANG_CHU = 'Trang chủ';
export const TOP_TIN_QUAN_TRONG = 'Top tin quan trọng cho bạn';
export const TAI_DON_VI = 2;
export const TIN_NEW = 1;
export const TIN_KHAC = 'Tin khác';
export const BO_QUA = 'Bỏ qua >>';
export const BAI_1_DOC_HIEU_NGHI_QUYET = 'Bài 1: Đọc hiểu Chuyên đề';
export const BAI_2_TRA_LOI_CAU_HOI = 'Bài 2: Trả lời các câu hỏi?';
export const BAN_DA_HOAN_THANH_XONG_BAI_HOC = 'Bạn đã hoàn thành xong bài học';
export const VAN_KIEN_DANG_BO = 'Văn kiện Đảng bộ';
export const LIST_OF_DRAWER = [
  {
    text: 'Lịch công tác Đảng',
    linkImage: '',
  },
  {
    text: 'Học tập theo Chuyên đề',
    linkImage: '',
  },
  {
    text: 'Hòm thư Đảng viên',
    linkImage: '',
  },
  {
    text: 'Hồ sơ Đảng viên',
    linkImage: '',
  },
  {
    text: 'Văn kiện Đảng bộ',
    linkImage: '',
  },
  {
    text: 'Quản trị Đảng viên',
    linkImage: '',
  }
]

export const LIST_DAY = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
export const SLIDE_INTRO = [
  {
    key: 'somethun-dos',
    title: 'Các lĩnh vực Chatbot - Thái Bình hỗ trợ gồm',
    titleStyle: { textAlign: 'center', textAlignVertical: 'center', fontSize: STYLES.fontSizeNormalText * 1.6, color: colors.colorBlack, fontFamily: 'Roboto-Medium' },
    text: '1/ Quy định về kết nạp đảng.\n\n2/ Điều lệ đảng viên.\n\n3/ Nguyên tắc tổ chức và cơ cấu tổ chức của Đảng.\n\n\n',
    image: IMAGE.lich,
    textStyle: { textAlign: 'left', textAlignVertical: 'center', paddingHorizontal: 25, color: colors.colorBlack, marginLeft: 25 },
    imageStyle: { width: 120, height: 120 },
    backgroundColor: colors.white,
  },
  {
    key: 'somethun1',
    title: 'Các lĩnh vực Chatbot - Thái Bình hỗ trợ gồm',
    titleStyle: { textAlign: 'center', textAlignVertical: 'center', fontSize: STYLES.fontSizeNormalText * 1.6, color: colors.colorBlack, fontFamily: 'Roboto-Medium' },
    text: '4/ Nghị quyết hội nghị lần thứ 4 Ban chấp hành Trung ương Đảng khóa XII về tăng cường xây dựng, chỉnh đốn Đảng; ngăn chặn, đẩy lùi sự suy thoái về tư tưởng chính trị, đạo đức, lối sống, những biểu hiện "tự diễn biến", "tự chuyển hóa" trong nội bộ.\n\n\n',
    image: IMAGE.bookmac,
    textStyle: { textAlign: 'left', textAlignVertical: 'center', paddingHorizontal: 25, color: colors.colorBlack },
    imageStyle: { width: 150, height: 150 },
    backgroundColor: colors.white,
  }
];

export const GIOI_THIEU_DRAWER = 'Giới thiệu Đảng bộ Tỉnh uỷ';
export const DANG_XUAT = 'Đăng xuất';
export const DANG_NHAP = 'Đăng nhập';
export const DANG_VIEN = 'Đảng viên';

export const HOC_TAP_THEO_CHUYEN_DE = 'Học tập theo Chuyên đề';
export const CHI_TIEU_CAN_DAT = 'Chỉ tiêu Kinh tế xã hội';

export const TRA_LOI = 'Trả lời';
export const CHUYEN_TIEP = 'Chuyển tiếp';
export const CAC_LINH_VUC_HO_TRO = 'Các lĩnh vực mà chúng tôi hỗ trợ';

export const HO_VA_TEN = 'Họ và tên';

export const LIST_OF_INIT_CHATBOT = [
  {
    user: 0,
    content: 'Xin chào bạn, tôi là Chatbot, bạn có thắc mắc nào không?'
  },
  {
    user: 3,
    content: ['Quy định về kết nạp Đảng', 'Điều lệ Đảng viên', 'Nguyên tắc tổ chức và cơ cấu tổ chức của Đảng',
      'Nghị quyết TW4 - khóa XII']
  },
];
export const DATA_FAKE_SERVER = [
  {
    "idmessage": 819,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 19",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:47",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:49",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 19",
    "priority": 0,
    "newsid": 19
  },
  {
    "idmessage": 818,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 18",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:46",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:48",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": true,
    "seennote": "fix seen",
    "details": "EventContent 18",
    "priority": 0,
    "newsid": 18
  },
  {
    "idmessage": 817,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 17",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:45",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:47",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 17",
    "priority": 0,
    "newsid": 17
  },
  {
    "idmessage": 816,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 16",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:44",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:46",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 16",
    "priority": 0,
    "newsid": 16
  },
  {
    "idmessage": 815,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 15",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:43",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:45",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 15",
    "priority": 0,
    "newsid": 15
  },
  {
    "idmessage": 814,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 14",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:42",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:44",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 14",
    "priority": 0,
    "newsid": 14
  },
  {
    "idmessage": 813,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 13",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:41",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:43",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 13",
    "priority": 0,
    "newsid": 13
  },
  {
    "idmessage": 812,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 12",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:40",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:42",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": true,
    "seennote": "fix seen",
    "details": "EventContent 12",
    "priority": 0,
    "newsid": 12
  },
  {
    "idmessage": 811,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 11",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:39",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:41",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 11",
    "priority": 0,
    "newsid": 11
  },
  {
    "idmessage": 810,
    "sender": "Trần Quang Mạnh",
    "title": "EventTitle 10",
    "timestart": "2019-03-19 22:28",
    "timeend": "2019-03-19 22:38",
    "time": "2019-03-19 22:29",
    "reminder": 0,
    "timereminder": "2019-03-19 22:40",
    "repeat": 0,
    "repeatnumber": 0,
    "timerepeat": null,
    "seen": false,
    "seennote": null,
    "details": "EventContent 10",
    "priority": 0,
    "newsid": 10
  }
]
export const LIST_INFOR_ACCOUNT = [
  {
    title: 'Số thẻ Đảng viên',
    detail: 'A2567123123'
  },
  {
    title: 'Họ và tên',
    detail: ''
  },
  {
    title: 'Quê quán',
    detail: 'Hà Nội'
  },
  {
    title: 'Nơi ở hiện nay',
    detail: '89 Phùng Hưng Hà Đông Hà Nội'
  },
  {
    title: 'Giới tính',
    detail: 'Nam'
  },
  {
    title: 'Dân tộc',
    detail: 'Kinh'
  },
  {
    title: 'Tôn giáo',
    detail: 'Không'
  },
  {
    title: 'Chức vụ',
    detail: 'Nhân viên'
  },
  {
    title: 'Trình độ',
    detail: 'Tiến sỹ'
  },
  {
    title: 'Ngày vào Đảng',
    detail: '23-05-1998'
  },
  {
    title: 'Ngày vào Đảng chính thức',
    detail: '23-05-1999'
  },
];
// format:1 tương ứng với "important"
// format: 2 tương ứng với "normal"
// format: 3 tương ứng với "no important"
// type : 1 tương ứng với "Tin tại đơn vị"
// type : 2 tương ứng với " Tin mới"
// format:1 tương ứng với "important"
// format: 2 tương ứng với "normal"
// format: 3 tương ứng với "no important"
// type : 2 tương ứng với "Tin tại đơn vị"
// type : 1 tương ứng với " Tin mới"

export const NORMAL = 2;
export const NO_IMPORTANT = 3;
export const IMPORTANT = 1;

export const ERROR = 'error';
export const TAI_KHOAN = 'Tài khoản Đảng viên';

export const DA_GUI_TIN_NHAN_THANH_CONG = 'Đã gửi tin nhắn thành công';
export const THONG_BAO = 'Thông báo';
export const THEM_IT_NHAT_MOT_NGUOI_NHAN = 'Thêm ít nhất một người nhận';
export const DA_XAY_RA_LOI_KHI_GUI = 'Đã xảy ra lỗi khi gửi';

export const SOAN_EMAIL = 'Soạn email';
export const DATA_USER_FAKE = [
  {
    name: 'Pham Huan',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    username: 'huanhtm@gmail.com'
  },
  {
    name: 'Hoang Anh',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    username: 'huanhtm@gmail.com'
  },
  {
    name: 'Quang Tuan',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    username: 'huanhtm@gmail.com'
  },
  {
    name: 'Linh Anh',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    username: 'huanhtm@gmail.com'
  },
  {
    name: 'Bao Anh',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    username: 'huanhtm@gmail.com'
  }
];

export const ARR_END = [{
  titleFirst: 'Người được mời',
  titleSecond: 'Không có',
  data: [
    { id: -1, content: 'Không có', format: 'no one' },
    { id: 0, content: 'Tất cả Đảng viên trong chi bộ', format: 'all' },
  ],
},
{
  titleFirst: 'Nhắc lịch',
  titleSecond: 'Không',
  data: [
    { id: -1, content: 'Không', format: '' },
    { id: 0, content: 'Vào lúc diễn ra sự kiện', format: 'Phút' },
    { id: 5, content: 'Trước 5 phút', format: 'Phút' },
    { id: 15, content: 'Trước 15 phút', format: 'Phút' },
    { id: 1, content: 'Trước 1 ngày', format: 'Ngày' },
    { id: 2, content: 'Trước 2 ngày', format: 'Ngày' },
    { id: 7, content: 'Trước 1 tuần', format: 'Ngày' },
    { id: 14, content: 'Trước 2 tuần', format: 'Ngày' },
  ]
},
{
  titleFirst: 'Lặp lại',
  titleSecond: 'Không',
  data: [
    { id: -1, content: 'Không', format: '' },
    { id: 1, content: 'Hàng ngày', format: 'Ngày' },
    { id: 7, content: 'Hàng tuần', format: 'Ngày' },
    { id: 1, content: 'Hàng tháng', format: 'Tháng' }
  ]
},
]
export const DATA_FORMAT_MONTH = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
export const ARR_OF_TITLE = [
  {
    label: 'Học Nghị quyết',
    value: 'Học Nghị quyết',
  },
  {
    label: 'Họp chi bộ',
    value: 'Họp chi bộ',
  },
  {
    label: 'Khác',
    value: 'Khác',
  },
];

export const NGUOI_DUOC_MOI = 'Người được mời';
export const KHONG_CO = 'Không có';

export const LAP_LAI = 'Lặp lại';
export const LAN = 'lần';


export const NHAC_LICH = 'Nhắc lịch';


export const ARR_TIME = ['Bắt đầu', 'Kết thúc'];
export const ARR_TITLE = [{ text: 'Tiêu đề khác', image: 'md-bookmarks' }, { text: 'Địa điểm', image: 'ios-home' }];
export const GHI_CHU = 'Ghi chú';
export const CAC_CONG_VIEC_CHINH = 'Các công việc chính';

export const TIN_TUC_SU_KIEN = 'Tin tức - Sự kiện';

export const NEU_BAN_DA_CO_MA = 'Nếu bạn đã có MÃ KÍCH HOẠT hãy nhập vào ô bên dưới';

export const VOCABULARY_REVIEW = 'Vocabulary review';
export const LISTEN_AND_WRITE_FOR_VOCABULARY = 'Listen and write for Vocabulary review';
export const LISTEN_AND_CHOOSE_THE_CORRECT_PICTURE = 'Listen and choose the correct picture';
export const LISTEN_AND_WRITE_FOR_DICTATION = 'Listen and write for Dictation';
export const PRONUNCIATION_DRILL = 'Pronunciation drill';
export const PRONUNCIATION_DRILL_FIRST = 'Pronunciation drill first';
export const SENTENCE_FORMATION_WITH_SVO_LAST = 'Sentence formation with SVO last';
export const SENTENCE_FORMATION_WITH_SVO_FIRST = 'Sentence formation with SVO first';
export const LISTEN_AND_WRITE_FOR_GRAMMAR = 'Listen and write for Grammar';
export const LISTEN_AND_WRITE_REVIEW = 'Listen and write Review';
export const INTRO_GAME = 'Intro Game';
export const COMPLETE_SCREEN = 'CompleteScreen';
export const SENTENCE_FORMATION_LABEL = 'Sentence formation';
export const SENTENCE_FORMATION_LABEL_2 = 'Xây dựng câu theo cấu trúc cho trước';
export const LISTEN_AND_CHOOSE_THE_CORRECT_ANSWER_LABEL_HEADER = 'Listen and choose the correct answer';
export const LISTEN_AND_CHOOSE_THE_CORRECT_ANSWER_LABEL = 'Nghe và chọn đáp án đúng';
export const LESSON_SVO = 'LessonSVO';
export const LESSON_SVC = 'LESSON_SVC';
export const SUMARIZE = 'Summarize';
export const USEFUL_EXPRESS = 'Useful expressions';
export const CONVERSATION_LABEL = 'Conversation';
export const SUMMARIZE_SCREEN_NAME = '\nSummarize';
export const TONG_KET = 'TỔNG KẾT';
export const UNIT_NAME = 'Unit ';

export const GOI_Y = 'Gợi ý';
export const CHI_TIET = 'Chi tiết';
export const HOME = 'Trang chủ';
export const NANGCAP = 'Nâng cấp';
export const NANGCAPNGAY = 'NÂNG CẤP NGAY';

export const HOM_NAY = 'Hôm nay';
export const TAT_CA = 'Tất cả';

export const DA_THUC_HIEN = 'Lịch định kỳ';
export const CAN_THUC_HIEN = 'Lịch khác';

export const BANG_XEP_HANG = 'Bảng xếp hạng';

export const DONE = 'DONE';
export const MA_KICH_HOAT = 'Mã kích hoạt gồm 8 kí tự';
export const CONTINUE = 'Tiếp tục';
export const TRY_AGAIN = 'thử lại';
export const CHECK = 'Kiểm tra';
export const LISTEN = 'Listen';
export const WHAT_IS_THIS = 'What is this?';
export const BAI_HOC_DA_HOAN_TAT = 'Bài học đã hoàn tất';
export const CUNG_NHAU_XEM_LAI = 'Cùng nhau xem lại nội dung đã học!';


export const ON_LAI_BAI_HOC = 'Ôn lại bài học';
export const TAI_SAO_NEN_ON_LAI_BAI_HOC = 'Tại sao nên ôn lại bài học?';
export const HAY_ON_LAI_BAI_HOC = ' Hãy ôn lại những gì bạn đã học rồi mới kết thúc. Chỉ cần 5 đến 10 phút nữa thôi, nhưng bạn sẽ ghi nhớ tốt hơn những gì đã học đấy.';
export const HUY = 'Hủy';
export const BAT_DAU = 'Bắt đầu';
export const CHAM_VAO_TU_BAN_KHONG_NAM_RO = 'Chạm vào từ bạn không nắm rõ nghĩa để được giải thích!';
export const MEO_NHO = 'Mẹo nhỏ!';
export const DUNG = 'Đúng';
export const SAU_KHI_LAM_XONG = 'Hãy nói: ';

export const GOI1 = 'GÓI 01';
export const GOI2 = 'GÓI 02';
export const GOI3 = 'GÓI 03';
export const HOAC = 'Hoặc';
export const UU_DAI = 'Ưu đãi của gói này';
export const TIEN = '59.000đ/tháng';
export const NEU_MA_KICH_HOAT = 'Nếu bạn đã có MÃ KÍCH HOẠT rồi mời bạn nhập mã vào ô bên dưới';
export const HOC_NHANH_PREMIUM = 'Học nhanh hơn với gói Premium';
export const NANG_CAP_NGAY = 'Nâng cấp ngay hôm nay để học hoàn thiện trình độ tiếng Anh của bạn!';

export const NANG_CAP_THANH_CONG = 'NÂNG CẤP THÀNH CÔNG!';
export const CHUC_BAN_HOC_TAP = 'Chúc bạn học tập hiệu quả với mọi ưu đãi của English at 5 finges tips dành cho bạn!';

export const MO_TOAN_BO = 'Mở toàn bộ nội dung';
export const FIRE_WORK = 'FireWord';

export const DANH_SACH_XEP_HANG = 'DANH SÁCH XẾP HẠNG';
export const AP_DUNG_CHO_CA = 'Áp dụng cho cả thành viên thường và Premium';

export const THEM_BAN = 'Thêm bạn';
export const GAME = 'Game';
export const THIET_LAP_MUC_TIEU = 'Thiết lập mục tiêu';

export const THE_SIMPLE_PAST_TENSE = 'The simple past tense';
export const TELLING_THE_TIME = 'Telling the time';
export const TIME_EXPRESSIONS = 'Time expressions';
export const PREPOSITIONS_OF_TIME = 'Prepositions of time';
export const EXAMPLE = 'Example:';

export const SIGNIN = 'Đăng nhập';
export const SIGNUP = 'Đăng ký';
export const FACEBOOK = 'Facebook';
export const GOOGLE = 'Google';
export const LOSTPASSWORD = 'Quên mật khẩu?';
export const ACCEPT = '*Bằng cách ấn vào nút đăng ký bạn xác nhận là bạn đồng ý  với điều khoản sử dụng App English At 5 Finger Tips';
export const LANGUGE = ['Tiếng Việt', 'Tiếng Anh', 'Tiếng Hàn'];
export const COURSE = ['TOEIC', 'IELTS', 'TOEFT'];
export const dataGioiThieu = ['>> Đảng bộ Tỉnh uỷ', '>> Văn phòng Tỉnh ủy', '>> Ban tổ chức', '>> Ban Tuyên giáo', '>> Ban dân vận', '>> Uỷ ban kiểm tra', '>> Ban nội chính'];
export const TEXT_BUTTON_LEARN_IMMEDIATELY = 'Tôi muốn học ngay, tôi sẽ đăng nhập sau';


export const SCREEN_MAILBOX = 'Hòm thư Đảng viên';
export const SCREEN_SOAN_EMAIL = 'Soạn email';
export const SCREEN_DETAILS_EMAIL = 'Chi tiết thư';
/// Fake 

export const DATA1 = [
  {
    timeStart: '15h',
    dayStart: 24,
    monthStart: 1,
    yearStart: 2019,
    timeEnd: '18h',
    dayEnd: 24,
    monthEnd: 1,
    location: 'Nhà hiệu bộ',
    yearEnd: 2019,
    title: 'Lịch sinh hoạt Tỉnh uỷ định kì',
    result: 'Sinh hoạt hàng tháng theo yêu cầu',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 26,
    monthStart: 1,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 27,
    monthEnd: 1,
    yearEnd: 2019,
    title: 'Lịch công tác Đảng theo chuyên đề',
    result: 'Học tập Nghị quyết TW',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 28,
    monthStart: 1,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 28,
    monthEnd: 1,
    yearEnd: 2019,
    title: 'Lịch học tập quán triệt Nghị quyết',
    result: '',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 29,
    monthStart: 1,
    yearStart: 2019,
    location: 'Nhà hiệu bộ',
    timeEnd: '18h',
    dayEnd: 31,
    monthEnd: 1,
    yearEnd: 2019,
    title: 'Lịch công tác khác',
    result: '',
    details: ''
  }
]

export const DATA2 = [
  {
    timeStart: '15h',
    dayStart: 3,
    monthStart: 2,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 5,
    monthEnd: 2,
    yearEnd: 2019,
    title: 'Lịch sinh hoạt Tỉnh uỷ định kì',
    result: 'Sinh hoạt hàng tháng theo yêu cầu',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 8,
    monthStart: 2,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 9,
    monthEnd: 2,
    yearEnd: 2019,
    title: 'Lịch công tác Đảng theo chuyên đề',
    result: 'Học tập Nghị quyết TW',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 26,
    monthStart: 2,
    yearStart: 2019,
    timeEnd: '18h',
    dayEnd: 26,
    location: 'Nhà hiệu bộ',
    monthEnd: 2,
    yearEnd: 2019,
    title: 'Lịch học tập quán triệt Nghị quyết',
    result: '',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 27,
    monthStart: 2,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 28,
    monthEnd: 2,
    yearEnd: 2019,
    title: 'Lịch công tác khác',
    result: '',
    details: ''
  }
]
export const DATA3 = [
  {
    timeStart: '15h',
    dayStart: 1,
    monthStart: 3,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 2,
    monthEnd: 3,
    yearEnd: 2019,
    title: 'Lịch sinh hoạt Tỉnh uỷ định kì',
    result: 'Sinh hoạt hàng tháng theo yêu cầu',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 26,
    monthStart: 3,
    yearStart: 2019,
    location: 'Nhà hiệu bộ',
    timeEnd: '18h',
    dayEnd: 27,
    monthEnd: 3,
    yearEnd: 2019,
    title: 'Lịch công tác Đảng theo chuyên đề',
    result: 'Học tập Nghị quyết TW',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 15,
    monthStart: 3,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 17,
    monthEnd: 3,
    yearEnd: 2019,
    title: 'Lịch học tập quán triệt Nghị quyết',
    result: '',
    details: ''
  },
  {
    timeStart: '15h',
    dayStart: 29,
    monthStart: 3,
    yearStart: 2019,
    timeEnd: '18h',
    location: 'Nhà hiệu bộ',
    dayEnd: 31,
    monthEnd: 3,
    yearEnd: 2019,
    title: 'Lịch công tác khác',
    result: '',
    details: ''
  }
]
export const DATA4 = []
export const DATA6 = []
export const DATA7 = []
export const DATA8 = []
export const DATA9 = []
export const DATA10 = []
export const DATA11 = []
export const DATA12 = []

export const CHI_TIET_THU = 'Chi tiết thư'

export const DATA_MAIL_BOX = [
  {
    sender: 'Admin',
    title: 'Giới thiệu',
    time: '2019-01-11 08:00',
    seen: false,
    details: 'Chào mừng bạn đến với Sổ tay Đảng viên điện tử'
  }
]
