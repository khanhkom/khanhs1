import {responsiveFontSize} from "./Function";
import {Dimensions} from "react-native";
const {width, height} = Dimensions.get('window');

const heightHeader= height >= 976 ? height / 10 : height / 5 >= 50 ? 50 : height / 5;
const fontSizeLabel = height >= 976 ? responsiveFontSize(2.3):responsiveFontSize(2.2);
const STYLES = {
    heightHeader: heightHeader,
    iconHeader: heightHeader* 0.35,
    widthScreen: width,
    heightScreen: height,
    heightScreenContainer: height - heightHeader,
    widthChapter: width * 0.8,
    heightItemUnit: heightHeader* 1.7,
    fontSizeLarge: responsiveFontSize(3),
    fontTittle: responsiveFontSize(12),
    fontSizeLabel: fontSizeLabel,
    fontSizeSubText: responsiveFontSize(1.5),
    fontSizeMiniText: responsiveFontSize(1.3),
    fontSizeText: responsiveFontSize(2),
    fontSize25: responsiveFontSize(2.8),
    fontSize26: responsiveFontSize(2.5),
    fontSizeChatbot: responsiveFontSize(2.3),
    fontSizeNormalText: responsiveFontSize(1.7),
    sizeIcon: width * 0.3,
    floatRight:{
        alignItems: 'flex-end',
    },
    centerItem: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    fullScreen:{
        width: width,
        height: height,
    },
    fullContent:{
        width: width,
        height: height - height / 5 >= 50 ? 50 : height / 5,
    }
};

export const fontConstant = new Float32Array([
    2.14, //American Typewriter, Baskerville
    1.91, //Georgia, Arial, Lucida Grande, Tahoma
    2.21, //Times New Roman
    2.1, //Calibri
    1.9, //Helvetica Neue
    2.11, //Trebuchet MS
    1.73, //Verdana
    1.64, //Courier New
]);


export default STYLES;