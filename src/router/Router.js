//import lib
import {  StackNavigator } from 'react-navigation';
import React from 'react';
//import screen
import Home from '../container/Home/Home';
import Intro from '../container/Intro/Intro';
import Usage from '../container/Usage/Usage';
import SignIn from '../container/SignIn/SignIn'
import AddEvent from '../container/SortJob/AddEvent';
import DetailsMail from '../container/MailBox/DetailsMail/DetailsMail';
import ReplyMail from '../container/MailBox/ReplyMail/ReplyMail';
import TestExercises from '../container/StudyByThematic/TestExercises';
import Attendance from '../container/Management/Attendance';
import HomeNew from '../container/HomeNew/HomeNew';
import TablePersion from '../container/Result/TablePersion';
	import Attendances from '../container/Management/Attendances'
import { SortJob } from '../container/SortJob/SortJob';
import PeopleInvited from '../container/SortJob/PeopleInvited'

console.disableYellowBox = true
const Router = StackNavigator({
	Intro: { screen: Intro },
	Home:{
		screen: ({ navigation,screenProps }) => <Home screenProps={{ navigation: navigation,
			 }} />,
	},
	SignIn: { screen: SignIn },
	ReplyMail: { screen: ReplyMail },
	Usage: { screen: Usage },
	
	// SortJob: { screen: SortJob },
	// Attendances: { screen: Attendances },
	PeopleInvited: {screen:PeopleInvited},
	AddEvent: { screen: AddEvent },
	DetailsMail: { screen: DetailsMail },
}, {
		initialRouteName: 'PeopleInvited',
		swipeEnabled: true,
		animationEnabled: false,
		headerMode: 'none',
		navigationOptions: {
			header: null
		},
		lazy: true,
		cardStyle: {
			backgroundColor: '#FFF',
			opacity: 1
		},
	})

export default Router;
