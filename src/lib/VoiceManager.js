import React, { Component } from 'react'
import Voice from 'react-native-voice';
var self;
class VoiceManager extends Component<Props> {
    constructor(props) {
        super(props);
        self=this;
        this.state={
          speakValue:'',
        }
        Voice.onSpeechResults = this.onSpeechResults.bind(this);
        this.onSpeechStart = this.onSpeechStart.bind(this);
      }
      componentWillUnmount() {
        Voice.destroy().then(Voice.removeAllListeners);
      }
       async onSpeechStart() {
        this.setState({speakValue:''}); 
        try {
          await Voice.start('vi');
        } catch (e) {
        }
      }
      onSpeechResults(e) {
         this.setState({
          speakValue: e.value[0],
        })
        return e.value[0];
      }
      outPut(){
        return this.state.speakValue;
      }
}
const store = new VoiceManager();
export default store;
