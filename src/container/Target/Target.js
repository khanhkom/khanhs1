/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView, Platform
} from 'react-native'
import FusionCharts from 'react-native-fusioncharts';
import { Card, ListItem, Button, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { CHI_TIEU_CAN_DAT, LIST_INFOR_CT, CT_KINH_TE } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';

class Target extends Component<Props> {
    constructor(props) {
        super(props);
        this.libraryPath = Platform.select({
            // Specify fusioncharts.html file location
            //ios: require('./assets/fusioncharts.html'),
            android: { uri: 'file:///android_asset/fusioncharts.html' }
        });
    }
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={CHI_TIEU_CAN_DAT}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                    <View style={styles.container}>
                        <View style={styles.cntInfor}>
                            {
                                LIST_INFOR_CT.map((value, index) => {
                                    return (
                                        <LinearGradient
                                            start={{ x: 0, y: 0 }}
                                            end={{ x: 1, y: 0 }}
                                            colors={value.color}
                                            style={styles.bntText}>
                                            <Text style={styles.text}>{value.title}</Text>
                                            <Text style={styles.text}>{value.soluong}</Text>
                                        </LinearGradient>
                                    )
                                })
                            }
                        </View>
                        {
                            CT_KINH_TE.map((value, index) => {
                                return (
                                    <View style={{
                                        marginVertical: 10 * STYLES.heightScreen / 896,
                                        width: STYLES.widthScreen,
                                        height: 0.5 * STYLES.heightScreen
                                    }}>
                                        <FusionCharts
                                            type={value.type}
                                            width={value.width}
                                            height={value.height}
                                            dataFormat={value.dataFormat}
                                            dataSource={value.dataSource}
                                            libraryPath={this.libraryPath} // set the libraryPath property
                                        />
                                    </View>)
                            })
                        }
                        {/* {
                            LIST_TARGET.map((value, index) => {
                                return (
                                    <Card
                                        title='Biểu đồ'
                                        image={value.sourceImage}>
                                        <Text style={{ marginBottom: 10 }}>
                                            {value.title}
                                        </Text>
                                        <Button
                                            icon={<Icon name='code' color='#ffffff' />}
                                            backgroundColor='#03A9F4'
                                            buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                                            title='Xem ngay' />
                                    </Card>
                                )
                            })
                        } */}
                    </View>
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(Target);