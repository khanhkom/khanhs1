import {
   StyleSheet,
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

export default styles = StyleSheet.create({
    container: {
      justifyContent:'center',
      paddingBottom: 20*STYLES.heightScreen/640,
    },
    cntViewPDF:{
      flex:1,
    },
    cntInfor:{
      marginVertical: 10*STYLES.heightScreen/896,
      paddingVertical: 20*STYLES.heightScreen/896,
      width:STYLES.widthScreen,
      paddingHorizontal:10*STYLES.widthScreen/360,
      justifyContent:'space-between',
      flexDirection: 'row',
      alignItems:'center',
      backgroundColor:'#fff',
    },
    bntText:{
        flex:0,
        justifyContent:'center',
        alignItems:'center',
        width:STYLES.widthScreen*0.3,
        borderRadius: 3,
        height:108*STYLES.heightScreen/896
    },  
    text:{
        fontSize:STYLES.fontSizeNormalText,
        color:Colors.white,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Bold',
    },
    cntMain: {
      flex:1,
      alignItems:'center',
      backgroundColor: Colors.lightGrayEEE,
    },
    cntPDF: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
  },
  pdf: {
      flex:1,
      width:STYLES.widthScreen,
  }
})

