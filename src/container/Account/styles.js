import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

export default styles = StyleSheet.create({
    container: {
      flex:1,
    },
    text: {
      color: Colors.colorWhite,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
      textAlign: "center",
      textAlignVertical: "center"
    },
    bia:{
        width:STYLES.widthScreen,
        height:250*STYLES.heightScreen/640,
    },
    banner:{
      width:STYLES.widthScreen,
      height:250*STYLES.heightScreen/640,
    },
    user:{
      width:80*STYLES.widthScreen/360,
      height:80*STYLES.widthScreen/360,
  },
  imageUser:{
      width:80*STYLES.widthScreen/360,
      height:80*STYLES.widthScreen/360,
      position: 'absolute',
      top: 230*STYLES.heightScreen/640,
      left: 140*STYLES.widthScreen/360,
      borderRadius: 50*STYLES.widthScreen/360,
      overflow: 'hidden',
  }
})

