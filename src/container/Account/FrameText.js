import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

type Props = {
  title:string,
  detail:string,
}
export default class FrameText extends Component<Props> {
  render() {
    const {title,detail}= this.props;  
    return (
     <View style={styles.container}>
            <Text style={[styles.text,{color:Colors.textGray}]}>{title}</Text>
            <Text style={styles.text}>{detail}</Text>
      </View>
    )
  }
}
const styles = StyleSheet.create({
      container: {
        width:STYLES.widthScreen,
        height:55*STYLES.heightScreen/640,
        paddingHorizontal: 10*STYLES.widthScreen/360,
        borderBottomColor: '#8B8989',
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      text: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
      },
})

