/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView
} from 'react-native'
import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { TAI_KHOAN, LIST_INFOR_ACCOUNT ,HO_VA_TEN} from '../../config/default';
import styles from './styles'

//import common
import Header from '../../common/Header';
import FrameText from './FrameText';
type Props = {
    title: string,
}
class Account extends Component<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
            account: {
                hoVaTen: '',
                username: '',
                soDienThoai: '',
            },
        }
    }
    async componentWillMount() {
        this.setState({ account:this.props.Account })
    }
    detailForList=(id)=>{
        const {account} = this.state;
        if (id==0) return account.StaffID;
        if (id==1) return account.StaffName;
        if (id==2) return account.StaffBirthPlace;
        if (id==3) return account.StaffAddr;
        if (id==4) return account.StaffSex==0 ? "Nam" : "Nữ";
        if (id==5) return "Kinh";
        if (id==6) return "Không";
        if (id==7) return "Nhân viên";
        if (id==8) return "Tiến sỹ";
        if (id==9) return "23-05-1998";
        if (id==10) return "23-05-1999";
    }
    render() {
        const { account } = this.state;
        return (
            <ScrollView style={styles.container}>
                <Header
                    title={TAI_KHOAN}
                    navigation={this.props.navigation} />
                <View style={styles.banner}>
                    <Image
                        resizeMode={'stretch'}
                        source={IMAGE.bia} style={styles.bia} />
                </View>
                <View style={styles.imageUser}>
                    <Image
                        source={IMAGE.icon_Chatbot} style={styles.user} />
                </View>
                <View style={{
                    flex: 0,
                    marginTop: 30 * STYLES.heightScreen / 640,
                }}>
                    {
                        LIST_INFOR_ACCOUNT.map((value, index) => {
                            return (
                                <FrameText
                                    key={index}
                                    title={value.title}
                                    detail={this.detailForList(index)}
                                />
                            )
                        })
                    }
                </View>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return { 
      MailBox: state.rootReducer.MailBox ,
      Account: state.rootReducer.Account
    };
  }
  
  export default connect(mapStateToProps, {
  })(Account);