import React, { Component } from 'react'
import {
  StyleSheet,AsyncStorage,
  View
} from 'react-native'
import { connect } from 'react-redux';
//import config
import Colors from '../../config/Colors';
import {LIST_OF_INIT_CHATBOT, MAIL_BOX} from '../../config/default';
//import code
import HomeSmall from '../index';
import global from '../global'
import { FetchJson, GetEventsByTime } from '../../servers/serivce.config';
import {addMail,addEvent} from '../../actions/actionCreators';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state={
      arrayList: LIST_OF_INIT_CHATBOT,
      arrayImage:[],
    }
    global.addList = this.addList.bind(this);
    global.addImage = this.addImage.bind(this);
    global.outImage = this.outImage.bind(this);
    global.outList = this.outList.bind(this);

  }
  async componentWillMount(){
    // const {Account} = this.props;
    // let data_mail=[];
    // let json={
    //   userID:Account.LoginUserID,
    //   time:'',
    //   size:10,
    //   pageindex:1,
    // }
    // await FetchJson(GetEventsByTime,json)
    // .then((res)=>{
    //   data_mail=JSON.parse(res.d).rows;
    // })
    // await AsyncStorage.multiSet([
    //     [MAIL_BOX, JSON.stringify(data_mail)],
    //   ]);
    //   await this.props.addMail(data_mail);
    //   await this.props.addEvent([]);
  }
  addList(value)
  {
    this.setState({arrayList:value});
  }
  addImage(value)
  {
    this.setState({addImage:value});
  }
  outImage(){
    return this.state.arrayImage;
  }
  outList(){
    return this.state.arrayList;
  }
  render() {
    return (
      <View style={[styles.container]}>
        <HomeSmall
            screenProps={this.props.screenProps}
        />
      </View>

    )
  }
}
function mapStateToProps(state) {
  return { 
    MailBox: state.rootReducer.MailBox ,
    Account: state.rootReducer.Account
  };
}

export default connect(mapStateToProps, {
  addMail,addEvent
})(Home);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  }
});
