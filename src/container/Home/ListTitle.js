import React, { Component } from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity,FlatList,AsyncStorage
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { INIT_STORGE } from '../../config/default';

type Props = {
    onButton:Function;
}
var dataForUser=['Tất cả','Mới nhất','Tại đơn vị'];
var dataForNomal=['Tất cả','Mới nhất'];
var dataColor=[
    {colorBnt:Colors.white,colorText:Colors.white},
    {colorBnt:Colors.colorPink,colorText:Colors.white},
    {colorBnt:Colors.colorPink,colorText:Colors.white},
    {colorBnt:Colors.colorPink,colorText:Colors.white},
]
export default class ListTitle extends Component<Props> {
    constructor(props){
        super(props);
        this.state={
            color:dataColor,
            account:null,
        }
    }
    async componentWillMount(){
        await AsyncStorage.multiGet([
            INIT_STORGE
        ], (err, results) => {
            let account=JSON.parse(results[0][1]);
            if (account!=undefined){
                this.setState({account})
            }
        });
    }
changeTitle(id){
        this.scrollView.scrollToIndex({animated: true,index:id});
        let colorFake=[];
        for (let i=0; i < 4; i++){
            colorFake.push({colorBnt:Colors.colorPink,colorText:Colors.white});
        }
        colorFake[id]={colorBnt:Colors.white,colorText:Colors.white}
        this.setState({color:colorFake})
        this.props.onButton(id);
}
  render() {
      const {account} = this.state;
    return (
     <View style={styles.container}>
            <FlatList
                    data={(account!=null && account!=undefined) ? dataForUser : dataForNomal}
                    ref={ref => this.scrollView = ref}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    scrollEnabled={true}
                    style={{flex:1}}
                    contentContainerStyle={{
                    flex:0,
                    justifyContent: 'space-between',
                    alignItems: 'center'}}
                    horizontal={true}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity 
                            onPress={() => this.changeTitle(index)}
                            style={{
                                flex:0,
                                width: (account!=null && account!=undefined) ? 0.33*STYLES.widthScreen : 0.5*STYLES.widthScreen,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                            <View 
                                style={[styles.containerMessager,{borderBottomWidth:0.45,borderBottomColor:this.state.color[index].colorBnt}]}
                               
                                key={index}
                            >
                                <Text style={[styles.text,{color:this.state.color[index].colorText}]}>{item}</Text>
                            </View>
                            </TouchableOpacity>
                        )
                    }}
                />
      </View>
    )
  }
}
const styles = StyleSheet.create({
      container: {
        height: (55 * STYLES.heightScreen / 830),
        width: STYLES.widthScreen,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor:Colors.colorPink,
      },
      text: {
        color: Colors.white,
        fontSize: STYLES.fontSizeNormalText,
        textAlign: "center",
        textAlignVertical: "center"
      },
      containerMessager:{
          flex:0,
          padding: 5,
          height: (45 * STYLES.heightScreen / 830),
          justifyContent: 'center',
          alignItems: 'center',
      }
})

