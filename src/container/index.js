import { DrawerNavigator,StackNavigator} from 'react-navigation';
import React, { Component } from 'react'



import TrangChu from './TrangChu/TrangChu';
import TrangChuNew from './TrangChuNew/TrangChuNew';

import Details from './Details/Details';
import GioiThieu from './GioiThieu/GioiThieu';
import Chatbot from './Chatbot/Chatbot';
import Search from './Search/Search';
import Drawer from './Drawer/Drawer'
import Account from './Account/Account';
import SortJob from './SortJob/SortJob';
import DetailsForJob from './SortJob/DetailsForJob';
import AddEvent from './SortJob/AddEvent';
import ListData from './SortJob/ListData';
import MailBox from './MailBox/MainMailBox/MailBox';
import StudyByThematic from './StudyByThematic/StudyByThematic';
import TestExercises from './StudyByThematic/TestExercises';
import ViewPDF from './StudyByThematic/ViewPDF';
import Target from './Target/Target';
import PartyDocument from './PartyDocument/PartyDocument';
import SeePDF from '../common/SeePDF.js';
import Management from './Management/Management';
import Attendance from './Management/Attendance';
import HomeNew from './HomeNew/HomeNew';
import ListEvent from './ListEvent/ListEvent';
import Result from './Result/Result';
import TablePersion from './Result/TablePersion';
import NewsHot from './NewsHot/NewsHot';
import Admin from './Admin/Admin';
import CheckImplement from './Result/CheckImplement';
import Calendar from './Calendar/Calendar';
const HomeSmall = StackNavigator ({
	TrangChu: {
		screen: ({ navigation,screenProps }) => <HomeNew screenProps={screenProps} navigation={navigation} />,
	},
	News:{
		screen: TrangChuNew,
		navigationOptions: {
			title:'Tin tuc su kien',
		  } 
	},
	ListEvent:{
		screen: ListEvent,
		navigationOptions: {
			title:'Tin tuc su kien',
		  } 
	},
	Admin:{
		screen: Admin,
		navigationOptions: {
			title:'Admin',
		  } 
	},
	Account: { 
		screen: Account,
		navigationOptions: {
			title:'Tai khoan',
		  } 
	},
	NewsHot: { 
		screen: NewsHot,
		navigationOptions: {
			title:'NewsHot',
		  } 
	},
	CheckImplement: { 
		screen: CheckImplement,
		navigationOptions: {
			title:'CheckImplement',
		  } 
	},
	SeePDF: { 
		screen: SeePDF,
		navigationOptions: {
			title:'Xem PDF',
		  } 
	},
	Attendance: { 
		screen: Attendance,
		navigationOptions: {
			title:'Diem danh',
		  } 
	},
	Management: { 
		screen: Management,
		navigationOptions: {
			title:'Quan tri dang vien',
		  } 
	},
	Result: { 
		screen: Result,
		navigationOptions: {
			title:'Kiem tra thuc hien Nghi quyet',
		  } 
	},
	Calendar: { 
		screen: Calendar,
		navigationOptions: {
			title:'Kiem tra thuc hien Nghi quyet',
		  } 
	},
	
	TablePersion: { 
		screen: TablePersion,
		navigationOptions: {
			title:'Kiem tra thuc hien Nghi quyet',
		  } 
	},
	PartyDocument: { 
		screen: PartyDocument,
		navigationOptions: {
			title:'Van kien dang bo',
		  } 
	},
	
	MailBox: { 
		screen: MailBox,
		navigationOptions: {
			title:'MailBox',
		  } 
	},
	ListData: { 
		screen: ListData,
		navigationOptions: {
			title:'ListData',
		  } 
	},
	DetailsForJob: { 
		screen: DetailsForJob,
		navigationOptions: {
			title:'Tai khoan',
		  } 
	},
	SortJob: { 
		screen: SortJob,
		navigationOptions: {
			title:'Tai khoan',
		  } 
	},
	StudyByThematic: { 
		screen: StudyByThematic,
		navigationOptions: {
			title:'Hoc tap theo chuyen de',
		  } 
	},
	TestExercises: { 
		screen: TestExercises,
		navigationOptions: {
			title:'Bai test',
		  } 
	},
	ViewPDF: { 
		screen: ViewPDF,
		navigationOptions: {
			title:'Xem tai lieu PDF',
		  } 
	},
	AddEvent: { 
		screen: AddEvent,
		navigationOptions: {
			title:'Tai khoan',
		  } 
	},
	Target: { 
		screen: Target,
		navigationOptions: {
			title:'Muc tieu can dat',
		  } 
	},
	Search: { 
		screen: Search,
		navigationOptions: {
			title:'Tìm kiếm',
		  } 
	},
	Chatbot: { 
		screen: Chatbot,
		navigationOptions: {
			title:'Chatbot',
		  } 
	},
	Details: { 
		screen: Details,
		navigationOptions: {
			title:'Chi tiết',
		  } 
	},
	GioiThieu: { 
		screen: GioiThieu,
		navigationOptions: {
			title:'Giới thiệu',
		  } 
	 },
}, {
	initialRouteName: 'TrangChu',
	swipeEnabled: true,
	animationEnabled: false,
	headerMode: 'none',
	navigationOptions: {
		header: null
	},
	lazy: true,
	cardStyle: {
		backgroundColor: '#FFF',
		opacity: 1
	},
})
export default HomeSmall;


