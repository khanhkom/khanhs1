import React, { Component } from 'react'
import {
  Text, View, ScrollView,AsyncStorage
} from 'react-native'
//import
import { connect } from 'react-redux';
import {TIN_TUC_SU_KIEN,TOP_TIN_QUAN_TRONG} from '../../config/default';
import styles from './styles'
import {FetchJson, GetNewsByTime} from '../../servers/serivce.config'
//data
import dataHomeNew from '../data/dataHomeNew.json';
//common
import {LoadingComponent} from '../../common/LoadingComponet';
import HeaderReal from '../../common/HeaderReal';
import TypeNew from '../../common/TypeNew/TypeNew';
import ButtonChatbot from '../ButtonChatbot';
type Props = {
  
}
var self;
class TrangChuNew extends Component<Props> {
  constructor(props){
    super(props);
    self=this;
    this.state={
      isLoading: false,
      dataImportant:[],
      pageIndex:1,
    }
  }
  onButtonSearch(){
      
  }
  onLoadData=async (idPage)=>{
    // let json={
    //   userID:this.props.Account.LoginUserID,
    //   time:'',
    //   format:0,
    //   type:0,
    //   size:3,
    //   pageindex:idPage,
    // }
    // await FetchJson(GetNewsByTime,json)
    // .then((res)=>{
    //   let dataImportant=this.state.dataImportant;
    //   let rows=JSON.parse(res.d).rows;
    //   dataImportant=dataImportant.concat(rows);
    //   this.setState({isLoading: false });
    //   this.setState({dataImportant});
    // })
  }
  async componentWillMount()
  {
    this.onLoadData(1);
  }
  render() {
    const {navigation} = this.props;  
    return (
      <View style={styles.container}>
            <HeaderReal
                    title={TIN_TUC_SU_KIEN}
                    onButton={() => this.props.navigation.goBack()}
              />
             <ScrollView
              showsVerticalScrollIndicator={false}
              onMomentumScrollEnd={()=>{
                this.onLoadData(this.state.pageIndex+1);
                this.setState({pageIndex:this.state.pageIndex+1});
              }}
             >   
             <View style={{flex:0,justifyContent:'center',alignItems:'center'}}>
              {
                dataHomeNew.map((value,index)=>{
                      return (
                          <TypeNew
                              key={index}
                              content={value}   
                              navigation={navigation}
                          />
                      )
                  })
              }
             </View>
            </ScrollView>
            <ButtonChatbot
              navigation={this.props.navigation}
            />
            <LoadingComponent isLoading={this.state.isLoading}></LoadingComponent>
      </View>  
  )
  }
}

function mapStateToProps(state) {
  return { MailBox: state.rootReducer.MailBox,
    Account: state.rootReducer.Account
  };
}

export default connect(mapStateToProps, {
})(TrangChuNew);