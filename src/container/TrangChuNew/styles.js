import {
    StyleSheet
  } from 'react-native'
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

export default styles = StyleSheet.create({
  textTitle:{
    color: Colors.colorBlack,
    fontSize: STYLES.fontSize25,
    fontFamily: 'Roboto-Regular',
    fontWeight: 'bold',
    marginTop: 20*STYLES.heightScreen/640,
    marginBottom: 10*STYLES.heightScreen/640,
    paddingLeft:10*STYLES.heightScreen/360,
  },
    container: {
      flex:1,
      backgroundColor:Colors.lightGrayEEE,
      // justifyContent:'center',
      // alignItems:'center',
    },
    line:{
        height:5,
        width:STYLES.widthScreen,
        backgroundColor: '#dddddd',
    },
    loadingContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
  },
})
