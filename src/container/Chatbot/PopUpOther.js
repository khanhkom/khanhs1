import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity
} from 'react-native'
import PopupDialog from 'react-native-popup-dialog';
//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';

type Props = {
    show:any,
    info:any,
    onChangeInfo:Function,
    onChangeShowInfo:Function,
    sendData:Function
}
export default class PopUpOther extends Component<Props> {
    constructor(props){
        super(props)
        this.state={
            check:true,
            checkSendData:false,
        }
    }
  render() {
    const {show,info,onChangeInfo,sendData,onChangeShowInfo}= this.props;  
    const {check,checkSendData}= this.state;  

    return (
        <PopupDialog
        containerStyle={styles.popup}
        show={show}
        width={STYLES.heightScreen * (243 / 640)}
        height={STYLES.widthScreen * (180 / 360)}
        dialogStyle={styles.popupOn}
        onDismissed={()=> {
            if (!checkSendData) sendData('KHÔNG',0);
            onChangeShowInfo(false);
            this.setState({checkSendData:false});
            }}
        dismissOnTouchOutside={true}
        dismissOnHardwareBackPress={false}
      >
        <View style={styles.popupOn}>
          <View style={{ margin: 5 }}></View>
          <Text style={[styles.textSpeak,{color:colors.colorBlack}]}>Mời bạn nhập</Text>
          <View style={{
                    width: 305 * STYLES.widthScreen / 465,
                    height: 50 * STYLES.heightScreen / 830,
                    borderWidth: 1,
                    borderColor: colors.green,
                    marginTop:STYLES.heightScreen * (15 / 640) ,
                    flexDirection: "row"
                }}>
                    <TextInput
                        style={styles.textInputPopup}
                        underlineColorAndroid="transparent"
                        onChangeText={(text) => onChangeInfo(text)}
                        value={info}
                        placeholder="Khác."
                        maxLength={50}
                    />
                </View>
                <View style={{width:STYLES.heightScreen * (243 / 640),marginTop:10,
                                alignItems:'flex-end',marginRight:STYLES.heightScreen * (20 / 640)}}>
                            <TouchableOpacity 
                            style={[styles.btnOk,{backgroundColor : colors.green}]}
                            onPress={() => {
                                onChangeShowInfo(false);
                                this.setState({checkSendData:true})
                                sendData(info,0)
                            }}
                    >
                        <Text style={styles.text}> OK </Text>
                    </TouchableOpacity>
                </View>  
        
        </View>
      </PopupDialog>
    )
  }
}
const styles = StyleSheet.create({
    textInputPopup: {
        color: "black",
        fontSize: STYLES.fontSizeText,
        width: 260 * STYLES.widthScreen / 465,
        textAlignVertical: "center",
        paddingLeft: 10 * STYLES.widthScreen / 465,
        fontFamily: 'Roboto-Regular',
    },
    btnOk:{
        flex:0,
        padding:5,
        paddingHorizontal: 8,
        backgroundColor: colors.red,
        borderRadius:5,
    },
    fullScreen: {
      flex: 1,
      zIndex: 10,
      elevation: 10,
      marginTop: STYLES.heightHeader,
    },
    text:{
        textAlign: 'center',
        textAlignVertical:'center',
        color: 'white',
        fontSize: STYLES.fontSizeChatbot,
        fontFamily: 'Roboto-Regular',
    },
    popup: {
      borderRadius: 0.02 * STYLES.widthScreen,
      //backgroundColor: colors.white,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
    },
    popupOn:{
        borderRadius: 0.02 * STYLES.widthScreen,
        //backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    containerSpeak: {
      marginTop: 22,
      height: STYLES.heightScreen * (243 / 640),
      width: STYLES.widthScreen * (217 / 360),
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
    },
    textSpeak: {
      textAlign: 'center',
      margin: 5,
      fontWeight: 'bold',
      color: 'black',
      fontSize: STYLES.fontSizeChatbot,
      fontFamily: 'Roboto-Regular',
    },
    microPhone: {
      height: STYLES.widthScreen * (217 / 360) / 2,
      width: STYLES.widthScreen * (217 / 360) / 2,
      alignSelf: 'center',
    },
    loadingContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
    },
  })