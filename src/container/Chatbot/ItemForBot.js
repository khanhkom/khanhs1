import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,Linking,TouchableWithoutFeedback,Modal
} from 'react-native'

//import
import ImageViewer from 'react-native-image-zoom-viewer';
import Hyperlink from 'react-native-hyperlink'
//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import {outNumberSmall} from '../../config/Function'
type Props = {
    dataItem:any,
    currentTime:any,
    phut:any,
    imageLink:any,
    openModal:any,
    outImage:any,
    isModalOpened:any,
    setModalOpened:any,
    index:any,
    idImage:any
}
export default ItemForBot = (props: any) => {
    const {idImage,index,dataItem,currentTime,phut,
        imageLink,openModal,outImage,isModalOpened,setModalOpened}= props;  
    return (
        <View style={styles.containerMessagerNone}
        key={index}>
        <Image source={IMAGE.icon_Chatbot} resizeMode={'contain'} style={styles.iconUser}></Image>
        <View style={styles.cntChatbot}>
            <View style={styles.content}>
            <View style={{flex:0}}>
                {
                    dataItem.map((dt,id)=>{
                            return (
                                <Hyperlink 
                                    key={id}
                                    linkStyle={ { color: Colors.bgBlue, textDecorationLine:'underline' }}
                                    onPress={ (url, text) => Linking.openURL(url) }>
                                    <Text style={styles.messager}>{dt}</Text>
                                </Hyperlink>
                            )
                    })
                   
                }
            </View>    
                {
                    (currentTime.getHours() > 12) && <Text style={styles.name}>{outNumberSmall(currentTime.getHours() - 12)}:{phut} PM</Text>
                }
                {
                    (currentTime.getHours() <= 12) && <Text style={styles.name}>{outNumberSmall(currentTime.getHours())}:{phut} AM</Text>
                }
            </View>
            {
                (imageLink[index] != '' && imageLink[index] != null && imageLink[index] != undefined) &&
                <View style={styles.slide} key={index}>
                    <TouchableWithoutFeedback onPress={() =>openModal(index)}>
                        <Image source={{ uri: imageLink[index] }} 
                        resizeMode={'stretch'} style={styles.img}></Image>
                    </TouchableWithoutFeedback>
                </View>
            }
            
            <Modal onRequestClose={()=> setModalOpened(false)} visible={isModalOpened} transparent={true}>
                <ImageViewer imageUrls={outImage(imageLink[idImage])} 
                enableSwipeDown={true}
                onSwipeDown={() => setModalOpened(false)}    
                />
            </Modal>
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
    cntChatbot: {
        flex: 0,
    },
    slide: {
        flex: 0,
    },
    img: {
        width: 0.75 * STYLES.widthScreen,
        height: 0.5 * STYLES.widthScreen,
        borderRadius: 15,
        marginTop: 3,
    },
    text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeChatbot,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center",
    },
    containerMessager: {
        marginTop: 10,
        flex: 0,
        justifyContent: 'center',
        //alignItems: 'center',
    },
    containerMessagerNone: {
        flex: 0,
        flexDirection: 'row',
        marginLeft: 10,
    },
    content: {
        flex: 0,
        justifyContent: 'space-between',
        padding: 8,
        borderRadius: 10,
        backgroundColor: Colors.backgroundColorMesseage,
        alignItems: 'flex-end',
    },
    iconUser: {
        width: 30,
        maxHeight: 30,
        marginRight: 10,
        // marginBottom: 5,
        borderRadius: 30,
    },
    name: {
        color: '#aaaaaa',
        marginTop: 3,
        fontSize: STYLES.fontSizeMiniText,
        fontFamily: 'Roboto-Regular',
        // textAlign: "center",
        // textAlignVertical: "center",
        fontStyle: 'italic',
    },
    messager: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        flexWrap: 'wrap',
        maxWidth: 0.75 * STYLES.widthScreen,
        fontFamily: 'Roboto-Regular',
        // textAlign: "center",
        // textAlignVertical: "center"
    }
})

