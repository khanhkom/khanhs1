import React, { Component } from 'react'
import {
     View, StyleSheet, FlatList, BackHandler,
} from 'react-native'
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';

import ChooseButton from './ChooseButton';
import ItemForBot from './ItemForBot';
import ItemForUser from './ItemForUser';
import Location from './Location';
import ChooseSample from './ChooseSample';


type Props = {
    data_Fake: string,
    showKeyboard: any,
    question: string,
    sendData: Function,
    imageLink: string,
}
var Name = [
    'Chat bot PTIT', 'Người dùng'
];
var self;
export default class PopList extends Component<Props> {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            isModalOpened: false,
            idImage: 0,
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }
    backPressed() {
        self.setState({ isModalOpened: false });
    }
    openModal = (id) => {
        this.setState({ isModalOpened: true, idImage: id })
    }
    setModalOpened = (val) => {
        this.setState({ isModalOpened: val })
    }
    outImage = (link) => {
        let images = [{
            url: link,
            freeHeight: true,
            props: {

            }
        }
        ]
        return images;
    }

    render() {
        const { data_Fake, showKeyboard, sendData, question, imageLink } = this.props;
        const { isModalOpened } = this.state;
        var currentTime = new Date();
        var phut = currentTime.getMinutes() < 10 ? "0" + currentTime.getMinutes() : currentTime.getMinutes();

        return (
            <View style={showKeyboard ? styles.containerKeyboard : styles.container}>
                <FlatList
                    data={data_Fake}
                    ref={ref => this.scrollView = ref}
                    showsVerticalScrollIndicator={false}
                    onContentSizeChange={(contentWidth, contentHeight) => {
                        this.scrollView.scrollToEnd({ animated: true })
                    }}
                    style={{ flex: 1 }}
                    renderItem={({ item, index }) => {
                        let dataItem = (item.user === 0 || item.user === 1) ? item.content.split('@@') : [];
                        return (
                            <View style={styles.containerMessager}>
                                {
                                    (item.user === 0) &&
                                    <ItemForBot
                                        dataItem={dataItem}
                                        currentTime={currentTime}
                                        phut={phut}
                                        index={index}
                                        imageLink={imageLink}
                                        openModal={this.openModal}
                                        outImage={this.outImage}
                                        idImage={this.state.idImage}
                                        isModalOpened={this.state.isModalOpened}
                                        setModalOpened={this.setModalOpened}
                                    />
                                }
                                {
                                    (item.user === 1) &&
                                    <ItemForUser
                                        dataItem={dataItem}
                                        currentTime={currentTime}
                                        phut={phut}
                                    />
                                }
                                {
                                    (item.user != 1 && item.user != 0 && item.user <10) &&
                                    <ChooseButton
                                        data={item.content}
                                        sendData={sendData}
                                        question={question}
                                        type={item.user}
                                    />
                                }
                                {
                                    (item.user == 10) && 
                                    <Location
                                        data={item}
                                    />
                                }
                                {
                                    (item.user == 12) && 
                                    <ChooseSample
                                        data={item}
                                        onButton={()=>RNImmediatePhoneCall.immediatePhoneCall(item.phoneNumber)}
                                    />
                                }
                            </View>

                        )
                    }}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 5,
    },
    containerKeyboard: {
        // height:STYLES.heightScreen*0.535,
        flex: 1,
        marginBottom: 5,
    },
    containerMessager: {
        marginTop: 10,
        flex: 0,
        justifyContent: 'center',
        //alignItems: 'center',
    },
})







