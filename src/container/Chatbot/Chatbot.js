import React, { Component } from 'react'
import {
  StyleSheet,
  View, Animated, Keyboard, ScrollView, StatusBar,
  Image, Text, TextInput, AppState,
} from 'react-native'
//import library
import Voice from 'react-native-voice';
import Tts from 'react-native-tts';
import io from 'socket.io-client/dist/socket.io';
import Loading from 'react-native-spinkit';
//import config
import { SOCKET, FetchJson } from '../../servers/serivce.config';
import Colors from '../../config/Colors';
import { CAC_LINH_VUC_HO_TRO } from '../../config/default'
import { sendDataChild, receiveData } from './function'
//import code
import MicPoup from '../../common/MicPopup';
import { LoadingComponent } from '../../common/LoadingComponet'
import HeaderReal from '../../common/HeaderReal';

import PopUpEmail from './PopUpEmail';
import PopList from './PopList';
import TextVoiceInput from './TextVoiceInput';
import global from '../global';
import PopUpOther from './PopUpOther';
import STYLES from '../../config/styles.config';
var self;

export default class Chatbot extends Component {
  constructor(props) {
    super(props);
    self = this;
    Tts.setDefaultVoice('com.apple.ttsbundle.Moira-compact');
    Tts.setDefaultLanguage('vi');
    this.state = {
      pushPopup: false,
      pushPupupInfo: false,
      info: '',
      popUpNotify: true,
      imageLink: '',
      //state voice
      modalVisible: false,
      bottom: 0,
      speakValue: '',
      selectButton: '',
      // state socket
      idSocket: 0,
      messageSocket: '',
      // question
      bonusQuestion: '',
      data_Fake: global.outList()

    };
    this.dataImageLink = global.outImage();
    this.socket = io(SOCKET, { jsonp: false, transports: ['websocket'] });
    this.socket.on('connect', function () {
      self.setState({ popUpNotify: false });
    });
    this.socket.on('message', function (data) {
      receiveData(self, data);
    });
    Voice.onSpeechResults = this.onSpeechResults.bind(this);
    this.onSpeechStart = this.onSpeechStart.bind(this);
  }
  componentWillMount() {
    self.setState({ bonusQuestion: CAC_LINH_VUC_HO_TRO });
    clearTimeout(this.interval);
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow);
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidHide', this._keyboardWillHide);
    this.backgroundShow = AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillShowListener.remove();
    if (this.backgroundShow != undefined) this.backgroundShow.remove();
  }
  _handleAppStateChange = (nextAppState) => {
    if (AppState.currentState == 'background') {
      self.setState({ popUpNotify: true });
    }
    if (AppState.currentState == 'active') {
      this.socket.emit('check', { msg: "abc" });
      this.socket.on('status', function (data) {
        self.setState({ popUpNotify: false });
      });
    }
  };
  _keyboardWillShow = (event) => {
    this.setState({ bottom: event.endCoordinates.height });
  };

  _keyboardWillHide = (event) => {
    this.setState({ bottom: 0 });

  };
  initVoice = () => {
    this.setState({
      modalVisible: false,
      speakValue: '',
    })
    Voice.start('vi');
    this.setState({ modalVisible: true });
  }
  onSpeechStart() {
    this.initVoice();
    clearTimeout(this.interval);
    this.interval = setTimeout(() => {
      Voice.stop();
      this.setState({ speakValue: '', modalVisible: false });
    }, 7000);

  }
  onSpeechResults(e) {
    this.sendData(e.value[0], 1);
    this.setState({
      speakValue: e.value[0],
    })
    Voice.stop();
  }

  onSetVoice() {
    Voice.stop();
  }
  unableTTS() {
    Tts.stop();
  }
  sendData(data, id) {
    sendDataChild(self, data, id)
  }
  onSetModal() {
    self.setState({ modalVisible: false });
  }
  onChangeEmail(word) {
    self.setState({ email: word });
  }
  onChangeShow(word) {
    self.setState({ pushPopup: word });
  }
  onChangeInfo(word) {
    self.setState({ info: word });
  }
  onChangeShowInfo(word) {
    self.setState({ pushPopupInfo: word });
  }
  render() {
    return (
      <View style={[styles.container]}>

        <HeaderReal
          onButton={() => this.props.navigation.goBack()}
          title={'Chatbot'}
        />
        <MicPoup
          speakValue={this.state.speakValue}
          modalVisible={this.state.modalVisible}
          onSetModal={this.onSetModal}
          onSetVoice={this.onSetVoice}
        ></MicPoup>
        <PopUpEmail
          show={this.state.pushPopup}
          onChangeShow={this.onChangeShow}
          email={this.state.email}
          onChangeEmail={this.onChangeEmail}
          sendData={this.sendData}
        />
        <PopUpOther
          show={this.state.pushPopupInfo}
          onChangeShowInfo={this.onChangeShowInfo}
          info={this.state.info}
          onChangeInfo={this.onChangeInfo}
          sendData={this.sendData}
        />
        <PopList
          data_Fake={this.state.data_Fake}
          showKeyboard={this.state.showKeyboard}
          sendData={this.sendData}
          imageLink={this.dataImageLink}
          question={this.state.bonusQuestion}
        />
        <TextVoiceInput
          unableTTS={this.unableTTS}
          stylesCnt={{ bottom: this.state.bottom }}
          sendData={this.sendData}
          onSpeechStart={this.onSpeechStart}
        />

        <LoadingComponent isLoading={this.state.popUpNotify}></LoadingComponent>
      </View>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cntChatbot: {
    flex: 1,
    width: 345 * STYLES.widthScreen / 360,
    height: 0.93 * STYLES.heightScreen,
    borderRadius: 30,
    backgroundColor: '#fff',
    elevation: 3,
  },
});
