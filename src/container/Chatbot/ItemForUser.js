import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, Linking, TouchableWithoutFeedback, Modal
} from 'react-native'

//import
import ImageViewer from 'react-native-image-zoom-viewer';
import Hyperlink from 'react-native-hyperlink'
//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { outNumberSmall } from '../../config/Function'
type Props = {
    dataItem: any,
    currentTime: any,
    phut: any,
}
export default ItemForUser = (props: any) => {
    const {  dataItem, currentTime, phut } = props;
    return (
        <View style={styles.containerMessagerUser}>
            <View style={styles.contentUser}>
                <View style={{ flex: 0 }}>
                    {
                        dataItem.map((dt, id) => {
                            return (
                                <Hyperlink
                                    key={id}
                                    linkStyle={{ textDecorationLine: 'underline' }}
                                    onPress={(url, text) => Linking.openURL(url)}>
                                    <Text style={[styles.messager, { color: Colors.white }]}>{dt}</Text>
                                </Hyperlink>
                            )
                        })

                    }
                </View>
                {
                    (currentTime.getHours() > 12) && <Text style={[styles.name, { color: Colors.white }]}>{outNumberSmall(currentTime.getHours() - 12)}:{phut} PM</Text>
                }
                {
                    (currentTime.getHours() <= 12) && <Text style={[styles.name, { color: Colors.white }]}>{outNumberSmall(currentTime.getHours())}:{phut} AM</Text>
                }
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    containerMessagerUser: {
        width: STYLES.widthScreen,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    contentUser: {
        flex: 0,
        marginRight: 10,
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        backgroundColor: Colors.bgBlue,
        borderRadius: 15,
        padding: 8,
    },
    messager: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        flexWrap: 'wrap',
        maxWidth: 0.75 * STYLES.widthScreen,
        fontFamily: 'Roboto-Regular',
        // textAlign: "center",
        // textAlignVertical: "center"
    },
    name: {
        color: '#aaaaaa',
        marginTop: 3,
        fontSize: STYLES.fontSizeMiniText,
        fontFamily: 'Roboto-Regular',
        // textAlign: "center",
        // textAlignVertical: "center",
        fontStyle: 'italic',
    },
})

