import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, Button, Linking,TouchableOpacity
} from 'react-native'
//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
type Props = {
    data: any,
    onButton:any,
}
var self;
export default class ChooseSample extends Component<Props> {
    render() {
        const { data,onButton } = this.props;
        return (
            <View style={styles.containerMessagerNone}
            >
                <Image source={IMAGE.icon_Chatbot} resizeMode={'contain'} style={styles.iconUser}></Image>
                <View style={styles.container}>
                    <View style={styles.title}>
                        <Text style={[styles.text, { fontWeight: "bold", flexWrap: "wrap", fontSize: STYLES.fontSizeChatbot }]}>
                            {data.question}
                        </Text>
                        <TouchableOpacity
                            style={{ marginBottom: 5,flex:0 }}
                            onPress={() => onButton()}
                        >
                            <Text style={{ color: Colors.bgBlue, fontWeight: 'bold' }}>
                                {data.content}
                         </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    video: {
        width: STYLES.widthScreen * 0.58,
        height: STYLES.heightScreen * 0.23,
    },
    container: {
        flex: 0,
        //minHeight: STYLES.heightScreen * 0.4,
        width: STYLES.widthScreen * 0.68,
        borderWidth: 0.3,
        borderColor: Colors.gray,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        paddingBottom: 10,
    },
    boderImage: {
        borderRadius: 15,
        width: STYLES.widthScreen * 0.6,
        minHeight: STYLES.heightScreen * 0.3,
    },
    cntImage: {
        borderRadius: 15,
        flex: 0,
        width: STYLES.widthScreen * 0.6,
        minHeight: STYLES.heightScreen * 0.38,
    },
    image: {
        width: STYLES.widthScreen * 0.6,
        height: STYLES.heightScreen * 0.25,
        borderRadius:10
    },
    containerKeyboard: {
        // height:STYLES.heightScreen*0.535,
        flex: 1,
        marginBottom: 5,
    },
    containerMessagerNone: {
        flex: 0,
        flexDirection: 'row',
        marginLeft: 10,

    },
    iconUser: {
        width: 30,
        maxHeight: 30,
        marginRight: 10,
        // marginBottom: 5,
        borderRadius: 30,
    },
    text: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeChatbot,
        marginTop: 15 * STYLES.heightScreen / 640,
    },
    textBtn: {
        color: Colors.bgBlue,
        fontSize: STYLES.fontSizeChatbot,
        textAlign: "center",
        textAlignVertical: "center",
        flexWrap: 'wrap'
    },
    cntButton: {
        flex: 0,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        width: STYLES.widthScreen * 0.68,
        flexWrap: 'wrap',
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    btn: {
        width: 0.68 * STYLES.widthScreen,
        paddingTop: 10,
        paddingBottom: 10,
        paddingHorizontal: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 0.3,
        borderColor: Colors.gray,
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10,
    }

})
