import React, { Component } from 'react'
import {
  Keyboard,
} from 'react-native';
import global from '../global';
import Tts from 'react-native-tts';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import { mapNumberPhone,handspace ,handhttp} from '../../config/Function';

var md5 = require('md5');

export const pushData=(data, id,self) => {
    let dataTmg = self.state.data_Fake;
    dataTmg.push({
      user: id,
      content: data,
    })
    self.setState({ data_Fake: dataTmg });
    global.addList(dataTmg)
}


export const sendDataChild=(self,dataInput, id) => {
    //console.log("====>>>>>Xem co gui duoc gi ko",dataInput);
    self.setState({ idSocket: id, messageSocket: '' });
   if (dataInput.toString().toLowerCase().indexOf("gọi đến") >= 0 ||
      dataInput.toString().toLowerCase().indexOf("gọi điện") >=0 ||
      dataInput.toString().toLowerCase().indexOf("liên lạc") >=0
   ) {
      RNImmediatePhoneCall.immediatePhoneCall("0942432002");
   }
   else if (dataInput.toString().toLowerCase().indexOf("đến ban") >= 0 ||
     dataInput.toString().toLowerCase().indexOf("đi đến") >= 0
   ) {
    pushData(dataInput, 1,self)
    let dataTmg = self.state.data_Fake;
    dataTmg.push({
      user:10,
      question:'Bạn có muốn chúng tôi chỉ đường đến Thái Bình không?',
      location: 'Thái Bình'
    })
    self.setState({ data_Fake: dataTmg });
    global.addList(dataTmg)
   }   
   else if (dataInput !== '') {
     if (id == 1 || id == 0) {
        self.socket.emit('text', { msg: dataInput });
       let str = dataInput;
       if (str.indexOf("COS ") !== 0 && str.indexOf('KHÔNG') !== 0) pushData(dataInput, 1,self);
     }
     if (id == 2) {
       let dataTmg = self.state.data_Fake;
       let lengthData = dataTmg.length;
       dataTmg.splice(lengthData - 1, 1);
       self.setState({ data_Fake: dataTmg });
       global.addList(dataTmg)
       pushData(dataInput.topic, 1,self);
       self.socket.emit('text', { msg: dataInput.idTopic.toString() });
     }
     if (id == 3) {
       let dataTmg = self.state.data_Fake;
       let lengthData = dataTmg.length;
       dataTmg.splice(lengthData - 1, 1);
       self.setState({ data_Fake: dataTmg });
       global.addList(dataTmg)
       if (md5(dataInput.toLowerCase())==md5('khác'))
       {
        self.setState({ pushPopupInfo: true, info: "" });
       }
       else 
       {
         pushData(dataInput, 1,self);
         self.socket.emit('text', { msg: dataInput.toString() });
       }
     }
     if (id == 4) {
       let dataTmg = self.state.data_Fake;
       let lengthData = dataTmg.length;
       dataTmg.splice(lengthData - 1, 1);
       self.setState({ data_Fake: dataTmg });
       global.addList(dataTmg);
       if (md5(dataInput.toLowerCase())===md5('khác'))
       {
        self.setState({ pushPopupInfo: true, info: "" });
       }
       else
       { 
             if (dataInput !== 'Không') {
                self.setState({ pushPopup: true, email: "" });
             }
             else {
                self.socket.emit('text', { msg: 'KHÔNG' });
             }
       }     
     }
   }
   Keyboard.dismiss();
 }

 export const receiveData=(self,data)=> {
    ////////////////////////////////////////////////////////////////TYPE1
    if (data.msg.type == 1) {
      let str = data.msg.message;
      self.dataImageLink[self.state.data_Fake.length] = data.msg.imageLink;
      global.addImage(self.dataImageLink)
      let res = {
        result: str,
        speak: str
      }
      if (str != undefined && str != null && str.indexOf("#") >= 0) res = mapNumberPhone(str);
      if (str != undefined && str != null && str.indexOf("@@") >= 0) {
        res.speak = handspace(res.speak).speak;
      }
      if (str != undefined && str != null && str.indexOf("http") >= 0) {
        res.speak = handhttp(res.speak).speak;
      }
      pushData(res.result, 0,self);
      if (self.state.idSocket === 1 && str != undefined && str != null && res.speak.length < 250) Tts.speak(res.speak);
    }
    ////////////////////////////////////////////////////////////////////TYPE2
     if (data.msg.type == 2) pushData(data.msg.message, 2,self);
    ////////////////////////////////////////////////////////////////////TYPE3
    if (data.msg.type == 3) {
      pushData(data.msg.message, 3,self);
      self.setState({ bonusQuestion: data.msg.question });
    }
    //////////////////////////////////////////////////////////////////TYPE4
    if (data.msg.type == 4) {
      pushData(data.msg.message, 4,self);
      self.setState({ bonusQuestion: data.msg.question });
    }
  }