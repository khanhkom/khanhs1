import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity,KeyboardAvoidingView
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

type Props = {
    sendData:Function,
    onSpeechStart:Function,
    stylesCnt:any,
    unableTTS:Function,
}
export default class TextVoiceInput extends Component<Props> {
  constructor(props){
      super(props);
      this.state={
          text:'',
      }
  }  
  render() {
    const {sendData,onSpeechStart,unableTTS}= this.props;  
    var colorSend = (this.state.text !== '') ? '#16C2FA' : Colors.white;
    var colorTextSend = (this.state.text !== '') ? Colors.white : Colors.gray;
    return (
      <View style={styles.ctn}>
          <KeyboardAvoidingView 
          behavior="padding"
          style={[styles.flex_1]}>
          <TextInput
            ref={ref => this.inputText=ref}
            style={styles.textEmail}
            placeholder='Mời bạn nhập câu hỏi?'
            placeholderTextColor={Colors.gray}
            returnKeyType='next'
            numberOfLines={3}
            multiline={true}
            underlineColorAndroid={'transparent'}
            onChangeText={(text) => {
              
              this.setState({text})}
            }
            value={this.state.text}
          />
          <TouchableOpacity onPress={() => {  unableTTS() ; onSpeechStart(); }}>
            <Image source={IMAGE.micro} resizeMode={'contain'} style={styles.iconMicro}></Image>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={(this.state.text === '')}
            onPress={() => {
                unableTTS();
                sendData(this.state.text, 0);
               this.setState({text:''});
               this.inputText.clear();
            }}
          >
            <Text style={[styles.textSend, { color: colorTextSend, backgroundColor: colorSend }]}>Send</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
    cntMess: {
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      ctn:{
        flex: 0,
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // borderTopWidth: 0.5,
      },
      text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeChatbot,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
      textSend: {
        color: Colors.gray,
        fontWeight: 'bold',
        fontSize: STYLES.fontSizeChatbot,
        fontFamily: 'Roboto-Regular',
        borderWidth: 0.3,
        borderRadius: 3,
        borderColor: Colors.gray,
        padding: 3,
        textAlign: "center",
        textAlignVertical: "center"
      },
      iconMicro: {
        width: 30 / 360 * STYLES.widthScreen,
        height: 30 / 360 * STYLES.widthScreen,
        marginRight: 5,
      },
      textEmail: {
        width: STYLES.widthScreen * 0.75,
        maxHeight: (95 * STYLES.heightScreen / 830) * 0.85,
        paddingTop:5,
        paddingBottom:5,
        flex:0,
        paddingLeft: STYLES.widthScreen * 0.05,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        color: Colors.colorBlack,
      },
      flex_1: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 0.5,
      },
})

