import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import image from '../../assets/image';

type Props = {
    data: any,
    question:string,
    type:any,
    sendData: Function;
}

export default class ChooseButton extends Component<Props> {
    constructor(props) {
        super(props);
    }
    render() {
        const { sendData, data ,question,type } = this.props;
        return (
            <View style={styles.containerMessagerNone}
            >
                <Image source={IMAGE.icon_Chatbot} resizeMode={'contain'} style={styles.iconUser}></Image>
                <View style={styles.container}>
                    <View style={styles.title}>
                        <Text style={[styles.text, { fontWeight: "bold",flexWrap:"wrap",fontSize:STYLES.fontSizeChatbot,fontFamily: 'Roboto-Regular', }]}>
                            {type==2 ? 'Bạn muốn hỏi thông tin về chủ đề nào?' : question }
                        </Text>
                        <Text style={[styles.text, { color:Colors.gray,fontSize:STYLES.fontSizeText,paddingRight:5, fontFamily: 'Roboto-Regular',}]}>
                            {'Hãy chọn những mục bên dưới để Chatbot có thể hỗ trợ bạn tốt hơn.'}
                        </Text>
                    </View>
                    <View style={styles.cntButton}>
                        {
                            data.map((value, index) => {
                                return (
                                    <TouchableOpacity
                                        key={index}
                                        onPress={() => {
                                            (type==2) ? sendData(value, 2) : sendData(value,type);
                                        }
                                        }
                                        style={styles.btn}
                                    >
                                        <Text style={styles.textBtn}>
                                        {type ==2 ? value.topic : value}
                                        </Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        width:STYLES.widthScreen*0.68,
        borderWidth:0.3,
        borderColor: Colors.gray,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius:15,
    },
    containerKeyboard: {
        // height:STYLES.heightScreen*0.535,
        flex: 1,
        marginBottom: 5,
    },
    containerMessagerNone: {
        flex: 0,
        flexDirection: 'row',
        marginLeft: 10,
    },
    iconUser: {
        width: 30,
        maxHeight: 30,
        marginRight: 10,
        // marginBottom: 5,
        borderRadius: 30,
    },
    text: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeChatbot,
        fontFamily: 'Roboto-Regular',
        // textAlign: "center",
        // textAlignVertical: "center"
    },
    textBtn:{
        color:Colors.bgBlue,
        fontSize: STYLES.fontSizeChatbot,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center",
        flexWrap:'wrap'
    },
    cntButton: {
        flex: 0,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        width: STYLES.widthScreen*0.68,
        flexWrap:'wrap',
        padding: 5,
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    btn: {
        width: 0.68 * STYLES.widthScreen,
        paddingTop: 10,
        paddingBottom: 10,
        paddingHorizontal: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth:0.3,
        borderColor: Colors.gray,
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10,
    }

})
