import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import Header from '../../common/Header';

type Props = {
  title:string,
}
export default class Search extends Component<Props> {
  render() {
    return (
     <View style={{flex:1}}>
             <Header navigation={this.props.navigation} />
             <View style={styles.container}>
                <Text style={styles.text}>Không tìm thấy kết quả ...</Text>
             </View>
            
      </View>
    )
  }
}
const styles = StyleSheet.create({
      container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      text: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeLabel,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
      },
})

