/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView
} from 'react-native'
import { List, Checkbox } from 'react-native-paper';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { VAN_KIEN_DANG_BO, LIST_VAN_KIEN,LIST_CONG_VAN } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import ListSection from '../../common/ListSection';

class PartyDocument extends Component<Props> {
    state = {
        expanded: true
    }
    _handlePress = () =>
        this.setState({
            expanded: !this.state.expanded
        });
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={VAN_KIEN_DANG_BO}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView 
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                     <ListSection
                        title={"Các văn kiện của Đảng"}
                        listDocument={LIST_CONG_VAN}
                        navigation={this.props.navigation}
                    />
                    <ListSection
                        title={"Các nghị quyết của Đảng"}
                        listDocument={LIST_VAN_KIEN}
                        navigation={this.props.navigation}
                    />
                    
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(PartyDocument);