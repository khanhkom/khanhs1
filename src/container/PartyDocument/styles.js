import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

export default styles = StyleSheet.create({
    container: {
      flex:1,
      paddingBottom: 20*STYLES.heightScreen/640,
    },
    cntViewPDF:{
      flex:1,
    },
    cntMain: {
      flex:1,
      //alignItems:'center',
    },
    cntPDF: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
  },
  pdf: {
      flex:1,
      width:STYLES.widthScreen,
  }
})

