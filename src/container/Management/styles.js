import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

export default styles = StyleSheet.create({
    cntMain: {
        backgroundColor: Colors.white,
        flex: 1,
        //alignItems:'center',
    },
    content: {
        flex: 0,
        width: 0.55 * STYLES.widthScreen,
        justifyContent: 'center',
    },
    avatar: {
        width: 0.15 * STYLES.widthScreen,
        marginLeft: 0.015 * STYLES.widthScreen,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cntCheckbox: {
        width: 0.2 * STYLES.widthScreen,
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cntInfo: {
        marginTop: 10 * STYLES.heightScreen / 896,
        marginLeft: 15 * STYLES.widthScreen / 360,
        flexDirection: 'row',
        flex: 0,
    },
    cntSmall: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 2 * STYLES.widthScreen / 360,
    },
    Cntcheckbox: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: STYLES.widthScreen * 0.95,
        paddingVertical: 20 * STYLES.heightScreen / 896,
        elevation: 3,
        borderRadius: 15,
        backgroundColor: '#fff',
        marginBottom: 15 * STYLES.heightScreen / 896,
    },
    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: STYLES.fontSizeText,
        marginLeft: 2 * STYLES.widthScreen / 360,
    },
    textSmall: {
        fontFamily: 'Roboto-LightItalic',
        fontStyle: 'italic',
        fontSize: STYLES.fontSizeNormalText,
        marginRight: 5 * STYLES.widthScreen / 360,
    },
    btnSendList: {
        width: STYLES.widthScreen,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20 * STYLES.heightScreen / 640,
    },
    buttonSendList: {
        width: 0.4 * STYLES.widthScreen,
        height: 32 * STYLES.heightScreen / 640,
        elevation: 3,
        borderRadius: 13,
        backgroundColor: Colors.colorPink,
        marginTop: 33 * STYLES.heightScreen / 640
    },
    //khanhthem
    Cntcheckboxs: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: STYLES.widthScreen * (340 / 360),
        elevation: 3,
        borderRadius: 15,
        marginLeft: 10 * STYLES.widthScreen / 360,
        backgroundColor: '#FFFFFF',
        marginTop: 8 * STYLES.heightScreen / 640,
    },
    cntSmalls: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 2 * STYLES.widthScreen / 360,
    },
    cntCheckboxs: {
        width: 34 * STYLES.widthScreen / 360,
    },
    cntInfos: {
        //marginTop: 10*STYLES.heightScreen/896,
        //marginLeft:15*STYLES.widthScreen/360,
        flexDirection: 'row',
        flex: 0,
    },
    texts: {
        fontFamily: 'Roboto',
        fontSize: STYLES.fontSizeText,
        color: '#1C1C1C',
        marginLeft: 4,
        // marginLeft: 15*STYLES.widthScreen/360,
    },
    textSmalls: {
        fontFamily: 'Roboto',
        //fontStyle:'italic',
        color: Colors.white,
        fontSize: STYLES.fontSizeNormalText,
        marginLeft: 2 * STYLES.widthScreen / 360,
    },
    cntBoxCheck: {
        height: STYLES.heightScreen * 23 / 640,
        width: 23 * STYLES.heightScreen / 640,
        marginTop: 13 * STYLES.heightScreen / 640,
        borderRadius: 5,
    },
    cntFrame: {
        width: 305 * STYLES.widthScreen / 360,
        backgroundColor: "#F4F4F4",
        borderRadius: 13,
        flexDirection: 'row',
        marginLeft: 35 * STYLES.widthScreen / 360
    },
    CntAvatar:
        { justifyContent: 'center', alignItems: 'center' },
    cntAvatar: {
        width: 51 * STYLES.heightScreen / 640,
        height: 51 * STYLES.heightScreen / 640
    },
    CntInforss: {
        width: 212 * STYLES.widthScreen / 360,
        paddingBottom: 6 * STYLES.heightScreen / 640,
        marginLeft: -29 * STYLES.heightScreen / 640
    },
    CntIconLock: {
        height: 13 * STYLES.heightScreen / 640,
        width: 10 * STYLES.heightScreen / 640
    },
    cntNumber: {
        width: 19 * STYLES.widthScreen / 360,
        backgroundColor: Colors.white, borderWidth: 1,
        borderColor: "#494949", alignItems: 'center',
        justifyContent: 'center'
    },
})

