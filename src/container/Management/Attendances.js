/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, TouchableOpacity, ScrollView, Text, Alert
} from 'react-native'
import { ListItem, Avatar, Button, CheckBox } from 'react-native-elements'
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { DANH_SACH_THANH_VIEN_THAM_GIA } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import image from '../../assets/image';
var DATA = [
    {
        StaffID: 100,
        LoginUserID: "manh.tq",
        StaffName: "Trần Quang Mạnh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "anh.dv",
        StaffName: "Đinh Văn Anh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "linh.dv",
        StaffName: "Đinh Văn Linh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "khoa.mv",
        StaffName: "Mạc Văn Khoa",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "khoa.vanhoang",
        StaffName: "Hoàng Văn Khoa",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "anh.pv",
        StaffName: "Pham Van Anh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "linh.pt",
        StaffName: "Pham Thi Linh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "dinh.ph",
        StaffName: "Phan Hoang Dinh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },

];
class Attendance extends Component<Props> {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            checked: this.onInit(),
            countNotAbsent: 0,
        }
    }
    onInit = () => {
        let arr = [];
        DATA.map((value, index) => {
            arr.push({
                LoginUserID: value.LoginUserID,
                present: false,
            });
        })
        return arr;
    }
    sendList = () => {
        const { countNotAbsent, checked } = this.state;
        let res = "Quân số " + countNotAbsent + "/" + checked.length + " đồng chí.Bạn có muốn gửi kết quả không?";
        Alert.alert(
            'Thông báo',
            res,
            [
                { text: 'Huỷ', onPress: () => { }, style: 'cancel' },
                {
                    text: 'Gửi kết quả', onPress: () => {
                        this.props.navigation.goBack();
                        Alert.alert('Bạn đã gửi danh sách thành công')
                    }
                },
            ],
            { cancelable: false }
        )

    }
    changeCheck = (id) => {
        let checked = this.state.checked;
        let countNotAbsent = checked[id].present ? this.state.countNotAbsent - 1 : this.state.countNotAbsent + 1;
        checked[id].present = !checked[id].present;
        this.setState({ checked, countNotAbsent })
    }
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={'Điểm danh'}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}
                    contentContainerStyle={{

                    }}
                >
                    {
                        DATA.map((value, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => this.changeCheck(index)}
                                    style={styles.Cntcheckboxs}>
                                    <View style={styles.cntFrame}>
                                        <View style={styles.CntAvatar}>
                                            <View style={styles.cntAvatar}>
                                                <Avatar
                                                    rounded
                                                    size="medium"
                                                    source={{
                                                        uri:
                                                            value.ImageFile
                                                    }}
                                                    containerStyle={{ width: 51 * STYLES.heightScreen / 640, position: 'absolute',
                                                     height: 51 * STYLES.heightScreen / 640, left: -38 * STYLES.heightScreen/ 640 }}
                                                />
                                            </View>
                                        </View>
                                        <View style={{        width: 212 * STYLES.widthScreen / 360,
        paddingBottom: 6 * STYLES.heightScreen / 640,
        marginLeft: -29 * STYLES.heightScreen / 640}}>
                                            <View style={{ width: 210 * STYLES.widthScreen / 360 }}>
                                                <Text style={styles.texts}>{value.StaffName}</Text>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={IMAGE.iconlock} style={styles.CntIconLock} />
                                                    <Text style={[styles.texts, { fontStyle: 'italic', color: '#494949' }]}>{value.LoginUserID}</Text>
                                                </View>
                                                <View style={styles.cntInfos}>
                                                    <View style={{
                                                        marginTop: 4,
                                                        //marginLeft: 15 * STYLES.widthScreen / 360,
                                                        flexDirection: 'row',
                                                        flex: 0,
                                                    }}>
                                                        <View style={{ backgroundColor: "#494949", paddingRight: 2 * STYLES.widthScreen / 360, alignItems: 'center', justifyContent: 'center' }}>
                                                            <Text style={styles.textSmalls}>Tổng số lần vắng họp</Text>
                                                        </View>
                                                        <View style={styles.cntNumber}>
                                                            <Text style={STYLES.textSmall}>15</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: 43 * STYLES.widthScreen / 360, alignItems: 'center', marginLeft: 29 * STYLES.heightScreen / 640 }}>
                                            <TouchableOpacity
                                                onPress={() => this.changeCheck(index)}
                                                style={[styles.cntBoxCheck, { backgroundColor: this.state.checked[index].present ? '#1BD18F' : 'white' }]}>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        })
                    }
                    <Button
                        title="Gửi danh sách"
                        onPress={() => {
                            this.sendList();
                        }}
                        containerStyle={styles.btnSendList}
                        buttonStyle={styles.buttonSendList}
                    />
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(Attendance);