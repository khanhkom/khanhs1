"use strict";
import {
    StyleSheet,Dimensions
} from 'react-native';

//import config
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
const { width, height } = Dimensions.get('window');
var widthDrawer = (height / width > 1.5) ? width : 41 * width / 68;
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.colorPink,
    },
    viewSample: {
        height: STYLES.heightHeader * 0.8,
        width: widthDrawer * 400 / 750,
        flexDirection: "row", alignItems: "center"
    },
    viewLine: {
        height: 1,
        width: widthDrawer * 400 / 750,
        backgroundColor: Colors.white
    },
    imageSample: {
        width: 0 * widthDrawer / 465, height: 0 * widthDrawer / 465
    },
    textSample: {
        marginLeft: 25 * widthDrawer / 750,
        color: Colors.yellowText,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        width: widthDrawer * 400 / 750 - 20 * widthDrawer / 465 * 0.8,
    },
    textMail:{
        marginLeft: 25 * widthDrawer / 750,
        color: Colors.yellowText,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        flex:0,
    },
    viewSpace: {
        height: STYLES.heightHeader * 0.8
    },
    viewMid: {
        height: STYLES.heightHeader * 0.5
    },
    drawerHeader: Object.assign(
        STYLES.centerItem,
        { width: '100%', height: 'auto' }
    ),
    iconAvatar: {
        width: 80 * widthDrawer / 450,
        height: 80 * widthDrawer / 450,
        backgroundColor: Colors.colorWhite,
    },
    imageAvatar: {
        height: 200 * STYLES.heightHeader / 120,
        backgroundColor: Colors.colorWhite,
    },
    iconDrawer: {
        width: 25,
        height: 25,
    },
    buttonContainer: {
        position: 'relative',
        flexDirection: 'row',
        margin: 15,
    },
    textButton: {
        color: Colors.colorBlack,
        marginLeft: 5,
    },
    separator: {
        backgroundColor: Colors.colorGray,
        width: '100%',
        height: 1,
    },
    activeImage: {
        width: '100%',
        height: null,
        flex: 1,
    },
    textSmartCom: {
        color: Colors.yellowText,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        fontWeight: "bold"
    },
    cntSlMail:{
            marginLeft: 30*STYLES.widthScreen/360,
            // backgroundColor:'white',
            borderRadius:20*STYLES.widthScreen/360,
            width:20*STYLES.widthScreen/360,
            height:20*STYLES.widthScreen/360,
            justifyContent: 'center',
            alignItems:'center'
    },
    slMail:{
        color: Colors.yellowText,
        fontSize: STYLES.fontSizeSubText,
        fontFamily:'Roboto-Medium'
    },
    textEmail: {
        fontSize: STYLES.fontSizeSubText,
    }
});