"use strict";
import React from 'react';
import {
    View, StyleSheet, ScrollView, AsyncStorage,
    Image, Text, TouchableOpacity, Dimensions, Alert
} from 'react-native'
import { SafeAreaView } from 'react-navigation'
import { connect } from 'react-redux';

//import config
import { calArrMail, countMainNotSeen } from '../../config/Function';
import styles from './styles';
import {
    INIT_STORGE, dataGioiThieu, LIST_OF_DRAWER,GIOI_THIEU_DRAWER,
    DANG_XUAT, DANG_NHAP, ADD_EVENT, MAIL_BOX, TIN_TUC_SU_KIEN,DANG_VIEN,CHI_TIEU_CAN_DAT, SET_LESSSON
} from '../../config/default'
import ItemDrawer from '../../common/ItemDrawer';
import AvatarDrawer from '../../common/AvatarDrawer';
import ItemDropDown from '../../common/ItemDropDown';

type Props = {
    navigation: Function,
};
export class Drawer extends React.PureComponent<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
            hoVaTen: '',
            account: null,
            dataMailBox: [],
            mailNotSeen: 0,
        }
    }
    onLogout = async () => {
        const { navigation } = this.props.screenProps;
        await AsyncStorage.multiRemove([INIT_STORGE, ADD_EVENT, MAIL_BOX,SET_LESSSON]);
        navigation.navigate('Intro');
    }
    functionForButton = (value,id) => {
        const { navigation } = this.props;
        const { account } = this.state;
        if (id == 0) navigation.navigate('SortJob');
        if (id == 1) {
            navigation.navigate('StudyByThematic');
                //hoc tap chuyen de StudyByThematic
        }
       
        if (id == 2) navigation.navigate('MailBox', {
            title: value.text,
            data: calArrMail(this.props.MailBox),
            account: account
        })
        if (id == 3) navigation.navigate('Account');
        if (id == 4) navigation.navigate('PartyDocument');
        if (id == 5) navigation.navigate('Management');
        navigation.closeDrawer();
    }
    async componentWillMount() {
        await AsyncStorage.multiGet([
            INIT_STORGE, MAIL_BOX
        ], (err, results) => {

            let dataMailBox = this.props.MailBox;
            let account = JSON.parse(results[0][1]);
            if (dataMailBox != undefined) {
                let count = 0;
                dataMailBox = calArrMail(dataMailBox)
                for (let i = 0; i < dataMailBox.length; i++)
                    if (dataMailBox[i].seen == false) count++;
                this.setState({ dataMailBox: dataMailBox, mailNotSeen: count });
            }
            if (account != undefined) {
                this.setState({ hoVaTen: account.StaffName, account: account })
            }
        });
    }
    onDropdown=()=> {
        this.setState({
            dropdown: !this.state.dropdown,
        })
    }
    render() {
        const { navigation } = this.props;
        const { account } = this.state;
        let textEnd = (account != null && account != undefined) ? DANG_XUAT : DANG_NHAP;
        return (
            <ScrollView style={styles.container}>
                <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
                    <AvatarDrawer
                        onButton={()=>navigation.navigate('Account')}
                        name={this.state.hoVaTen}
                        position={DANG_VIEN}
                    />
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <ItemDropDown
                            text={GIOI_THIEU_DRAWER}
                            data={dataGioiThieu}
                            dropdown={this.state.dropdown}
                            onButton={this.onDropdown}
                            linkImage={''}
                        />
                        <ItemDrawer
                            text={CHI_TIEU_CAN_DAT}
                            linkImage={''}
                            onButton={()=>{
                                navigation.closeDrawer();
                                navigation.navigate('Target')
                            }}
                        />
                        <ItemDrawer
                            text={TIN_TUC_SU_KIEN}
                            linkImage={''}
                            onButton={()=>{
                                navigation.closeDrawer();
                                navigation.navigate('TrangChu')
                            }}
                        />
                        {(account != null && account != undefined) && <View style={{ flex: 0 }}>
                            {
                                LIST_OF_DRAWER.map((value, index) => {
                                    return (
                                        <ItemDrawer
                                            key={index}
                                            text={value.text}
                                            linkImage={value.linkImage}
                                            onButton={() => this.functionForButton(value,index)}
                                        >
                                            {
                                                ( index==2) &&
                                                <View style={styles.cntSlMail}>
                                                    <Text style={styles.slMail}>
                                                        {'10+'}
                                                    </Text>
                                                </View>
                                            }
                                        </ItemDrawer>
                                    )
                                })
                            }
                        </View>
                        }
                        <ItemDrawer
                            text={textEnd}
                            linkImage={''}
                            onButton={this.onLogout}
                        />
                    </View>
                </SafeAreaView>
            </ScrollView>
        );
    }
}
function mapStateToProps(state) {
    return { 
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account
     };
}

export default connect(mapStateToProps, {

})(Drawer);

