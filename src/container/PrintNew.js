import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity,RefreshControl,ScrollView
} from 'react-native'
import Loading from 'react-native-spinkit';

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import Colors from '../config/Colors';
import TypeOne from './TypeNews/TypeOne';
import TypeTwo from './TypeNews/TypeTwo';
import TypeThree from './TypeNews/TypeThree';
import ButtonChatbot from './ButtonChatbot';
import Header from '../common/Header';
import InternetManager from './InternetManager/InternetManager';
import ListTitle from './Home/ListTitle';

//data
type Props = {
  data:any,
  dataBonus:any,
  navigation:Function,
  onButton:Function,
}
var self;
export default class PrintNew extends Component<Props> {
  constructor(props){
    super(props);
    self=this;
    this.state={
      isLoading: false,
      isInternet:false,
    }
    InternetManager.default();
    InternetManager.networkChecker((isConnected) => {
        if (!isConnected) InternetManager.notificationEnableNetwork();
        else this.setState({isInternet:true});
    })
    this._onRefresh=this._onRefresh.bind(this);
  }
  renderList(){
    if (!this.state.isInternet) return;
    const {data,onButton}=this.props;  
    let list=[];
    for (let id=0; id < data.length; id++)
    {
      if (id % 5 == 0)
      {
          list.push(
            <TypeOne
              title={data[id].title}
              time={data[id].time}
              details={data[id].details}
              author={data[id].author}
              sourceImage={data[id].sourceImage}
              navigation={this.props.navigation} 
          />
          )
          if (id==0) list.push(
             <ListTitle
                  onButton={onButton}
             />
            )
      }
      if (id % 5 == 1)
      {
        list.push(
          <TypeTwo
             title={data[id].title}
              time={data[id].time}
              details={data[id].details}
              author={data[id].author}
              sourceImage={data[id].sourceImage}
              navigation={this.props.navigation} 
        />
        )
        list.push(
          <TypeTwo
             title={data[id+1].title}
              time={data[id+1].time}
              details={data[id+1].details}
              author={data[id+1].author}
              sourceImage={data[id+1].sourceImage}
              navigation={this.props.navigation} 
        />
        )
        list.push( <View style={styles.line}/>)
      }
      if (id % 5==3)
      {
        list.push(
          <View style={{ flexDirection: 'row',flex:1,justifyContent:'center',alignItems:'center' }}>
          <TypeThree
             title={data[id].title}
              time={data[id].time}
              author={data[id].author}
              details={data[id].details}
              sourceImage={data[id].sourceImage}
              navigation={this.props.navigation} 
          />
          <TypeThree
             title={data[id+1].title}
              time={data[id+1].time}
              details={data[id+1].details}
              author={data[id+1].author}
              sourceImage={data[id+1].sourceImage}
              navigation={this.props.navigation} 
          />
        </View>
        )
        list.push( <View style={styles.line}/>)
      }
    }
    return list;
  }
  renderBonusList(){
    if (!this.state.isInternet) return;
    const {dataBonus}=this.props;  
    let list=[];
    list.push(
      <View style={{width:STYLES.widthScreen,height:30*STYLES.heightScreen/830,
      borderBottomColor:Colors.gray,borderBottomWidth:0.5,
      justifyContent:'center',
      }}>
         <Text style={{marginLeft:10,color:Colors.gray}}>Tin khác</Text>
      </View>
    )
    for (let id=0; id < dataBonus.length; id++){
      list.push(
        <TypeTwo
             title={dataBonus[id].title}
              time={dataBonus[id].time}
              details={dataBonus[id].details}
              author={dataBonus[id].author}
              sourceImage={dataBonus[id].sourceImage}
              navigation={this.props.navigation} 
        />
      )
    }
    return list;
  }
  _onRefresh(){
    this.setState({isLoading: true});
    InternetManager.default();
    InternetManager.networkChecker((isConnected) => {
        if (!isConnected) InternetManager.notificationEnableNetwork();
        else this.setState({isInternet:true});
        this.setState({isLoading: false});
  })
  }
  render() {
    return (
      <View style={styles.container}>
           <ScrollView 
              refreshControl={
                <RefreshControl
                  colors={["red", "red", "red"]}
                  refreshing={this.state.isLoading}
                  onRefresh={this._onRefresh}
                />
              }
              style={styles.container}>
           {
             this.renderList()
           }
           {
             this.renderBonusList()
           }
          </ScrollView>
          <ButtonChatbot
            navigation={this.props.navigation}
          />
      </View>  
  )
  }
}
const styles = StyleSheet.create({
      container: {
        flex:1,
        // justifyContent:'center',
        // alignItems:'center',
      },
      line:{
          height:5,
          width:STYLES.widthScreen,
          backgroundColor: '#dddddd',
      },
      loadingContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
})

