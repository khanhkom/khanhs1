import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,ScrollView,
} from 'react-native'
import HTML from 'react-native-render-html';
//import
import Header from '../../common/Header';
import ButtonChatbot from '../ButtonChatbot';

type Props = {
 
}
export default class GioiThieu extends Component<Props> {
  render() {
    const CUSTOM_STYLES = {
    };
    const CUSTOM_RENDERERS = {
      htm: (htmlAttribs, children, convertedCSSStyles, passProps) => {
          return (
            <View style={{flex:0, marginLeft: 10}}>
                <Text style={styles.textTitle}>{children}</Text>
                <View style={styles.cntNote}>
                      <Text style={styles.textAuthor}>{'Bưu chính'}</Text>  
                      <Text style={styles.textTime}>{"28-10-2018"}</Text>
              </View>
            </View>
          )
      },
    }
    const DEFAULT_PROPS = {
      htmlStyles: CUSTOM_STYLES,
      renderers: CUSTOM_RENDERERS,
      onLinkPress: (evt, href) => { Linking.openURL(href); },

    };
    return (
     <View style={{flex:1}}>
         <Header navigation={this.props.navigation} />
         <ScrollView style={styles.container}>
            <HTML 
              {...DEFAULT_PROPS}
              html={this.props.navigation.getParam('details')} 
            />
            <View style={styles.line}/>
        </ScrollView>
        <ButtonChatbot
            navigation={this.props.navigation}
          />
     </View> 
    )
  }
}

