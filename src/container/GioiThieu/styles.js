import {
   StyleSheet, 
} from 'react-native'
//import
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
export default styles = StyleSheet.create({
    container: {
      flex:1,
      paddingTop: 30,
      padding:15,

    },
    line:{
        height:50,
    },
    cntImage:{
      width:STYLES.widthScreen*0.85,
    },
    textTitle:{
      color: Colors.colorRed,
      fontSize: 28,
      fontFamily: 'Roboto-Regular',
      //textAlign: "center",
      fontWeight:'bold',
      //textAlignVertical: "center",
      marginBottom: 10,
    },
    text: {
      color: Colors.colorBlack,
      fontSize: STYLES.fontSizeLabel,
      fontFamily: 'Roboto-Regular',
      letterSpacing:0.6,
      // textAlign: "center",
      // textAlignVertical: "center"
    },
    textBold:{
      color: Colors.colorBlack,
      fontSize: STYLES.fontSizeLabel,
      fontFamily: 'Roboto-Regular',
      letterSpacing:0.6,
      // textAlign: "center",
      // textAlignVertical: "center",
      fontWeight: 'bold',
    },
    textTime: {
      color: Colors.grey,
      fontSize: STYLES.fontSizeNormalText,
      fontFamily: 'Roboto-Regular',
      fontStyle: 'italic',
    },
    textAuthor: {
      color: Colors.colorWhite,
      fontSize: STYLES.fontSizeNormalText,
      padding: 3,
      backgroundColor: Colors.blueNew,
      fontFamily: 'Roboto-Regular',
      borderRadius: 2,
      marginRight: 3,
    },
    cntNote:{
        flexDirection: 'row',
        flex:0,
        marginBottom: 10,
        //justifyContent: 'center',
        alignItems: 'center',
    }
})
