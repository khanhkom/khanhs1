import React from 'react'
import {
   View, ScrollView,Text,TouchableOpacity
} from 'react-native'
import Entypo from 'react-native-vector-icons/Entypo';
import STYLES from '../../../config/styles.config';
import styles from './stylesDetails';
type Props = {
  sender:string,
  receiver:string,
  onReply:Function,
  data:any,
}
export default MainDetailsMail = (props: any) => {
    const {sender,receiver,onReply,data}= props;  
    return (
        <ScrollView style={{flex:1}}>
                <View style={styles.cntMain}>
                      <View style={styles.cntUser}>
                          <View style={{flex:0}}>
                            <Text style={styles.textUser}>
                                Từ: {sender}
                            </Text>  
                            <Text style={styles.textUser}>
                                Đến: {receiver}
                            </Text> 
                          </View>  
                          <TouchableOpacity
                            onPress={()=>onReply()}
                              style={styles.reply}
                          >
                                <Entypo name="reply" size={26 * STYLES.heightScreen / 640} color="#0000" />
                          </TouchableOpacity>
                      </View>

                      <View style={styles.cntTitle} >
                         <Text style={styles.textTitle}>
                            {data.title}
                        </Text> 
                        <Text style={styles.textTime}>
                            {data.time}
                        </Text> 
                      </View>

                      <View style={styles.cntDetails} >
                        <Text style={styles.text}>
                                {data.details}
                            </Text> 
                      </View>
                </View>
        </ScrollView>
    )
}

