import {
  StyleSheet, 
  
} from 'react-native'

//import
import STYLES from '../../../config/styles.config';
import colors from '../../../config/Colors';

export default styles = StyleSheet.create({
    container: {
      flex:1,
    },
    cntReply:{
        width:STYLES.widthScreen,
        paddingHorizontal: 10*STYLES.widthScreen/360,
        marginBottom:30*STYLES.heightScreen/640,
        flexDirection:'row',
        justifyContent:'space-between',
    },
    reply:{
        marginRight:10*STYLES.widthScreen/360
    },
    text: {
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    cntMain:{
        justifyContent:'center',
        alignItems: 'center',
        flex:1,
        marginBottom: 20*STYLES.heightScreen/640,
    },
    cntUser:{
        width:0.9*STYLES.widthScreen,
        height:75*STYLES.heightScreen/640,
        marginTop: 8 * STYLES.heightScreen / 640,
        justifyContent: 'space-between',
        alignItems: 'center',
         borderBottomColor: '#E2E2E2',
         borderBottomWidth: 1,
         flexDirection: 'row',
    },
    textUser:{
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
      marginTop: 5 * STYLES.heightScreen / 640,
      fontFamily: 'Roboto-Regular',
    },
    cntTitle:{
      width:0.9*STYLES.widthScreen,
      minHeight:75*STYLES.heightScreen/640,
      marginTop: 8 * STYLES.heightScreen / 640,
      justifyContent: 'center',
    },
    textTitle:{
      color: colors.colorBlack,
      fontWeight:'bold',
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    textTime:{
      color: colors.lightGray,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    cntDetails:{
        flex:0,
        marginTop: 8 * STYLES.heightScreen / 640,
        width:0.9*STYLES.widthScreen,
    },
})

