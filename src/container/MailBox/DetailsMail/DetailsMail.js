import React, { Component } from 'react'
import {
  Text, View, ScrollView,
  TouchableOpacity,BackHandler
} from 'react-native'

//import
import styles from './stylesDetails';
import {TRA_LOI,CHUYEN_TIEP,SCREEN_DETAILS_EMAIL,SCREEN_SOAN_EMAIL} from '../../../config/default'
import HeaderReal from '../../../common/HeaderReal';
import ButtonIcon from '../../../common/ButtonIcon';
import ReplyMail from '../ReplyMail/ReplyMail';
import MainDetailsMail from './MainDetailsMail';


type Props = {
  title:string,
  data:any,
  onBack:Function,
  account:string,
}
export default class DetailsMail extends Component<Props> {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      movingScreen:SCREEN_DETAILS_EMAIL,
      userReceive:undefined,
      titleReply:TRA_LOI,
      contentMail:undefined,
    }
  }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
      }
    
      handleBackPress = async () => {
        await this.props.onBack()
        return true;
      }

      onChangeScreen=(val)=>{
        this.setState({movingScreen:val})
      }
      onReply=()=>{
        this.setState({
          movingScreen:SCREEN_SOAN_EMAIL,
          titleReply:TRA_LOI,
          userReceive:this.props.data.sender,
          contentMail:undefined  
        })
      }
      
      onForward=()=>{
        this.setState({
          movingScreen:SCREEN_SOAN_EMAIL,
          titleReply:CHUYEN_TIEP,
          userReceive:undefined,
          contentMail:this.props.data  
        })
      }
  render() {
      const {onBack,title,data,account} = this.props;
    if (this.state.movingScreen===SCREEN_DETAILS_EMAIL)
    return (
     <View style={styles.container}>
        <HeaderReal
          title={title}
          onButton={() => onBack()}
        />
        <MainDetailsMail
            onReply={this.onReply}
            data={data}
            sender={data.sender}
            receiver={account.LoginUserID}
        />
        <View style={styles.cntReply}>  
                      <ButtonIcon
                          title={TRA_LOI}
                          nameIcon={'reply'}
                          onButton={this.onReply}
                      />
                      <ButtonIcon
                          title={CHUYEN_TIEP}
                          nameIcon={'forward'}
                          onButton={this.onForward}
                      />
          </View>
      </View>
    )
  if (this.state.movingScreen==SCREEN_SOAN_EMAIL) return (
    <ReplyMail
        userReceive={this.state.userReceive}
        userSend={account.StaffName}
        account={account}
        onBack={() => this.onChangeScreen(SCREEN_DETAILS_EMAIL)}
        title={this.state.titleReply}
        contentMail={this.state.contentMail}
    />
  )
  }
}