import {
    StyleSheet, 
    
  } from 'react-native'
  
  //import
import STYLES from '../../../config/styles.config';
import colors from '../../../config/Colors';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    iconAdd: {
      fontSize: 20,
      height: 22,
      color: 'white',
    },
    text: {
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
      textAlign: "center",
      textAlignVertical: "center"
    },
    cntMain: {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      marginBottom: 20 * STYLES.heightScreen / 640,
    },
    cntDot: {
      flexDirection: 'row',
      flex: 0,
      alignItems: 'center',
    },
    btnMail: {
      flexDirection: 'row', 
      alignItems: 'center', 
      backgroundColor: 'white', width: STYLES.widthScreen, 
      borderBottomColor: '#eee', 
      borderBottomWidth: 1
    },
    cntSender: {
      flexDirection: 'row', 
      justifyContent: 'space-between', 
      alignItems: 'flex-start',
      flex:0,
    },
    dotFalse: {
      backgroundColor: '#0284FE',
      marginRight: 5 * STYLES.widthScreen / 360,
      height: 5 * STYLES.widthScreen / 360,
      width: 5 * STYLES.widthScreen / 360,
      borderRadius: 5 * STYLES.widthScreen / 360,
    },
    dotTrue: {
      marginRight: 5 * STYLES.widthScreen / 360,
      height: 5 * STYLES.widthScreen / 360,
      width: 5 * STYLES.widthScreen / 360,
    },
    textSender: {
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
    },
    textTime: {
      color: '#3b60c4',
      fontSize: STYLES.fontSizeNormalText,
      textAlign: "center",
      textAlignVertical: "center",
    },
    textTitle: {
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
    },
    textDetails: {
      color: 'gray',
      fontSize: STYLES.fontSizeText,
    }
  })