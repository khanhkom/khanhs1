import React, { Component } from 'react'
import {
  View,  AsyncStorage
} from 'react-native'
import { connect } from 'react-redux';

import { CHI_TIET_THU, MAIL_BOX ,SCREEN_MAILBOX,SCREEN_SOAN_EMAIL,SCREEN_DETAILS_EMAIL} from '../../../config/default';
//import common
import DetailsMail from '../DetailsMail/DetailsMail';
import { addMail } from '../../../actions/actionCreators'
import ReplyMail from '../ReplyMail/ReplyMail';
import MainMailBox from './MainMailBox';
import {LoadingComponent} from '../../../common/LoadingComponet';
import { FetchJson, SetEventsByTime, GetEventsByTime } from '../../../servers/serivce.config';
import { calArrMail } from '../../../config/Function';
type Props = {
  title: string,
}
export class MailBox extends Component<Props> {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      movingScreen: SCREEN_MAILBOX,
      detailMail: undefined,
      pageIndex:0,
      listMail:[],
      isLoading:true
    }
  }
  async componentWillMount(){
    await this.onLoadData();
  }
  onLoadData=async()=>{
    let listMail=this.props.MailBox;
    if (listMail==undefined || listMail==null || this.state.pageIndex ==0 ) listMail=[];
    let json={
      userID:this.props.navigation.getParam('account').LoginUserID,
      time:'',
      size:10,
      pageindex:this.state.pageIndex+1,
    }
    await FetchJson(GetEventsByTime,json)
    .then((res)=>{
      this.setState({isLoading: false });
      let fliter=calArrMail(JSON.parse(res.d).rows);
       listMail=listMail.concat(fliter);
       this.setState({listMail})
    })
    // await AsyncStorage.multiSet([
    //     [MAIL_BOX, JSON.stringify(listMail)],
    //   ]);
    console.log("====>>>>List mail",listMail);
    await this.props.addMail(listMail);
  }
  onChangePageIndex=()=>{
        this.setState({pageIndex:this.state.pageIndex+1})
  }
  onChangeMovingSceen = (val) => {
    this.setState({ movingScreen: val });
  }
  onButtonMail = async (val, id) => {
    let arr = this.props.MailBox;
    if (arr==undefined || arr==null) arr=[];
    arr[id].seen=true;
    await this.props.addMail(arr);
    await AsyncStorage.multiSet([
      [MAIL_BOX, JSON.stringify(arr)],
    ]);

    let json={
      userID:this.props.navigation.getParam('account').LoginUserID,
      eventsid:arr[id].idmessage,
      status:1,
    }
    await FetchJson(SetEventsByTime,json)
    .then((res)=>{
      
    })
    await this.setState({ detailMail: val })
    await this.setState({ movingScreen: SCREEN_DETAILS_EMAIL });
  }
  render() {
    const account = this.props.navigation.getParam('account');
    const title = this.props.navigation.getParam('title')
    if (this.state.movingScreen == SCREEN_MAILBOX)
      return (
        <View style={{flex:1}}>
            <MainMailBox
                onBack={() => this.props.navigation.goBack()}
                title={title}
                onLoadData={this.onLoadData}
                onChangePageIndex={this.onChangePageIndex}
                data={this.state.listMail}
                onChangeMovingSceen={this.onChangeMovingSceen}
                onButtonMail={this.onButtonMail}
            />
            <LoadingComponent isLoading={this.state.isLoading}></LoadingComponent>
        </View>
      )
    if (this.state.movingScreen==SCREEN_DETAILS_EMAIL)
      return (
        <DetailsMail
          onBack={() => this.onChangeMovingSceen(SCREEN_MAILBOX)}
          title={CHI_TIET_THU}
          account={account}
          data={this.state.detailMail}
        />
      )
    if (this.state.movingScreen==SCREEN_SOAN_EMAIL)
      return (
        <ReplyMail
        userReceive={undefined}
        userSend={account.hoVaTen}
        account={account}
        onBack={() => this.onChangeMovingSceen(SCREEN_MAILBOX)}
        title={SCREEN_SOAN_EMAIL}
        contentMail={undefined}
        />
      )
  }
}
function mapStateToProps(state) {
  return { MailBox: state.rootReducer.MailBox };
}

export default connect(mapStateToProps, {
  addMail
})(MailBox);



