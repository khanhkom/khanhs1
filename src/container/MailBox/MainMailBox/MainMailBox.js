import React from 'react'
import {
  View, ScrollView, Text, TouchableOpacity
} from 'react-native'
import ActionButton from 'react-native-action-button';
import { Avatar } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import STYLES from '../../../config/styles.config';
import styles from './stylesMailBox';
import HeaderReal from '../../../common/HeaderReal';
import { SCREEN_SOAN_EMAIL } from '../../../config/default';
import HeaderMail from '../../../common/HeaderMail';
import image from '../../../assets/image';
import { mapToYearMonthDay } from '../../../config/Function';

type Props = {
  onBack: string,
  title: string,
  data: any,
  onChangeMovingSceen: Function,
  onButtonMail: Function,
  onLoadData: Function,
  onChangePageIndex: Function,
}
export default MainMailBox = (props: any) => {
  const { onBack, onChangeMovingSceen, onButtonMail, data, title, onLoadData, onChangePageIndex } = props;
  return (
    <View style={styles.container}>
      <HeaderMail
        title={title}
        onButtonLeft={() => onBack()}
        onButtonRight={() => { }}
      />
      <ScrollView
        onMomentumScrollEnd={() => {
          onLoadData();
          onChangePageIndex();
        }}
        style={{ flex: 1 }}>
        <View style={styles.cntMain}>
          {
            data.map((value, index) => {
              let fontWeight=value.seen ? "normal" : "700";
              let color=value.seen ? 'gray' : '#3b60c4';
              let time = (value.timereminder == null || value.timereminder == undefined) ? value.time : value.timereminder;
              time=mapToYearMonthDay(time).time;
              return (
                <TouchableOpacity style={styles.btnMail}
                  key={index}
                  onPress={() => onButtonMail(value, index)}
                >
                  <View style={{ width: 50, justifyContent: 'center', alignItems: 'center', paddingLeft: 14 }} >
                    <Avatar
                      size="small"
                      rounded
                      source={image.dang}
                    />
                  </View>
                  <View style={{ flex: 1, padding: 16, justifyContent: 'center' }}>
                    <View style={styles.cntSender}>
                      <Text style={[styles.textSender,{fontWeight}]}>{value.sender}</Text>
                      <Text style={[styles.textTime,{color}]}>{time}</Text>
                    </View>
                    <View style={{paddingRight: 24, paddingTop: 2 }}>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={[styles.textTitle,{fontWeight}]}>{value.title}
                      </Text>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={styles.textDetails}>{value.details}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            })
          }
        </View>
      </ScrollView>
      <ActionButton
        buttonColor="rgba(231,76,60,1)"
        icon={<Ionicons name='md-create' style={styles.iconAdd} />}
        offsetX={10}
        offsetY={10}
        onPress={() => onChangeMovingSceen(SCREEN_SOAN_EMAIL)}
      />
    </View>
  )
}

