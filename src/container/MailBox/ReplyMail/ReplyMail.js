import React, { Component } from 'react'
import {
  Text, View, StyleSheet,
  Image, ScrollView, TextInput, BackHandler, Alert,TouchableOpacity
} from 'react-native'

import ImagePicker from 'react-native-image-picker';
// import RNFetchBlob from 'react-native-fetch-blob';

//import
import styles from './stylesReplyMail';
import { checkUser } from '../../../config/Function';
import { THONG_BAO, DA_GUI_TIN_NHAN_THANH_CONG, SOAN_EMAIL, DATA_USER_FAKE, ERROR, THEM_IT_NHAT_MOT_NGUOI_NHAN, DA_XAY_RA_LOI_KHI_GUI } from '../../../config/default'
import HeaderReal from '../../../common/HeaderReal';
import InputForMail from '../../../common/InputForMail';
import ListItem from '../../../common/ListItem';
import { FetchJson, GetStaffByKey, CreateEvent,UploadFilesEvent } from '../../../servers/serivce.config';
import HeaderForEmail from '../../../common/HeaderForEmail';
import ShowImageMail from '../../../common/ShowImageMail';
import STYLES from '../../../config/styles.config';

type Props = {
  title: string,
  userReceive: any,
  userSend: any,
  onBack: Function,
  contentMail: any,
  account:any,
}
export default class ReplyMail extends Component<Props> {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      listUserSuggest:[],
      avatarSource:'',
      data:null,
      isList:false,
      movingScreen: false,
      userReceive: (props.userReceive != undefined && props.userReceive!="") ?  props.userReceive + "," : "",
      title: props.contentMail != undefined ? props.contentMail.title : '',
      contentMail: props.contentMail != undefined ? props.contentMail.details : '',
    }
  }
  uploadPhoto=async ()=>{
    // const {account}=this.props;
    // let json ={
    //     userID:account.LoginUserID, 
    //     eventsid : 825 , 
    //     filename : 'avatar-png.png', 
    //     filesdata: this.state.data
    //   };
    // await FetchJson(UploadFilesEvent,json)
    //   .then((res)=>{
    //         console.log("====>>>>xem ket qua ra sao",res);
    //   })
    // RNFetchBlob.fetch('POST', UploadFilesEvent, {
    //   Authorization : "Bearer access-token",
    //   otherHeader : "foo",
    //   'Content-Type' : 'multipart/form-data',
    // }, [
    //   // custom content type
    //   {userID:account.LoginUserID, eventsid : 825 , 
    //   filename : 'avatar-png.png', filesdata: this.state.data},
    // ]).then((resp) => {
    //   console.log("====>>>>xem co gi ko resp",resp)
    //   // ...
    // }).catch((err) => {
    //   console.log("====>>>>xem co gi ko err",err)
    //   // ...
    // })
  }
  selectPhotoTapped=()=> {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        console.log("======>>>>> respone sau khi chon nay e ie",response);
        this.setState({
          avatarSource: source,
          data:response.data,
        });
      }
    });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = async () => {
    await this.props.onBack()
    return true;
  }
  onChangeTitle = (value) => {
    this.setState({ title: value });
  }
  onChangeUserReceive = async (value) => {
    const {account}=this.props;
    await this.setState({ userReceive: value,isList:true });
    let str=value;
    let vt = str.lastIndexOf(",");
    if (vt > 0){
            str=str.substring(vt+1,str.length);
    }
    let json={
      userID:account.LoginUserID,
      keysearch:str.trim(),
      size:5,
      pageindex:1,
    }
    await FetchJson(GetStaffByKey,json)
    .then((res)=>{
          this.setState({listUserSuggest:JSON.parse(res.d).rows})
    })
    
  }
  onAddUser=(value)=>{
      if (checkUser(this.state.userReceive)==ERROR) {
        this.setState({userReceive:value.LoginUserID+","});
      } else {
          let str=this.state.userReceive;
          let vt = str.lastIndexOf(",");
          if (vt > 0){
            this.setState({userReceive:str.substring(0,vt+1)+value.LoginUserID+","})
          } else {
            this.setState({userReceive:value.LoginUserID+","})
          }
          
      }
      this.setState({isList:false});
  }
  onAttach=()=>{
    
  }
  deleteImage=()=>{
      this.setState({avatarSource:''})
  }
  onSendMessage=async ()=>{
    const {account}=this.props;
    if (this.state.userReceive==""){
        Alert.alert(THEM_IT_NHAT_MOT_NGUOI_NHAN);
    } else {
      let json={
        createuserid:account.LoginUserID,
        sendto:this.state.userReceive,
        title:this.state.title,
        content:this.state.contentMail,
        onday:0,
        startdatetime:'',
        endatetime:'',
        reminder:0,
        reminderdatetime:'',
        repeat:0,
        repeatnumber:3,
        repeatdatetime:'',
        note:"Gửi tin nhắn",
        priority:0,
        newsid:1,
        eventid:0,
      }
      await FetchJson(CreateEvent,json)
      .then((res)=>{
            if (JSON.parse(res.d).status==true) {
              Alert.alert(DA_GUI_TIN_NHAN_THANH_CONG);
              this.props.onBack();
            }
            else {
              Alert.alert(DA_XAY_RA_LOI_KHI_GUI);
            }
      })
      
      
    }
  }
  render() {
    const { onBack, title, userSend, userReceive, account } = this.props;
    return (
      <View style={styles.container}
      
      >
        {/* <HeaderReal
          title={title}
          onButton={() => onBack()}
          colorImage={'#7BDEFE'}
          linkImage={'send'}
          onButtonRight={() => {
            this.onSendMessage();
          }}
        /> */}
        <HeaderForEmail
            onBack={()=> onBack()}
            onAttach={this.selectPhotoTapped.bind(this)}
            onSend={()=>this.onSendMessage()}
        />
        <ScrollView style={{ flex: 1 }}
        keyboardShouldPersistTaps={'handled'}>
          <View style={styles.cntMain}>
            <View style={styles.cntUserSend}>
              <Text style={styles.textUser}>
                Từ
                            </Text>
              <Text style={styles.textDefault}>
                {account.LoginUserID}
              </Text>
            </View>
            <InputForMail
              title={'Tới'}
              content={this.state.userReceive}
              onChange={this.onChangeUserReceive}
            />
            {
             this.state.isList &&
              <View style={{flex:0}}>
                    {
                          this.state.listUserSuggest.map((value,index) => {
                        return (
                        <ListItem
                           key={index}
                          image={value.ImageFile}
                          title={value.StaffName}
                          sub_title={value.LoginUserID}
                          onButton={()=> this.onAddUser(value)}
                        />
                      )
                      })
                    }
              </View>
                  
            }
            <InputForMail
              title={'Tiêu đề'}
              content={this.state.title}
              onChange={this.onChangeTitle}
            />
            {/* <TouchableOpacity
              onPress={()=> this.uploadPhoto()}
            >
                  <Text style={{color:"red"}}>Upload Anh</Text>
            </TouchableOpacity> */}
            <TextInput
              style={[styles.textEmail,this.state.avatarSource!='' ? {minHeight:0.15*STYLES.heightScreen} :{}]}
              placeholder={SOAN_EMAIL}
              returnKeyType='next'
              multiline={true}
              underlineColorAndroid={'transparent'}
              onChangeText={(value) => this.setState({ contentMail: value })}
              value={this.state.contentMail}
            />
            { 
              this.state.avatarSource!=""&& 
              <ShowImageMail
                linkImage={this.state.avatarSource}
                onDelete={this.deleteImage}
              />
            }
          </View>
        </ScrollView>
      </View>
    )
  }
}
