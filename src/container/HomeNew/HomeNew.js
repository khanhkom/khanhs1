import React, { Component } from 'react'
import {
    Text, View, ScrollView, AsyncStorage, ImageBackground
} from 'react-native';
import IMAGE from '../../assets/image';
import PopupDialog from 'react-native-popup-dialog';

import { Badge } from 'react-native-elements';
//import
import { connect } from 'react-redux';
import { LIST_ON_POP_UP, SCREEN_MAILBOX, INIT_STORGE, ADD_EVENT, MAIL_BOX, SET_LESSSON, DATA_FOR_HOME_OLD, DATA_FOR_HOME_NEW, DATA_FOR_HOME_NO_USER } from '../../config/default';
import styles from './styles'
import { FetchJson, GetNewsByTime, GetEventsByTime } from '../../servers/serivce.config'
//data
import dataHomeNew from '../data/dataHomeNew.json';
//common
import { LoadingComponent } from '../../common/LoadingComponet';
import ButtonChatbot from '../ButtonChatbot';
import TemplateForHome from '../../common/TemplateForHome';
import STYLES from '../../config/styles.config';
import HeaderHomeNew from '../../common/HeaderHomeNew';
import SwiperEvent from '../../common/SwiperEvent';
import { calArrMail, countMainNotSeen } from '../../config/Function';
import { setAccount } from '../../actions/actionCreators';
import ItemOnPopup from '../../common/ItemOnPopup';
type Props = {

}
var self;
class HomeNew extends Component<Props> {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            isLoading: false,
            visible: false,
            slMail: 0,
        }
    }
    async componentWillMount() {
        const { Account } = this.props;
        let listMail = [];
        let json = {
            userID: Account.LoginUserID,
            time: '',
            size: 10,
            pageindex: 1,
        }
        this.setState({ isLoading: false });
        await FetchJson(GetEventsByTime, json)
            .then((res) => {
                this.setState({ isLoading: false });
                listMail = calArrMail(JSON.parse(res.d).rows);
            })
        await AsyncStorage.multiSet([
            [MAIL_BOX, JSON.stringify(listMail)],
        ]);
        await this.props.addMail(listMail);
    }
    onLogout = async () => {
        const { navigation } = this.props.screenProps;
        await this.props.setAccount(undefined);
        await AsyncStorage.multiRemove([INIT_STORGE, ADD_EVENT, MAIL_BOX, SET_LESSSON]);
        await navigation.navigate('Intro');
    }
    onButtonForNoUser= (id) => {
        const { navigation, Account } = this.props;
        if (id == 1) navigation.navigate("News");
        if (id == 2) navigation.navigate("PartyDocument");
        if (id == 4) this.onLogout();
    }
    onButton = (id) => {
        const { navigation, Account } = this.props;
        if (id == 1) navigation.navigate("Target");
        if (id == 2) navigation.navigate("News");
        if (id == 3) navigation.navigate('SortJob');
        if (id == 4) navigation.navigate('StudyByThematic');
        if (id == 5) navigation.navigate('MailBox', {
            title: SCREEN_MAILBOX,
            account: Account
        })
        if (id == 6) navigation.navigate('Account');
        if (id == 7) navigation.navigate('PartyDocument');
        if (id == 8) {
            this.setState({ visible: true })
            //navigation.navigate('Management');
        }
        if (id == 10) this.onLogout();
        // if (id == 11) this.onLogout();
        // if (id == 13) navigation.navigate('NewsHot');
    }
    onButtonPopUp=(id)=>{
        const { navigation, Account } = this.props;
        // if (id == 0) navigation.navigate("Management");
        // if (id == 1) navigation.navigate("CheckImplement");
        if (id == 1) navigation.navigate("NewsHot");
        // if (id == 1) navigation.navigate("CheckImplement");
        // //if (id == 2) navigation.navigate('SortJob');
        // if (id == 3) navigation.navigate('NewsHot');
        this.setState({visible:false});
    }
    render() {
        const { navigation, MailBox,Account } = this.props;
        let slMail = MailBox != undefined && MailBox != null ? countMainNotSeen(MailBox) : 0;
        return (
            <View style={styles.container}>
                <PopupDialog
                    show={this.state.visible}
                    // width={STYLES.heightScreen * (243 / 640)}
                    // height={0.45*STYLES.heightScreen}
                    dialogStyle={styles.contentPopup}
                    onDismissed={() => {
                       this.setState({visible:false})
                    }}
                    dismissOnTouchOutside={true}
                    dismissOnHardwareBackPress={true}
                >
                    <View style={styles.contentPopup}>
                        {
                            LIST_ON_POP_UP.map((value,index)=>{
                               {/* if (index % 2 != 1) */}
                                    return (
                                        <ItemOnPopup
                                            key={index}
                                            content={value}
                                            onButton={() => this.onButtonPopUp(index)}
                                        />
                                    )
                                {/* else return (
                                    <View style={{ flex: 0 }}>
                                        <ItemOnPopup
                                            key={index}
                                            content={value}
                                            onButton={() => this.onButtonPopUp(index)}
                                        >
                                        </ItemOnPopup>
                                        <View style={{ height: 15 * STYLES.heightScreen / 896 }}></View>
                                    </View>
                                ) */}
                            })
                        }
                    </View>
                </PopupDialog>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    onMomentumScrollEnd={() => {
                    }}
                    contentContainerStyle={{ paddingBottom: 57 * STYLES.heightScreen / 896 }}
                    style={{ flex: 1 }}
                >

                    <HeaderHomeNew
                        account={this.props.Account}
                    />
                    <SwiperEvent
                        navigation={this.props.navigation}
                        data={dataHomeNew}
                    />
                    <View
                        style={{ width: STYLES.widthScreen, flexDirection: 'row', flexWrap: "wrap" }}>
                        <ImageBackground
                            style={styles.nen}
                            source={IMAGE.nenBG}>

                        </ImageBackground>
                        {
                            (Account!=undefined && Account!=null) && DATA_FOR_HOME_NEW.map((value, index) => {
                                if (index % 3 != 2)
                                    return (
                                        <TemplateForHome
                                            key={index}
                                            content={value}
                                            onButton={() => this.onButton(index)}
                                        />
                                    )
                                else return (
                                    <View style={{ flex: 0 }}>
                                        <TemplateForHome
                                            key={index}
                                            content={value}
                                            onButton={() => this.onButton(index)}
                                        >
                                            {/* {(slMail > 0 && index == 5) &&
                                                <Badge value={slMail}
                                                    status="success"
                                                    containerStyle={{ position: 'absolute', top: -4, right: 0 }}
                                                />} */}
                                        </TemplateForHome>
                                        <View style={{ height: 25 * STYLES.heightScreen / 896 }}></View>
                                    </View>
                                )
                            })
                        }
                        {
                            (Account==undefined||Account==null) && DATA_FOR_HOME_NO_USER.map((value, index) => {
                                if (index % 3 != 2)
                                    return (
                                        <TemplateForHome
                                            key={index}
                                            content={value}
                                            onButton={() => this.onButtonForNoUser(index)}
                                        />
                                    )
                                else return (
                                    <View style={{ flex: 0 }}>
                                        <TemplateForHome
                                            key={index}
                                            content={value}
                                            onButton={() => this.onButtonForNoUser(index)}
                                        >
                                        </TemplateForHome>
                                        <View style={{ height: 25 * STYLES.heightScreen / 896 }}></View>
                                    </View>
                                )
                            })
                        }
                    </View>
                </ScrollView>
                <ButtonChatbot
                    navigation={this.props.navigation}
                />
                <LoadingComponent isLoading={this.state.isLoading}></LoadingComponent>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account
    };
}

export default connect(mapStateToProps, {
    setAccount
})(HomeNew);