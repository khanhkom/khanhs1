import {
    StyleSheet
  } from 'react-native'
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

export default styles = StyleSheet.create({
  textTitle:{
    color: Colors.colorBlack,
    fontSize: STYLES.fontSize25,
    fontFamily: 'Roboto-Regular',
    fontWeight: 'bold',
    paddingTop: 20*STYLES.heightScreen/640,
    marginBottom: 10*STYLES.heightScreen/640,
    paddingLeft:10*STYLES.heightScreen/360,
  },
  popup: {
  
  },
  contentPopup: {
    paddingTop:45*STYLES.heightScreen/640,
    borderRadius: 0.02 * STYLES.widthScreen,
    flexWrap: 'wrap',
    width:STYLES.widthScreen * (500 / 640),
    justifyContent: 'center',
    flexDirection:'row',
    backgroundColor:Colors.white,
    padding:10,

  },
  nen:{
    position:'absolute',
    top:0.1*STYLES.heightScreen,
    left:0.175*STYLES.widthScreen,
    width:0.65*STYLES.widthScreen,
    height:0.35*STYLES.heightScreen,
  },  
  cntRow:{
        flex:0,
        flexDirection: 'row',
  },
    container: {
      flex:1,
      // backgroundColor:'#FF3333',
      // justifyContent:'center',
      // alignItems:'center',
    },
    line:{
        height:5,
        width:STYLES.widthScreen,
        backgroundColor: '#dddddd',
    },
    loadingContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
  },
})
