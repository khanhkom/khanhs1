import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,ImageBackground,TouchableOpacity
} from 'react-native'

//import
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

type Props = {
  title:string,
  sourceImage:any,
  time:string,
  author:string,
  details:string,
  navigation:Function,
}
export default TypeOne = (props: any) => {

    const {title,sourceImage,time,navigation,author,details}= props;  
    return (
     <TouchableOpacity
         onPress={() => navigation.navigate('Details',{details:details,time:time,author:author})}
     >   
            <ImageBackground 
                style={styles.container}
                source={{uri:sourceImage}}
                resizeMode={'stretch'}
            >
                <Text
                numberOfLines={2} 
                style={styles.textTitle}>{title}</Text>
                <View style={styles.cntNote}>
                    <Text style={styles.textAuthor}>{author}</Text>  
                     <Text style={styles.textTime}>{time}</Text>
                </View>
               
            </ImageBackground>
      </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
      container: {
        height:STYLES.heightScreen*0.3,
        width:STYLES.widthScreen,
        paddingTop: STYLES.heightScreen*0.3*0.55,
        paddingLeft: 10,
      },
      textTitle: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeLarge,
        fontFamily: 'Roboto-Regular',
        fontWeight: 'bold',
      },
      textTime: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        fontStyle: 'italic',
      },
      textAuthor: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        padding: 3,
        backgroundColor: Colors.blueNew,
        borderRadius: 2,
        marginRight: 3,
      },
      cntNote:{
          flexDirection: 'row',
          flex:0,
          //justifyContent: 'center',
          alignItems: 'center',
      }

})

