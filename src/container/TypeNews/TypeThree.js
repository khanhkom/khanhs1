import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,ImageBackground,TouchableOpacity
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

type Props = {
  content:string,
  navigation:Function,
}
export default TypeThree = (props: any) => {
    const {title,sourceImage,time,author,details}= props.content;  
    return (
     <TouchableOpacity
     onPress={() => this.props.navigation.navigate('Details',{content:content})}
     >   
            <View 
                style={styles.container}
            >
                <Image
                    source={{uri:sourceImage}}
                    style={styles.imageMain}
                    resizeMode={'stretch'}
                />
                <Text 
                  numberOfLines={2}
                  style={styles.textTitle}>{title}</Text>
                <Text style={styles.textTime}>{author} 
                        <Text style={[styles.textTime,{fontStyle:'normal'}]}> | </Text> 
                        {time}
                </Text>
            </View>
      </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
      container: {
        flex:0,
        width:STYLES.widthScreen*0.48,
        padding: 10,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
      },
      imageMain:{
        height:STYLES.heightScreen*0.2,
        width:STYLES.widthScreen*0.48-20,
      },
      textTitle: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeLabel,
        fontFamily: 'Roboto-Regular',
        fontWeight: 'bold',
        paddingBottom: 10,
      },
      textTime: {
        color: Colors.gray,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        fontStyle: 'italic',
      },
      textAuthor: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        padding: 3,
        backgroundColor: Colors.blueNew,
        borderRadius: 2,
        marginRight: 3,
      },
      cntNote:{
        width:STYLES.widthScreen*0.48,
        flex:0,
      }

})

