import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,ImageBackground,TouchableOpacity,
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

type Props = {
  content:any,
  navigation:Function,
  onButton:any,
}
export default TypeTwo = (props: any) => {
    const {title,sourceImage,time,author,details}= props.content;  
    return (
     <TouchableOpacity
     onPress={async () => {
        await props.navigation.dismiss();
        await props.navigation.navigate('Details',{content:props.content})
        if (props.onButton!=undefined) await props.onButton();
       }
     }
     >   
            <View 
                style={styles.container}
            >
                <Image
                    source={{uri:sourceImage}}
                    style={styles.imageMain}
                    resizeMode={'stretch'}
                />
                <View style={styles.ctnDetails}>
                    <Text 
                    numberOfLines={2}
                    style={styles.textTitle}>{title}</Text>
                    <Text style={styles.textTime}>{author} 
                        <Text style={[styles.textTime,{fontStyle:'normal'}]}> | </Text> 
                        {time}
                    </Text>
                </View>
            </View>
      </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
      container: {
        height:STYLES.heightScreen*0.2,
        width:STYLES.widthScreen,
        flexDirection: 'row',
        padding: 10,
        marginTop: 10,
        borderBottomWidth: 0.5,
        borderBottomColor:Colors.gray,
        //backgroundColor:Colors.gray
      },
      imageMain:{
        height:STYLES.heightScreen*0.2-20,
        width:STYLES.widthScreen*0.3,
      },
      ctnDetails:{
        flex:1,
        marginLeft: 5,
        justifyContent:'space-between',
      },
      textTitle: {
        color: Colors.colorBlack,
        fontSize: STYLES.fontSizeLabel,
        fontFamily: 'Roboto-Regular',
        fontWeight: 'bold',
        paddingBottom: 20,
      },
      textTime: {
        color: Colors.gray,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        fontStyle: 'italic',
      },
      textAuthor: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        padding: 3,
        backgroundColor: Colors.blueNew,
        borderRadius: 2,
        marginRight: 3,
      },
      cntNote:{
          flexDirection: 'row',
          flex:0,
          //justifyContent: 'center',
          alignItems: 'center',
      }

})

