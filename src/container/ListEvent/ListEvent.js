/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { Card, ListItem, Button, Icon } from 'react-native-elements'
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { CHI_TIEU_CAN_DAT, LIST_TARGET, LIST_ATTENT } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import ItemEvent from '../../common/ItemEvent';
import { FetchJson, GetEventsByTime } from '../../servers/serivce.config';
import { mapToYearMonthDay, isSmallerTen } from '../../config/Function';

class ListEvent extends Component<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
            dataLoadFromServer: []
        }
    }
    async componentWillMount() {
        let current=new Date();
        let time=current.getFullYear().toString()+"-"+isSmallerTen(current.getMonth() + 1);
        console.log("===>>Xem tine",time);
        let json = {
            userID: this.props.Account.LoginUserID,
            time: time,
            size: 10,
            pageindex: 1,
        }
        await FetchJson(GetEventsByTime, json)
            .then((response) => {
                let dataLoadFromServer = JSON.parse(response.d).rows;
                this.setState({ dataLoadFromServer });
            })
    }
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={'Các sự kiện'}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                    <View style={styles.container}>
                        {
                            LIST_ATTENT.map((value, index) => {
                                return (
                                    <ItemEvent
                                        key={index}
                                        title={value.title}
                                        time={value.time}
                                        backGround={'#fff'}
                                        month={value.month}
                                        index={index}
                                        onButton={() => this.props.navigation.navigate("Attendance")}
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(ListEvent);