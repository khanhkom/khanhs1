import React, { Component } from 'react'
import {
  Text, View, StyleSheet
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';
import {SLIDE_INTRO} from '../../config/default';
import AppIntroSlider from 'react-native-app-intro-slider';



type Props = {
  title:string,
}
export default class Usage extends Component<Props> {
    _onDone = () => {
        const { dispatch } = this.props.navigation;
          dispatch({
            type: 'Navigation/RESET',
            actions: [{
                type: 'Navigate',
                routeName: 'Home'
            }], index: 0
        });
      }
  render() {
    return (
     <View style={styles.container}>
           <AppIntroSlider slides={SLIDE_INTRO}
            onDone={this._onDone}
            doneLabel={'Tiếp tục'}
            nextLabel={'Tiếp tục'}
            skipLabel={'Bỏ qua'}
            onSkip={this._onDone}
            showSkipButton={true}
            activeDotStyle={{backgroundColor:colors.grey}}
            buttonTextStyle={{color:colors.colorBlack,fontWeight:'bold'}}  
            />
      </View>
    )
  }
}
const styles = StyleSheet.create({
      container: {
        flex:1,
      },
      text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
      },
})

