/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView, Alert
} from 'react-native'

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { BAN_DA_HOAN_THANH_XONG_BAI_HOC, BAI_2_TRA_LOI_CAU_HOI, SET_LESSSON } from '../../config/default';
import styles from './styles'
import { setCompleteLesson, setLesson } from '../../actions/actionCreators'
//import common
import HeaderReal from '../../common/HeaderReal';
import CheckBoxForTest from '../../common/CheckBoxForTest';
type Props = {
    sourcePDF: string,
    timesRead: any,
}
var self;
class TestExercises extends Component<Props> {
    constructor(props: Props) {
        super(props);
        self = this;
        this.dataAnswer = this.onInit();
    }
    onInit = () => {
        let exercises = this.props.navigation.getParam('content').exercises;
        let res = [];
        exercises.map((value, index) => {
            res.push({
                idExercises: index,
                answer: []
            })
        })
        return res;
    }
    onResult = (key, val) => {
        this.dataAnswer[key] = val;
    }
    onCompleteLesson = async () => {
        let content = this.props.navigation.getParam('content');
        let arr = this.props.DataExercises;
        await arr.map((value, index) => {
				if (value.idLesson == content.idLesson) {
					arr[index].complete = 1;
				}
			})
        await Alert.alert(
            'Thông báo',
            BAN_DA_HOAN_THANH_XONG_BAI_HOC,
            [
                { text: 'Làm lại', onPress: () => { this.props.navigation.navigate("ViewPDF", { content }) } },
                {
                    text: 'Gửi kết quả', onPress: async () => {
                        await this.props.setLesson(arr);
                        await AsyncStorage.multiSet([
                            [SET_LESSSON, JSON.stringify(arr)],
                        ]);
                        await this.props.navigation.navigate("StudyByThematic");
                    }
                },
            ],
            { cancelable: false }
        )

    }
    render() {
        let content = this.props.navigation.getParam('content');
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={BAI_2_TRA_LOI_CAU_HOI}
                    onButton={() => this.props.navigation.navigate("ViewPDF", { content })} />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                    <View style={styles.container}>
                        {
                            content.exercises.map((value, index) => {
                                return (
                                    <CheckBoxForTest
                                        key={index}
                                        id={index}
                                        content={value}
                                        onResult={this.onResult}
                                        navigation={this.props.navigation}
                                    />
                                )
                            })
                        }
                        <Button
                            title="Hoàn tất"
                            containerStyle={{
                                width: STYLES.widthScreen,
                                justifyContent: 'center',
                                alignItems: 'center',
                               
                            }}
                            buttonStyle={{
                                width: STYLES.widthScreen * 0.55,
                                elevation:3,
                                justifyContent: 'center',
                                borderRadius: STYLES.widthScreen * 0.1,
                                alignItems: 'center',
                                marginTop: 10 * STYLES.heightScreen / 640
                            }}
                            onPress={async () => await this.onCompleteLesson()}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}
function mapDispatchToProps(dispatch) {
	return {
		setLesson: (arr) => dispatch(setLesson(arr))
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(TestExercises);