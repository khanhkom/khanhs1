/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView
} from 'react-native'

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { HOC_TAP_THEO_CHUYEN_DE } from '../../config/default';
import styles from './styles'

//import common
import Header from '../../common/Header';
import HeaderReal from '../../common/HeaderReal';
import TypeForExercises from '../../common/TypeNew/TypeForExercises';
class StudyByThematic extends Component<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
            timeStart: undefined
        }
    }
    onButtonThematic = (val) => {
        let content = {
            timesRead: 0,
            sourcePDF: val,
        }
        this.props.navigation.navigate("ViewPDF", { content });
    }
    render() {
        const {DataExercises,navigation} = this.props;
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={HOC_TAP_THEO_CHUYEN_DE}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{flex:1}}>
                    <View style={styles.container}>
                        {
                            DataExercises.map((value, index) => {
                                return (
                                    <TypeForExercises
                                        key={index}
                                        content={value}
                                        navigation={navigation}
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(StudyByThematic);