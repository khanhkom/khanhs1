/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView
} from 'react-native'
import Pdf from 'react-native-pdf';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { calTime } from '../../config/Function'
import { HOC_TAP_THEO_CHUYEN_DE,BAI_1_DOC_HIEU_NGHI_QUYET } from '../../config/default';
import styles from './styles'

//import common
import Header from '../../common/Header';
import HeaderReal from '../../common/HeaderReal';
type Props = {
    sourcePDF: string,
    timesRead:any,
}
class ViewPDF extends Component<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
                timeStart:undefined
          }
    }
    onStart=()=>{
        let timeStart = new Date();
        this.setState({timeStart})
    }
    onCompleteLesson=()=>{
        let data = this.props.navigation.getParam('content');
        let timeEnd=new Date();
        let content={
            timesRead:data.timesRead+1,
            readingTime:calTime(this.state.timeStart,timeEnd),
            exercises:data.exercises,
            sourcePDF:data.sourcePDF,
            idLesson:data.idLesson
        }
        this.props.navigation.navigate("TestExercises",{content});
    }
    render() {
        let sourcePDF = this.props.navigation.getParam('content').sourcePDF;
        return (
            <View style={styles.cntViewPDF}>
                <HeaderReal
                    title={BAI_1_DOC_HIEU_NGHI_QUYET}
                    onButton={() => this.props.navigation.navigate('StudyByThematic')} />
                <View style={styles.cntPDF}>
                    <Pdf
                        source={sourcePDF}
                        onLoadComplete={(numberOfPages, filePath) => {
                            this.onStart(numberOfPages);
                        }}
                        onPageChanged={(page, numberOfPages) => {
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error) => {
                            console.log(error);
                        }}
                        style={styles.pdf} />
                </View>
                <Button
                    title="Hoàn thành bài đọc"
                    onPress={()=>this.onCompleteLesson()}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account
    };
}

export default connect(mapStateToProps, {
})(ViewPDF);