/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements'
//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import styles from './styles'
import {LIST_NEWS} from '../../config/default';

//import common

import HeaderReal from '../../common/HeaderReal';

class Admin extends Component<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
            dataLoadFromServer: []
        }
    }
    onButton=(id)=>{
       if (id==0) this.props.navigation.navigate('Management');
       if (id==1) this.props.navigation.navigate('CheckImplement');
       if (id==3) this.props.navigation.navigate('NewsHot');
    }
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={'Quản trị công tác Đảng'}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                    <View style={styles.container}>
                        {
                            LIST_NEWS.map((value, index) => {
                                return (
                                    <ListItem
                                        style={{
                                            marginBottom:5,
                                        }}
                                        title={value.title}
                                        leftIcon={{ name: value.icon }}
                                        rightIcon={{ name: 'keyboard-arrow-right' }}
                                        onPress={() => {
                                            this.onButton(index);
                                        }}
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(Admin);