import React, { Component } from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity,Image
} from 'react-native'
import IMAGE from '../assets/image';

type Props = {
  navigation:any,
}
export default ButtonChatbot = (props: any) => {
    return (
        <TouchableOpacity 
        onPress={() => props.navigation.navigate('Chatbot')}
        style={{position:'absolute',bottom:5,right:10}}
        >
         {/* <LottieView
          source={IMAGE.robot}
          autoPlay
          style={styles.logoStart}
          loop
        /> */}
        <Image source={IMAGE.chatbot} style={styles.logoStart}/>
    </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    logoStart:{
        width:55,
        height:55,
      },
})

