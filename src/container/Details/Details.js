/**
 * This component to show details news 
 * @huanhtm
 */

import React, { Component } from 'react'
import {
  Text, View, Image, ScrollView, Linking,AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';
//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import { CHI_TIET,TAI_DON_VI,TIN_KHAC } from '../../config/default';
import Colors from '../../config/Colors';
import styles from './styles';
import {FetchJson, GetNewsByTime} from '../../servers/serivce.config'

//import data
import dataDonvi from '../../container/data/dataDonvi.json';
import dataNew from '../../container/data/dataNew.json';

//import common
import HeaderReal from '../../common/HeaderReal';
import ButtonChatbot from '../ButtonChatbot';
import TypeTwo from '../TypeNews/TypeTwo';


type Props = {

}
class Details extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      fontText: STYLES.fontSizeText,
      dataWithType:[],
      pageIndex:1,
      account:{
        LoginUserID:''
      }
    }
  }

  onLoadData=async (idPage)=>{
    // let json={
    //   userID:this.props.Account.LoginUserID,
    //   time:'',
    //   format:0,
    //   type:this.props.navigation.getParam('content').type,
    //   size:10,
    //   pageindex:idPage,
    // }
    // await FetchJson(GetNewsByTime,json)
    // .then((res)=>{
    //   let dataWithType=this.state.dataWithType;
    //   let rows=JSON.parse(res.d).rows;
    //   dataWithType=dataWithType.concat(rows);
    //   this.setState({dataWithType});
    // })
    this.setState({dataWithType:dataNew});
  }

  async componentWillMount()
  {
      this.setState({account:this.props.Account});
      this.onLoadData(1);
  }
  changeFontText = () => {
    if (this.state.fontText == STYLES.fontSizeText)
      this.setState({ fontText: STYLES.fontSizeChatbot });
    else this.setState({ fontText: STYLES.fontSizeText });
  }
  render() {
    const CUSTOM_STYLES = {
    };
    const { author, time,details } = this.props.navigation.getParam('content');
    const content = this.props.navigation.getParam('content');
    const CUSTOM_RENDERERS = {
      htm: (htmlAttribs, children, convertedCSSStyles, passProps) => {
        return (
          <View 
            key={children}
            style={{ flex: 0, marginLeft: 10 }}>
            <Text style={styles.textTitle}>{children}</Text>
            <View style={styles.cntNote}>
              <Text style={styles.textAuthor}>{author}</Text>
              <Text style={styles.textTime}>{time}</Text>
            </View>
          </View>
        )
      },
    }
    const DEFAULT_PROPS = {
      htmlStyles: CUSTOM_STYLES,
      renderers: CUSTOM_RENDERERS,
      onLinkPress: (evt, href) => { Linking.openURL(href); },

    };
    return (
      <View style={{ flex: 1 }}>
        <HeaderReal
          onButton={() => this.props.navigation.goBack()}
          title={CHI_TIET}
          linkImage={this.state.fontText == STYLES.fontSizeText ? 'zoom-in' : 'zoom-out'}
          onButtonRight={this.changeFontText}
        />
        <ScrollView 
          onMomentumScrollEnd={()=>{
                this.onLoadData(this.state.pageIndex+1);
                this.setState({pageIndex:this.state.pageIndex+1});
              }}
          ref= {"scroll"}
          showsVerticalScrollIndicator={false}
          style={styles.container}>
        <View style={styles.cntHTML}>
            <HTML
                {...DEFAULT_PROPS}
                html={details}
                baseFontStyle={{ fontSize: this.state.fontText }}
              />
        </View>
          
          <View style={styles.cntText}>
            <Text style={{color: Colors.white,
            fontSize:STYLES.fontSizeText ,fontFamily: 'Roboto-Regular',}}>{TIN_KHAC}</Text>
          </View>
          <View style={styles.cntBonus}>
          {
            (this.state.dataWithType).map((value,index)=>{
              if (value.title!==content.title)
              return (
                <TypeTwo
                  key={index}
                  content={value}
                  onButton={()=>this.refs["scroll"].scrollTo({x: 0, y: 0, animated: true})}
                  navigation={this.props.navigation}
                />
              )
            })
          }
          </View>
          
        </ScrollView>
        <ButtonChatbot
          navigation={this.props.navigation}
        />
      </View>
    )
  }
}
function mapStateToProps(state) {
  return { 
    MailBox: state.rootReducer.MailBox ,
    Account: state.rootReducer.Account
  };
}

export default connect(mapStateToProps, {
  
})(Details);