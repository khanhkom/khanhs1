/**
 * This component to custom styles
 * @huanhtm
 */

import {
   StyleSheet
} from 'react-native'

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingBottom: 50*STYLES.heightScreen/640,
      //padding: 15,
  
    },
    cntText:{
        flex:0,
         padding:3,
         borderRadius:5 * STYLES.heightScreen / 830, 
         backgroundColor:Colors.blueNew,
         alignItems:'center',
         justifyContent: 'center',
         marginHorizontal: 10, 
    },
    cntBonus:{
        flex:0,
        justifyContent:'center',
        alignItems:'center',
    },
    cntHTML:{
        flex:0,
        padding:15*STYLES.widthScreen/360,
    },  
    line: {
      height: 10*STYLES.heightScreen/360,
    },
    cntImage: {
      width: STYLES.widthScreen * 0.85,
    },
    textTitle: {
      color: Colors.colorRed,
      fontSize: 28,
      fontFamily: 'Roboto-Regular',
      //textAlign: "center",
      fontWeight: 'bold',
      //textAlignVertical: "center",
      marginBottom: 10,
    },
    text: {
      color: Colors.colorBlack,
      fontSize: STYLES.fontSizeLabel,
      fontFamily: 'Roboto-Regular',
      letterSpacing: 0.6,
      // textAlign: "center",
      // textAlignVertical: "center"
    },
    textBold: {
      color: Colors.colorBlack,
      fontSize: STYLES.fontSizeLabel,
      fontFamily: 'Roboto-Regular',
      letterSpacing: 0.6,
      // textAlign: "center",
      // textAlignVertical: "center",
      fontWeight: 'bold',
    },
    textTime: {
      color: Colors.grey,
      fontSize: STYLES.fontSizeNormalText,
      fontFamily: 'Roboto-Regular',
      fontStyle: 'italic',
    },
    textAuthor: {
      color: Colors.colorWhite,
      fontSize: STYLES.fontSizeNormalText,
      fontFamily: 'Roboto-Regular',
      padding: 3,
      backgroundColor: Colors.blueNew,
      borderRadius: 2,
      marginRight: 3,
    },
    cntNote: {
      flexDirection: 'row',
      flex: 0,
      marginBottom: 10,
      //justifyContent: 'center',
      alignItems: 'center',
    }
  })