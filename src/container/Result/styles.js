import {
    StyleSheet,
 } from 'react-native'
 
 //import
 import IMAGE from '../../assets/image';
 import STYLES from '../../config/styles.config';
 import colors from '../../config/Colors';
 
 export default styles = StyleSheet.create({
     container: {
       flex:1,
       paddingBottom: 20*STYLES.heightScreen/640,
     },
     row:{
        flex:0,
        flexDirection:'row',
     },
     header:{
        marginTop: 20*STYLES.heightScreen/640,
        width:STYLES.widthScreen,
        flexDirection:'row'
     },
     cntViewPDF:{
       flex:1,
     },
     btnNote:{
        flex:0,
        marginTop:10,
     },
     cntMain: {
       flex:1,
       backgroundColor: colors.lightGrayEEE,
      //  alignItems:'center',
     },
     cntPersion:{
      width: STYLES.widthScreen*0.3,
      minHeight:0.35*STYLES.heightScreen,
      paddingHorizontal:5*STYLES.widthScreen/360,
      borderColor: colors.grayNew,
      borderWidth:0.5,
      justifyContent:'center',
      alignItems:'center',
    },
    cntNhiemvu:{
      width: STYLES.widthScreen*0.3,
      minHeight:0.35*STYLES.heightScreen,
      paddingHorizontal:5*STYLES.widthScreen/360,
      borderColor: colors.grayNew,
      borderWidth:0.5,
      justifyContent:'center',
      alignItems:'center',
    },
    cntMinhChung:{
      width: STYLES.widthScreen*0.25,
      minHeight:0.35*STYLES.heightScreen,
      paddingHorizontal:5*STYLES.widthScreen/360,
      borderColor: colors.grayNew,
      borderWidth:0.5,
      justifyContent:'center',
      alignItems:'center',
    },
    cntDanhGia:{
      width: STYLES.widthScreen*0.15,
      minHeight:0.35*STYLES.heightScreen,
      paddingHorizontal:5*STYLES.widthScreen/360,
      borderColor: colors.grayNew,
      borderWidth:0.5,
      justifyContent:'center',
      alignItems:'center',
    },
     nguoitg:{
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeNormalText,
      fontFamily: 'Roboto-Regular',
      width: STYLES.widthScreen*0.3,
      textAlign:'center',
      textAlignVertical: 'center',
      borderColor: colors.grayNew,
      borderWidth:0.5,
      fontWeight:'bold',
     }, 
     text:{
      color: colors.colorBlack,
      fontWeight:'bold',
      marginVertical:20*STYLES.heightScreen/896,
      marginLeft: 0.03*STYLES.widthScreen,
      fontSize: STYLES.fontSizeChatbot,
      fontFamily: 'Roboto-Regular',
     },
     nhiemvu:{
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeNormalText,
      fontFamily: 'Roboto-Regular',
      width: STYLES.widthScreen*0.3,
      textAlign:'center',
      textAlignVertical: 'center',
      borderColor: colors.grayNew,
      borderWidth:0.5,
      fontWeight:'bold',
     },
     minhchung:{
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeNormalText,
      fontFamily: 'Roboto-Regular',
      width: STYLES.widthScreen*0.25,
      textAlign:'center',
      textAlignVertical: 'center',
      borderColor: colors.grayNew,
      borderWidth:0.5,
      fontWeight:'bold',
     },
     danhgia:{
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeNormalText,
      fontFamily: 'Roboto-Regular',
      width: STYLES.widthScreen*0.15,
      textAlign:'center',
      textAlignVertical: 'center',
      borderColor: colors.grayNew,
      borderWidth:0.5,
      fontWeight:'bold',
     },
     cntPDF: {
       flex: 1,
       justifyContent: 'flex-start',
       alignItems: 'center',
   },
   pdf: {
       flex:1,
       width:STYLES.widthScreen,
   },
   btnSendList:{
    width:STYLES.widthScreen,
    justifyContent:'center',
    alignItems:'center',
    marginVertical: 20*STYLES.heightScreen/640,
},
buttonSendList:{
  width:0.55*STYLES.widthScreen,
  elevation:3,
  borderRadius: 0.1*STYLES.widthScreen,
}
 })
 
 