import {
    StyleSheet, 
    
  } from 'react-native'
  
  //import
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    avatar: {
      width: STYLES.widthScreen*0.8,
      height: 0.25 * STYLES.heightScreen
    },
    textEmail: {
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
      width: STYLES.widthScreen,
      minHeight: 0.35 * STYLES.heightScreen,
      paddingHorizontal: 20 * STYLES.widthScreen / 360,
      marginTop: 10 * STYLES.heightScreen / 640,
      textAlignVertical: 'top'
    },
    cntReply: {
      width: STYLES.widthScreen,
      paddingHorizontal: 55 * STYLES.widthScreen / 360,
      marginBottom: 30 * STYLES.heightScreen / 640,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    reply: {
      marginRight: 10 * STYLES.widthScreen / 360
    },
    textDefault: {
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    text: {
      color: colors.colorBlack,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    cntMain: {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      marginBottom: 20 * STYLES.heightScreen / 640,
    },
    cntUserSend: {
      width: 0.9 * STYLES.widthScreen,
      height: 55 * STYLES.heightScreen / 640,
      alignItems: 'center',
  
      marginTop: 8 * STYLES.heightScreen / 640,
      borderBottomColor: '#E2E2E2',
      borderBottomWidth: 1,
      flexDirection: 'row',
    },
    textUser: {
      color: colors.grey,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
      marginRight: 20 * STYLES.widthScreen / 360,
    },
    cntTitle: {
      width: 0.9 * STYLES.widthScreen,
      minHeight: 75 * STYLES.heightScreen / 640,
      marginTop: 8 * STYLES.heightScreen / 640,
      justifyContent: 'center',
    },
    textTitle: {
      color: colors.colorBlack,
      fontWeight: 'bold',
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    textTime: {
      color: colors.lightGray,
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
    },
    cntDetails: {
      flex: 0,
      marginTop: 8 * STYLES.heightScreen / 640,
      width: 0.9 * STYLES.widthScreen,
    },
  })
  
  