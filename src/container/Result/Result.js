/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { Card, ListItem, Button, Icon } from 'react-native-elements'
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { LIST_NQ } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import ItemEvent from '../../common/ItemEvent';

class Result extends Component<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
            timeStart: undefined
        }
    }
    render() {
        let data=this.props.navigation.getParam('content');
        return (
            <View style={{
                flex: 1,
            }}>
                <HeaderReal
                    title={"Kiểm tra thực hiện Nghị quyết"}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        paddingBottom:20*STYLES.heightScreen/896
                    }}>
                        {
                            data.map((value, index) => {
                                return (
                                    <ItemEvent
                                        key={index}
                                        title={value.title}
                                        time={value.time}
                                        month={value.month}
                                        backGround={'#fff'}
                                        index={index}
                                        onButton={() =>
                                            this.props.navigation.navigate("TablePersion", { content: value.listAttent })
                                        }
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(Result);