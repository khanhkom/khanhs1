/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements'
//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import styles from './styles'
import {LIST_CHECK, LIST_NQ_DINH_KY, LIST_NQ_TW, LIST_NQ_THANG, LIST_NQ_NAM} from '../../config/default';

//import common

import HeaderReal from '../../common/HeaderReal';

class CheckImplement extends Component<Props> {
    constructor(props: Props) {
        super(props);
        this.state = {
            dataLoadFromServer: []
        }
    }
    onButton=(id)=>{
      let data=LIST_NQ_DINH_KY;
      if (id==0) data=LIST_NQ_DINH_KY;
      if (id==1) data=LIST_NQ_TW;
      if (id==2) data=LIST_NQ_THANG;
      if (id==3) data=LIST_NQ_NAM;
      this.props.navigation.navigate('Result',{content:data});
    }
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={'Kiểm tra thực hiện Nghị quyết'}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                    <View style={styles.container}>
                        {
                            LIST_CHECK.map((value, index) => {
                                return (
                                    <ListItem
                                        style={{
                                            marginBottom:5,
                                        }}
                                        title={value.title}
                                        leftIcon={{ name: value.icon }}
                                        rightIcon={{ name: 'keyboard-arrow-right' }}
                                        onPress={() => {
                                            this.onButton(index);
                                        }}
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(CheckImplement);