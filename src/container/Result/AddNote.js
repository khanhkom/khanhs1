import React, { Component } from 'react'
import {
  Text, View, StyleSheet,
  Image, ScrollView, TextInput, BackHandler, Alert,TouchableOpacity
} from 'react-native'

import ImagePicker from 'react-native-image-picker';
// import RNFetchBlob from 'react-native-fetch-blob';

//import
import styles from './stylesNote';
import HeaderForEmail from '../../common/HeaderForEmail';
import ShowImageMail from '../../common/ShowImageMail';
import STYLES from '../../config/styles.config';

type Props = {
  onBack: Function,
  onChangeContent:Function,
  idNote:any,
}
export default class AddNote extends Component<Props> {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      avatarSource:'',
      data:"",
      contentMail: '',
    }
  }
  uploadPhoto=async ()=>{
    // const {account}=this.props;
    // let json ={
    //     userID:account.LoginUserID, 
    //     eventsid : 825 , 
    //     filename : 'avatar-png.png', 
    //     filesdata: this.state.data
    //   };
    // await FetchJson(UploadFilesEvent,json)
    //   .then((res)=>{
    //         console.log("====>>>>xem ket qua ra sao",res);
    //   })
    // RNFetchBlob.fetch('POST', UploadFilesEvent, {
    //   Authorization : "Bearer access-token",
    //   otherHeader : "foo",
    //   'Content-Type' : 'multipart/form-data',
    // }, [
    //   // custom content type
    //   {userID:account.LoginUserID, eventsid : 825 , 
    //   filename : 'avatar-png.png', filesdata: this.state.data},
    // ]).then((resp) => {
    //   console.log("====>>>>xem co gi ko resp",resp)
    //   // ...
    // }).catch((err) => {
    //   console.log("====>>>>xem co gi ko err",err)
    //   // ...
    // })
  }
  selectPhotoTapped=()=> {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          avatarSource: source,
          data:response,
        });
      }
    });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = async () => {
    await this.props.onBack()
    return true;
  }
  onAttach=()=>{
    
  }
  deleteImage=()=>{
      this.setState({avatarSource:''})
  }
  onSendMessage=async ()=>{
    const {onChangeContent,onBack,idNote} = this.props;
    onChangeContent(idNote,{
        text:this.state.contentMail,
        image:this.state.data,
    })
    onBack();
  }
  render() {
    const { onBack,onChange } = this.props;
    return (
      <View style={styles.container}
      
      >
        <HeaderForEmail
            onBack={()=> onBack()}
            onAttach={this.selectPhotoTapped.bind(this)}
            onSend={()=>this.onSendMessage()}
        />
        <ScrollView style={{ flex: 1 }}
        keyboardShouldPersistTaps={'handled'}>
          <View style={styles.cntMain}>
            <TextInput
              style={[styles.textEmail,this.state.avatarSource!='' ? {minHeight:0.15*STYLES.heightScreen} :{}]}
              placeholder={"Thêm ghi chú"}
              returnKeyType='next'
              multiline={true}
              underlineColorAndroid={'transparent'}
              onChangeText={(value) => this.setState({ contentMail: value })}
              value={this.state.contentMail}
            />
            { 
              this.state.avatarSource!=""&& 
              <ShowImageMail
                linkImage={this.state.avatarSource}
                onDelete={this.deleteImage}
              />
            }
          </View>
        </ScrollView>
      </View>
    )
  }
}
