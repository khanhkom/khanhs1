/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView, Alert, Linking, TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux';
import { CheckBox, Button } from 'react-native-elements'
//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { CHI_TIEU_CAN_DAT, LIST_CA_NHAN, LIST_NQ_DINH_KY } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import AddNote from './AddNote';
import Table from '../../common/Table';
var self;
class TablePersion extends Component<Props> {
    constructor(props: Props) {
        super(props);
        self=this;
        this.state = {
            note: this.onInit(props).note,
            idNote:0,
            movingScreen: 'ROOT',
            checked: this.onInit(props).checked
        }
    }
    onInit = (props) => {
        let data =LIST_CA_NHAN.concat(LIST_NQ_DINH_KY[0].listAttent);
        let arr = [];
        let note=[];
        data.map((value, index) => {
            note.push({
                text:'',
                image:'',
            })
            arr.push({
                LoginUserID: value.hoTen,
                present: false,
            });
        })
        return {
            checked:arr,
            note:note
        };
    }
    sendList = () => {
        this.props.navigation.goBack();
        Alert.alert('Bạn đã gửi danh sách thành công')
    }
    changeCheck = (id) => {
        let checked = this.state.checked;
        checked[id].present = !checked[id].present;
        this.setState({ checked })
    }
    onChangeScreen = (val) => {
        this.setState({ movingScreen: val })
    }
    onChangeNote = (id,data) => {
        let note=this.state.note;
        note[id]=data;
        this.setState({ note })
    }
    render() {
        let data = LIST_NQ_DINH_KY[0].listAttent;
        if (this.state.movingScreen == 'ROOT')
            return (
                <View style={styles.cntMain}>
                    <HeaderReal
                        title={"Những người tham gia"}
                        onButton={() => this.props.navigation.goBack()}
                    />
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={{ flex: 1 }}>
                        <View style={styles.container}>
                            <Text style={styles.text}>{"Nhiệm vụ của cá nhân"}</Text>
                            {
                                Table(self,LIST_CA_NHAN,0)
                            }
                            <Text style={styles.text}>{"Nhiệm vụ của mọi người"}</Text>
                            {
                                Table(self,data,1)
                            }
                            <Button
                                title="Gửi danh sách"
                                onPress={() => {
                                    this.sendList();
                                }}
                                containerStyle={styles.btnSendList}
                                buttonStyle={styles.buttonSendList}
                            />
                        </View>
                    </ScrollView>
                </View>
            )
        if (this.state.movingScreen == "ADD_NOTE") {
            return (
                <AddNote
                    onBack={() => this.onChangeScreen("ROOT")}
                    onChangeContent={this.onChangeNote}
                    idNote={this.state.idNote}
                />
            )

        }
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(TablePersion);