import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, BackHandler,TextInput
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';
import Feather from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderReal from '../../common/HeaderReal';
import { LAP_LAI, LAN } from '../../config/default';
type Props = {
    data: any,
    stateInput: string,
    onChange: Function,
    onBack: Function,
    onChangeFrequency:Function,
    stateFrequency:string,
    title: string,
}

export default class ListData extends Component<Props> {
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = async () => {
        await this.props.onBack()
        return true;
    }
    onSave=() => {
        this.props.onBack();
    }
    render() {
        const { data, stateInput, onChange, onBack, title ,onChangeFrequency,stateFrequency } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <HeaderReal
                    title={title}
                    onButton={() => onBack()}
                    linkImage={title == LAP_LAI ? 'check': ''}
                    colorImage={title == LAP_LAI ? '#7BDEFE':''}
                    onButtonRight={title == LAP_LAI ? this.onSave : {} }
                />
                <ScrollView style={styles.container}>
                    <View style={styles.ctnScroll}>
                        {
                            data.map((value, index) => {
                                let colorTick = value.content == stateInput ? colors.colorPink : 'white'
                                return (
                                    <TouchableOpacity
                                        key={index}
                                        onPress={async () => {
                                           if (title !== LAP_LAI) {
                                            await onChange(value)
                                            await onBack();
                                           }    
                                            else {
                                                   await onChange(value);
                                            }
                                        }
                                        }
                                        style={styles.viewCangay}>
                                        <Text style={styles.text}>{value.content}</Text>
                                        <Feather
                                            name={'check'}
                                            size={15 * STYLES.heightScreen / 640}
                                            color={colorTick}
                                        />
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                    {
                        title == LAP_LAI && 
                        <View style={styles.viewLimit}>
                            <View style={styles.viewMainLimit}>
                                <Text style={styles.text}>
                                    {LAP_LAI}
                                </Text>
                                <TextInput
                                    style={styles.textEmail}
                                    placeholder={'3'}
                                    returnKeyType='next'
                                    underlineColorAndroid={'transparent'}
                                    onChangeText={(frequency) => onChangeFrequency(frequency)}
                                    value={stateFrequency}
                                />
                                <Text style={styles.text}>
                                    {LAN}
                                </Text>
                            </View>
                        </View>
                    }
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    viewLimit:{
        marginTop:20 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen * 0.85,
        alignItems:'flex-end',
    },
    viewMainLimit:{
        flex:0,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row'
    },
    textEmail:{
        marginHorizontal: 5*STYLES.widthScreen/360,
        borderWidth:1,
        borderColor:colors.lightBlack,
        padding: 3*STYLES.widthScreen/360,
        textAlignVertical:'center',
        textAlign:'center'
    },
    container: {
        flex: 1,
        marginBottom: 20 * STYLES.heightScreen / 640,
    },
    ctnScroll: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.lightBlack,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
    },
    viewCangay: {
        marginTop: 8 * STYLES.heightScreen / 640,
        height: 40 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen * 0.9,
        paddingHorizontal: STYLES.widthScreen * 0.025,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomColor: '#E2E2E2',
        borderBottomWidth: 1,
        flexDirection: 'row',
    },
})

