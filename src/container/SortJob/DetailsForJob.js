import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity, BackHandler,ImageBackground
} from 'react-native'

//import
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';
import IMAGE from '../../assets/image';
import HeaderReal from '../../common/HeaderReal';
import TabBottom from './TabBottom';
import ItemDetailsEvent from '../../common/ItemDetailsEvent';
type Props = {

}
export default class DetailsForJob extends Component<Props> {
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('SortJob');
        return true;
    }
    onAttendance = () => {
        this.props.navigation.navigate("Attendance");
    }
    onTablePersion = () => {
        this.props.navigation.navigate("TablePersion");
    }
    render() {
        const data = this.props.navigation.getParam('data');
        let time = data.timeStart+" "+data.dayStart+"/"+data.monthStart+" - "+data.timeEnd+" "+data.dayEnd+"/"+data.monthEnd;
        const {  bodyStyle, tieudeStyle,  bodyStyle1, noidungStyle, buttomStyle, ketluanStyle } = styles;
        return (
            <View>
                <HeaderReal
                    title={'Chi tiết'}
                    onButton={()=>this.handleBackPress()}
                />
                <ImageBackground source={IMAGE.bgDetailsEvent} style={{ resizeMode: "stretch", width: '100%', height: '100%', }}>
                    <View style={{ height: (480 / 640) * STYLES.heightScreen }}>
                        <View style={bodyStyle}>
                            <View style={tieudeStyle}>
                                <Text style={{ color: "#DA251D", fontSize: 15, fontWeight: '500' }}>{data.title}</Text>
                            </View>
                            <ItemDetailsEvent image={IMAGE.iconLich} heights={20 / 640} widths={20 / 360} marginLeftt={10 / 360} infor={time} ></ItemDetailsEvent>
                            <ItemDetailsEvent image={IMAGE.diadiem} heights={20 / 640} widths={12 / 360} marginLeftt={15 / 360} infor={"43,Nguyễn Chí Thanh"} ></ItemDetailsEvent>
                            <ItemDetailsEvent image={IMAGE.thanhphan} heights={20 / 640} widths={20 / 360} marginLeftt={10 / 360} infor={"Đinh Văn Anh, Đinh Văn Linh, Mạc Văn Khoa, Trần Quang Mạnh"} ></ItemDetailsEvent>
                        </View>
                        <View style={bodyStyle1}>
                            <View style={noidungStyle}>
                                <View style={{ justifyContent: "center", alignItems: "center", width: 0.92 * STYLES.widthScreen, marginTop: (8 / 640) * STYLES.heightScreen }}>
                                    <Text style={{ textDecorationLine: "underline", fontSize: 14, color: "#1C1C1C" }}>Nội dung</Text>
                                </View>
                                <View style={{ width: (322 / 360) * STYLES.widthScreen, marginLeft: (9 / 360) * STYLES.widthScreen, marginTop: (12 / 640) * STYLES.heightScreen, height: (98 / 640) * STYLES.heightScreen }}>
                                    <View>
                                        <Text style={{ color: "#1C1C1C", fontFamily: 'Roboto', fontSize: 15 }}>{data.details}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={ketluanStyle}>
                                <View style={{ justifyContent: "center", alignItems: "center", width: 0.92 * STYLES.widthScreen, marginTop: (8 / 640) *  STYLES.heightScreen }}>
                                    <Text style={{ textDecorationLine: "underline", fontSize: 14, color: "#1C1C1C", fontFamily: 'Roboto' }}>Kết luận</Text>
                                </View>
                                <View style={{ width: (322 / 360) * STYLES.widthScreen, marginLeft: (9 / 360) * STYLES.widthScreen, marginTop: (4 / 640) * STYLES.heightScreen, height: (53 / 640) * STYLES.heightScreen }}>
                                    <View >
                                            <Text  style={{ color: "#1C1C1C", fontFamily: 'Roboto', fontSize: 15 }}>{data.result}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{
                        width: (5 / 6) * STYLES.widthScreen,
                        marginLeft: (1 / 12) * STYLES.widthScreen,
                        flexDirection: "row",
                        justifyContent: "space-between",
                        borderColor: "#DA251D",

                    }}>
                        <TouchableOpacity 
                            onPress={()=>this.props.navigation.navigate("Attendance")}
                            style={buttomStyle}>
                            <Text style={{ color: "white", fontSize: 15, fontFamily: 'Roboto' }}>Điểm danh</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                             onPress={()=>this.props.navigation.navigate("TablePersion")}
                            style={buttomStyle}>
                            <Text style={{ color: "white", fontSize: 15, fontFamily: 'roboto' }}>Kiểm tra thực hiện</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    headerStyle: {
        height: (46 / 640) * STYLES.heightScreen,
        width: STYLES.widthScreen,
        flexDirection: 'row',
        backgroundColor: '#DA251D',
        justifyContent: 'center',
        alignItems: "center",
    },
    iconBackStyle: {
        height: 0.04 * STYLES.heightScreen,
        width: 0.25 * STYLES.widthScreen,
        backgroundColor: '#DA251D',
        borderRadius: 7,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    textHeadrStyle: {
        width: 0.5 * STYLES.widthScreen,
        height: STYLES.heightScreen,
        justifyContent: 'center',
        alignItems: "center"
    },
    iconNoteStyle: {
        height: 0.04 * STYLES.heightScreen,
        width: 0.25 * STYLES.widthScreen,
        backgroundColor: '#DA251D',
        borderRadius: 7,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    bodyStyle: {
        width: (286 / 360) * STYLES.widthScreen,
        marginLeft: (37 / 360) * STYLES.widthScreen,

    },
    tieudeStyle: {
        backgroundColor: "white",
        padding: 5,
        justifyContent: "center",
        borderRadius: 13,
        flexDirection: "row",
        marginTop: (12 / 640) * STYLES.heightScreen,
    },
    tieudeStyle1: {
        backgroundColor: "white",
        padding: 5,
        borderRadius: 13,
        flexDirection: "row",
        marginTop: (12 / 640) * STYLES.heightScreen,
    },
    bodyStyle1: {
        width: (339/360) * STYLES.widthScreen,
        marginLeft: (7/240) * STYLES.widthScreen,

    },
    noidungStyle: {
        backgroundColor: "white",
        //justifyContent: "center",
        borderRadius: 13,
        height:(150/640)*STYLES.heightScreen,
        marginTop: (12/640) * STYLES.heightScreen,
    },
    ketluanStyle: {
        backgroundColor: "white",
        //justifyContent: "center",
        borderRadius: 13,
        height:(94/640)*STYLES.heightScreen,
        marginTop: (12/640) * STYLES.heightScreen,
    },
    buttomStyle: {
        elevation:3,
        backgroundColor: "#DA251D",
        alignItems: "center",
        justifyContent: "center",
        height:(32/640)*STYLES.heightScreen,
        width: (144/360) * STYLES.widthScreen,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: "#DDDDDD",
    }
})


