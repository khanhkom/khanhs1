import { calListViewMonth, resultIndexDay, calArrEvent, calMixColor, loadDataByMonth } from '../../config/Function'
import colors from '../../config/Colors';
import STYLES from '../../config/styles.config';
var dataColor = [colors.yellowCalendar, colors.blueCalendar, colors.greenCalendar, colors.grayNew]

export const onButtonRight=async (self) => {
    const { day, month, year ,arrEventLocal} = self.state;
    const {Account} = self.props;
    let str = month === 12 ? 1 : month + 1;
    await self.setState({ month: str });
    let res = calListViewMonth(1, self.state.month, year);
    let arrloadDataByMonth=await loadDataByMonth(self,arrEventLocal,str,year,Account)
    let resArr = await calArrEvent(arrloadDataByMonth, 1, str, year);
   
    self.setState({
      dataDay: res.arr,
      selected: res.selected,
      eventsByDay: resArr.eventsByDay,
      onSelected: resArr.eventsNotDone,
    })
  }
export const onButtonLeft=async (self)=> {
    const { day, month, year,arrEventLocal } = self.state;
    const {Account} = self.props;
    let str = month === 1 ? 12 : month - 1;
    await self.setState({ month: str });
    let res = calListViewMonth(1, self.state.month, year)
    let arrloadDataByMonth=await loadDataByMonth(self,arrEventLocal,str,year,Account)
    let resArr = await calArrEvent(arrloadDataByMonth, 1, str, year);
    self.setState({
      dataDay: res.arr,
      selected: res.selected,
      eventsByDay: resArr.eventsByDay,
      onSelected: resArr.eventsNotDone,
    })
  }
export const onButtonDay=async (self, id, value)=> {
    self.setState({ selected: id, onSelected: self.state.eventsByDay[value].dataEvent })
  }
export const onDoneJob=async(self)=> {
    let dateCurrent = new Date();
    const { day, month, year,arrEventLocal } = self.state;
    const {Account} = self.props;
    let arrloadDataByMonth=await loadDataByMonth(self,arrEventLocal,month,year,Account);
    let resArr = await calArrEvent(arrloadDataByMonth,
      dateCurrent.getDate(), dateCurrent.getMonth() + 1, dateCurrent.getFullYear());
    self.setState({
      onSelected: resArr.eventsDone
    })
  }
export const onToday=async(self)=> {
    let dateCurrent = new Date();
    await self.setState({
      day: dateCurrent.getDate(),
      month: dateCurrent.getMonth() + 1,
      year: dateCurrent.getFullYear(),
      thu: dateCurrent.getDay(),
    });
    const { day, month, year ,arrEventLocal} = self.state;
    let res = await calListViewMonth(day, month, year);
    const {Account} = self.props;
    let arrloadDataByMonth=await loadDataByMonth(self,arrEventLocal,month,year,Account);
    let resArr = await calArrEvent(arrloadDataByMonth, day, month, year);
    self.setState({
      dataDay: res.arr,
      selected: -1,
      eventsByDay: resArr.eventsByDay,
      onSelected: resArr.eventsNotDone,
    })
  }
  export const onNeedDoneJob=async(self)=> {
    let dateCurrent = new Date();
    const { day, month, year,arrEventLocal } = self.state;
    const {Account} = self.props;
    let arrloadDataByMonth=await loadDataByMonth(self,arrEventLocal,month,year,Account);
    let resArr = await calArrEvent(arrloadDataByMonth,
      dateCurrent.getDate(), dateCurrent.getMonth() + 1, dateCurrent.getFullYear());
    self.setState({
      onSelected: resArr.eventsNotDone
    })
  }
  export const outColorForDay=(self,value)=> {
    let arr = self.state.eventsByDay[value].dataColorEvent;
    let minDay = self.state.eventsByDay[value].minDay;
    let maxDay = self.state.eventsByDay[value].maxDay;
    let res = {
      viewDay: {},
      btnDay: {},
      textDay: {}
    }
    for (let i = 0; i < arr.length; i++) {
      let colorMix = res.btnDay.backgroundColor != undefined
        ? calMixColor(dataColor[arr[i].idEvent % 3], res.btnDay.backgroundColor)
        : dataColor[arr[i].idEvent % 3]
      ///////////////////////////Truong hop bang 0
      let id = arr[i].idFormat;
      if (id == 0) {
        if (value == minDay) {
          Object.assign(res.viewDay,
            {
              borderBottomLeftRadius: 35 * STYLES.heightScreen / 640,
              borderTopLeftRadius: 35 * STYLES.heightScreen / 640,
              backgroundColor: colorMix,
            },
          );
        } else Object.assign(res.viewDay,
          {
            backgroundColor: colorMix,
          },
        );

        Object.assign(res.btnDay,
          {
            backgroundColor: colorMix,
          },
        )
        Object.assign(res.textDay,
          { color: colors.colorBlack }
        )
      };
      if (id == 2) {
        if (value == maxDay) {
          Object.assign(res.viewDay,
            {
              borderTopRightRadius: 35 * STYLES.heightScreen / 640,
              borderBottomRightRadius: 35 * STYLES.heightScreen / 640,
              backgroundColor: colorMix,
            },
          );
        } else
          Object.assign(res.viewDay,
            {
              backgroundColor: colorMix,
            },
          );
        Object.assign(res.btnDay,
          {
            backgroundColor: colorMix,
          },
        )
        Object.assign(res.textDay,
          { color: colors.colorBlack }
        )
      }
      ///////////////////////////Truong hop bang 1
      if (id == 1) {

        Object.assign(res.viewDay,
          {
            backgroundColor: colorMix,
          },
        );
        Object.assign(res.btnDay,
          {
            backgroundColor: colorMix,
          },
        )
        Object.assign(res.textDay,
          { color: colors.colorBlack }
        )
      };
      ///////////////////////////Truong hop bang 2

      if (id == 1) {
        res.viewDay.borderTopLeftRadius = 0
        res.viewDay.borderBottomLeftRadius = 0
        res.viewDay.borderTopRightRadius = 0
        res.viewDay.borderBottomRightRadius = 0
      }
    }
    return res;
  }