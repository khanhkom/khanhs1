import React, { Component } from 'react'
import {
  Text, View, TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  onButtonRight,onButtonLeft,
  onButtonDay,onDoneJob,onToday,
  onNeedDoneJob,outColorForDay} from './functionForSortJob';
import {addMail,addEvent} from '../../actions/actionCreators';
import styles from './styles';
//import common
import ViewMonth from './ViewMonth';
import Header from '../../common/Header';
import ViewDay from './ViewDay';
import ListEvent from './ListEvent';
import TabBottom from './TabBottom';
import { LoadingComponent } from '../../common/LoadingComponet';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
type Props = {
  title: string,
}

var self;
export class SortJob extends Component<Props> {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      month: 1,
      year: 2018,
      day: 1,
      thu: 2,
      selected: 0,
      dataDay: [],
      eventsByDay: [],
      onSelected: [],
      arrEventLocal:[],
      isLoading:false
    }
  }
  async componentWillMount() {
      self.setState({arrEventLocal: this.props.DataEvent});
    await this.onToday();
  }
  onChangeLoading=(val)=>{
      this.setState({isLoading:val})
  }
  onToday=async ()=>{
    await onToday(self);
  }
  onButtonRight=async ()=>{
    await onButtonRight(self);
  }
  onButtonLeft=async()=>{
    await onButtonLeft(self);
  }
  onDoneJob=async()=>{
    this.props.navigation.navigate("Calendar",{title:"Lịch thường kỳ"});
    // await onDoneJob(self);
  }
  onNeedDoneJob=async()=>{
    this.props.navigation.navigate("Calendar",{title:"Lịch khác"});
    // await onNeedDoneJob(self);
  }
  addJob() {
    self.props.navigation.navigate('AddEvent')
  }
  
  render() {
    const { title } = this.props;
    return (
      <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity 
            onPress={()=>this.props.navigation.goBack()}
            style={styles.cntBack}>
              <Ionicons name="ios-arrow-back" size={20 * STYLES.heightScreen / 640} color={Colors.lightBlack} />
              <Text style={styles.textHeader}>{"Trở về"}</Text>
        </TouchableOpacity>
        <TouchableOpacity
            onPress={()=>this.props.navigation.navigate("AddEvent")}
            style={{flex:0}}>
          <Ionicons name="ios-add-circle-outline" size={26 * STYLES.heightScreen / 640} color={Colors.lightBlack} />  
       </TouchableOpacity>
      </View>
       
        <View style={styles.cntDinalog}>
        <ViewMonth
          onButtonLeft={this.onButtonLeft}
          onButtonRight={this.onButtonRight}
          title={'Tháng ' + this.state.month}
        />
        <ViewDay />
        <View style={styles.cntDay}>
          {

            this.state.dataDay.map((value, index) => {
              let checkEvent = 'white';
              let res = {
                viewDay: {},
                btnDay: {},
                textDay: {}
              }
              if (value != '') {
                res = outColorForDay(self,value);
              }
              let ba = this.state.selected == index ? { backgroundColor: 'black' } : {};
              let colorText = this.state.selected == index ? { color: 'white' } : {};
              let currentDay = new Date();
              if (currentDay.getMonth() + 1 == self.state.month
                && currentDay.getDate() == value
              ) {
                ba = { backgroundColor: 'red' }
                colorText = { color: 'white' };
              }
              return (
                <View style={[styles.viewDay, res.viewDay]}
                  key={index}
                >
                  <TouchableOpacity
                    style={[styles.btnDay, res.btnDay, ba]}
                    onPress={() => onButtonDay(self,index, value)}
                  >
                    <Text style={[styles.text, { fontWeight: '400' }, res.textDay, colorText]}>
                      {value}
                    </Text>
                  </TouchableOpacity>
                </View>
              )
            })
          }
        </View>
        </View>
        <ListEvent
          data={this.state.onSelected}
          navigation={this.props.navigation}
        />
        <TabBottom
          onButtonLeft={this.onDoneJob}
          contentLeft={"Lịch định kỳ"}
          contentRight={"Lịch khác"}
          onButtonRight={this.onNeedDoneJob}
        >
        </TabBottom>
        <LoadingComponent isLoading={this.state.isLoading}></LoadingComponent>
      </View>
    )
  }
}

function mapStateToProps(state) {
  return { 
      MailBox: state.rootReducer.MailBox, 
      DataEvent:state.rootReducer.DataEvent,
      Account: state.rootReducer.Account
  };
}

export default connect(mapStateToProps, {
  addMail,addEvent
})(SortJob);