
import { AsyncStorage } from 'react-native'
import { mapToYearMonthDay,mapToStanđardFormat,calDayStandard,formatDateForServer } from '../../config/Function';
import {
   ADD_EVENT,NGUOI_DUOC_MOI,LAP_LAI,NHAC_LICH,
   MAIL_BOX,DATA_FORMAT_MONTH
} from '../../config/default';
import { FetchJson, CreateEvent } from '../../servers/serivce.config'
export const addOneEvent = async (self,theme,location,begin,end,remind,note,repeat,frequency) => {
    ///Khoi tao
    let {arrEvent,arrMailBox,Account} = self.props
    if (arrEvent == undefined) arrEvent = [];
    if (arrMailBox == undefined) arrMailBox = [];
    let timeBD = mapToYearMonthDay(begin);
    let timeKT = mapToYearMonthDay(end);
    let timeRemind='';
    if (remind.format=='Phút'){
      if (timeBD.minute-remind.id >= 0) 
      timeRemind=timeBD.year+"-"+mapToStanđardFormat(timeBD.month)+"-"
      +mapToStanđardFormat(timeBD.day)+" "+(timeBD.hour+":"+(timeBD.minute-remind.id));
      else {
        if (timeBD.hour > 0){
          timeRemind=timeBD.year+"-"+mapToStanđardFormat(timeBD.month)+"-"
          +mapToStanđardFormat(timeBD.day)+" "+((timeBD.hour-1)+":"+(60+timeBD.minute-remind.id));
        }
        else {
            if (timeBD.day > 1) {
              timeRemind=timeBD.year+"-"+mapToStanđardFormat(timeBD.month)+"-"
              +mapToStanđardFormat(timeBD.day-1)+" "+(23+":"+(60+timeBD.minute-remind.id));
            }
            else {
              if (timeBD.month > 1) {
                timeRemind=timeBD.year+"-"+mapToStanđardFormat(timeBD.month-1)+"-"
                +mapToStanđardFormat(DATA_FORMAT_MONTH[timeBD.month-1])+" "+(23+":"+(60+timeBD.minute-remind.id));
              }
              else {
                timeRemind='';
              }
            }
        }
        // else timeRemind='';
      }
  }
    if (remind.format=='Ngày'){
        if (timeBD.day-remind.id > 0) timeRemind=timeBD.year+"-"+mapToStanđardFormat(timeBD.month)+"-"+mapToStanđardFormat(timeBD.day-remind.id)+" "+(timeBD.time);
        else {
          if (timeBD.month > 1){
            timeRemind=timeBD.year+"-"+mapToStanđardFormat(timeBD.month-1)+"-"+mapToStanđardFormat(DATA_FORMAT_MONTH[timeBD.month-1]-(remind.id-timeBD.day))+" "+(timeBD.time);
          }
          else timeRemind='';
        }
    }
    if (remind.format=='Tháng'){
      if (timeBD.month-remind.id > 0) timeRemind=timeBD.year+"-"+mapToStanđardFormat(timeBD.month-remind.id)+"-"+mapToStanđardFormat(timeBD.day)+" "+(timeBD.time);
      else {
        timeRemind='';
      }
  }
  let json={
      createuserid:Account.LoginUserID,
      sendto:Account.LoginUserID,///se check de gui nhieu nguoi
      title: theme,
      content:note,
      onday:0,//hoi lai
      startdatetime:formatDateForServer(begin),
      endatetime:formatDateForServer(end),
      reminder:timeRemind=='' ? 0 :1,
      reminderdatetime:formatDateForServer(timeRemind),
      repeat:repeat.id==-1 ? 0 : 1,
      repeatnumber:parseInt(frequency),
      repeatdatetime:formatDateForServer(begin),
      note:location,
      priority:1,//se them sau
      newsid:1,//id bai
      eventid:0,//id su kien
  }
  await FetchJson(CreateEvent, json)
		.then((response) => {
            res= JSON.parse(response.d);
            console.log("====>>>> xem gui len server duoc chua>",res);
            if (res.status==true){
              let event = {
                idmessage: res.eventid,
                sender: Account.LoginUserID,
                title: theme,
                timeStart: timeBD.time,
                dayStart: timeBD.day,
                monthStart: timeBD.month,
                yearStart: timeBD.year,
                timeEnd: timeKT.time,
                dayEnd: timeKT.day,
                monthEnd: timeKT.month,
                yearEnd: timeKT.year,
                location: location,
                time: begin,
                reminder: timeRemind=='' ? 0 :1,
                timereminder: timeRemind,
                repeat: repeat.id==-1 ? 0 : 1,
                repeatnumber: frequency,
                timerepeat: begin,
                seen: false,
                seennote: 'fix seen',
                details: note,
                priority: 1,
                newsid: 1,
              };
              self.notif.scheduleNotif(timeRemind,{
                title:theme,
                details:note,
              });
              // arrEvent.push(event);
              // arrMailBox.unshift(event)
              // AsyncStorage.multiSet([
              //     [ADD_EVENT, JSON.stringify(arrEvent)],
              //     [MAIL_BOX, JSON.stringify(arrMailBox)],
              //   ]);
              // self.props.addMail(arrMailBox);
              // self.props.addEvent(arrEvent);
            }
    });
  //tao lich de thong bao
  }
  export const onSave = async (self) => {
    const {repeat,remind,frequency,theme,location,timeBegin,timeEnd,note} = self.state;
    addOneEvent(self,theme,location,timeBegin,timeEnd,remind,note,repeat,frequency);
    let timeBD = mapToYearMonthDay(timeBegin);
    let timeKT = mapToYearMonthDay(timeEnd);
    for (let idFrequency=1; idFrequency < parseInt(frequency) ; idFrequency++){
       let timeStart='';
       let timeEnd='';
       if (repeat.format=='Tháng'){
          timeStart= timeBD.year+"-"+mapToStanđardFormat(Math.min(timeBD.month+repeat.id*idFrequency,12))+"-"+mapToStanđardFormat(timeBD.day)+" "+timeBD.time;
          timeEnd= timeKT.year+"-"+mapToStanđardFormat(Math.min(timeKT.month+repeat.id*idFrequency,12))+"-"+mapToStanđardFormat(timeKT.day)+" "+timeKT.time;
          if (timeBD.month+repeat.id*idFrequency <= 12) 
          {
              if (DATA_FORMAT_MONTH[timeBD.month+repeat.id*idFrequency] >= timeBD.day ) 
              addOneEvent(self,theme,location,timeStart,timeEnd,remind,note,repeat,frequency);
          }     
       }
       if (repeat.format=='Ngày'){
            addOneEvent(self,theme,location,calDayStandard(timeBD,timeBD.day+repeat.id*idFrequency),
            calDayStandard(timeKT,timeKT.day+repeat.id*idFrequency),remind,note,repeat,frequency); 
     }
    }
    self.props.navigation.navigate("SortJob");
  }
  export const outLineEnd = (self,id) => {
    if (id == 0) return {
      title: NGUOI_DUOC_MOI,
      stateInput: self.state.invitePeople.content,
      onChange: self.changeInvitePeople
    }
    if (id == 2) return {
      title: LAP_LAI,
      stateInput: self.state.repeat.content,
      onChange: self.changeRepeat,
    }
    if (id == 1) return {
      title: NHAC_LICH,
      stateInput: self.state.remind.content,
      onChange: self.changeRemind,
    }
  }
  export const onChangeTimeBegin=(self,val) => {
    let time = mapToYearMonthDay(val);
    if (self.state.fullDay) {
      self.setState({ timeEnd: (time.year + '-' + time.month + '-' + time.day + " 23:59") });
    }
    self.setState({ timeBegin: val });
  }
  export const onChangeTimeEnd=(self,val)=> {
    let time = mapToYearMonthDay(val);
    if (self.state.fullDay) {
      self.setState({ timeBegin: (time.year + '-' + time.month + '-' + time.day + " 00:00") });
    }
    self.setState({ timeEnd: val });
  }