import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, Alert
} from 'react-native'
import { BoxShadow } from 'react-native-shadow'
//import
import STYLES from '../../config/styles.config';
import styles from './styles'
import colors from '../../config/Colors';
import { connect } from 'react-redux'
import image from '../../assets/image';
import { SearchBar, Avatar } from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import Colors from '../../config/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
type Props = {
  content: string,
  onButton: Function,
}
var DATA = [
  {
    StaffID: 100,
    LoginUserID: "manh.tq",
    StaffName: "Trần Quang Mạnh",
    ImageFile: image.iconuser1
  },
  {
    StaffID: 100,
    LoginUserID: "anh.dv",
    StaffName: "Đinh Văn Anh",
    ImageFile: image.iconuser2
  },
  {
    StaffID: 100,
    LoginUserID: "linh.dv",
    StaffName: "Đinh Văn Linh",
    ImageFile: image.iconuser3
  },
  {
    StaffID: 100,
    LoginUserID: "khoa.mv",
    StaffName: "Mạc Văn Khoa",
    ImageFile: image.iconuser3
  },
  {
    StaffID: 100,
    LoginUserID: "khoa.vanhoang",
    StaffName: "Hoàng Văn Khoa",
    ImageFile: image.iconuser4
  },
  {
    StaffID: 100,
    LoginUserID: "anh.pv",
    StaffName: "Pham Van Anh",
    ImageFile: image.iconuser1
  },
  {
    StaffID: 100,
    LoginUserID: "linh.pt",
    StaffName: "Pham Thi Linh",
    ImageFile: image.iconuser4
  },
  {
    StaffID: 100,
    LoginUserID: "dinh.ph",
    StaffName: "Phan Hoang Dinh",
    ImageFile: image.iconuser1
  },

];

class PeopleInvited extends Component {
  constructor(props) {
    super(props)
    this.state = {
      theme: '',
      search: '',
      checked: this.onInit(),
      countNotAbsent: 0,
      Filterchoice: false,
      Swapchoice: true,
      Moreverticalchoice: false
    }
  }
  sendList = () => {
    const { countNotAbsent, checked } = this.state;
    let res = "Tổng số" + countNotAbsent + "/" + checked.length + " đồng chí.Bạn có muốn gửi kết quả không?";
    Alert.alert(
      'Thông báo',
      res,
      [
        { text: 'Huỷ', onPress: () => { }, style: 'cancel' },
        {
          text: 'Gửi kết quả', onPress: () => {
            this.props.navigation.goBack();
            Alert.alert('Bạn đã gửi danh sách thành công')
          }
        },
      ],
      { cancelable: false }
    )

  }
  onInit = () => {
    let arr = [];
    DATA.map((value, index) => {
      arr.push({
        LoginUserID: value.LoginUserID,
        present: false,
      });
    })
    return arr;
  }
  updateSearch = search => {
    this.setState({ search });
  };
  changeCheck = (id) => {
    let checked = this.state.checked;
    let countNotAbsent = checked[id].present ? this.state.countNotAbsent - 1 : this.state.countNotAbsent + 1;
    checked[id].present = !checked[id].present;
    this.setState({ checked, countNotAbsent })
  }
  render() {
    const { search } = this.state;
    return (
      <View style={styles.cnt}>
        <HeaderReal
          title={'Người được mời'}
          onButton={() => this.props.navigation.navigate('SortJob')}
          linkImage={'check'}
          colorImage={'#FFFFFF'}
          onButtonRight={this.sendList}
        />
        <View style={styles.cntSearch}>
          <View style={styles.cntTab}>
            <View>
              <Text style={styles.textSl}>Số lượng:{this.state.countNotAbsent}</Text>
            </View>
            <View></View>
            <View style={styles.cntFilter}>
              {/* <MaterialIcons name="swap-vert" size={24 * STYLES.heightScreen / 640} color={this.state.Swapchoice ? Colors.colorPink : '#B1B1B1'} />
              <Feather
                name="filter" size={22 * STYLES.heightScreen / 640} color={this.state.Filterchoice ? Colors.colorPink : '#B1B1B1'}
              />
              <Feather
                name="more-vertical" size={24 * STYLES.heightScreen / 640} color={this.state.Filterchoice ? Colors.colorPink : '#B1B1B1'}
              /> */}
            </View>
          </View>
          <View style={styles.cntsearch}>
            <Text style={styles.textSl}>TÌm kiếm</Text>
            <SearchBar
              placeholder="Nhập tên"
              onChangeText={this.updateSearch}
              value={search}
              containerStyle={{
                backgroundColor: Colors.white, width: 281 * STYLES.widthScreen / 360, paddingBottom:5 * STYLES.heightScreen / 640,paddingTop:5 * STYLES.heightScreen / 640 ,
                borderWidth: 1, borderRadius: 5, borderColor: Colors.white, alignItems: 'center'
              }}
              inputContainerStyle={{ backgroundColor: '#F4F4F4', borderRadius: 13}}
              inputStyle={{ fontSize: STYLES.fontSizeNormalText, fontFamily: 'Roboto' }}
            />
          </View>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ flex: 1, height: 200 }}
          contentContainerStyle={{

          }}
        >
          {
            DATA.map((value, index) => {
              return (
                <TouchableOpacity
                  onPress={() => this.changeCheck(index)}
                  style={styles.cntList}>
                  <View style={styles.CntAvatar}>
                    <Image source={value.ImageFile} style={styles.cntAvatarImg} />
                  </View>
                  <View style={styles.cntInfor}>
                    <View >
                      <View style={{ width: 140 * STYLES.widthScreen / 360 }}>
                        <Text style={styles.cntTextName}>{value.StaffName}</Text>
                      </View>
                      <View style={styles.cntFrameid}>
                        <Text style={{ fontFamily: 'Roboto', fontWeight: '500', fontSize: STYLES.fontSizeNormalText, color: '#1C1C1C' }}>{'Tài khoản: '}</Text>
                        {/* <Image source={image.iconuser} style={{ height: 13 * STYLES.heightScreen / 640, width: 13 * STYLES.heightScreen / 640 }} /> */}
                        <Text style={[styles.textSl, { marginLeft: 2 }]}>{value.LoginUserID}</Text>
                      </View>
                    </View>
                    <View style={{ width: 55 * STYLES.widthScreen / 360 }}>
                      <Text style={styles.textMini}>Bí thư chi bộ</Text>
                    </View>
                    <View style={styles.CntButton}>
                      <TouchableOpacity
                        onPress={() => this.changeCheck(index)}
                        style={{
                          borderRadius: 5, backgroundColor: this.state.checked[index].present ? '#1BD18F' : '#DA251D',
                          width: 65 * STYLES.widthScreen / 360, alignItems: 'center', paddingTop: 2 * STYLES.heightScreen / 640, paddingBottom: 2 * STYLES.heightScreen / 640
                        }}>
                        {
                          !this.state.checked[index].present ?
                            (<Text style={{ color: Colors.white, fontSize: STYLES.fontSizeNormalText }}>Chọn</Text>) : (<Text style={{ color: Colors.white, fontSize: STYLES.fontSizeNormalText }}>Đã chọn</Text>)}
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </View>

    )
  }
}

function mapStateToProps(state) {
  return {
    MailBox: state.rootReducer.MailBox,
    Account: state.rootReducer.Account,
    DataExercises: state.rootReducer.DataExercises,
  };
}

export default connect(mapStateToProps, {
})(PeopleInvited);
