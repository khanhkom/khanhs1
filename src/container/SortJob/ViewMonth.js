import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'

//import
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
type Props = {
  title:string,
  onButtonLeft:Function,
  onButtonRight:Function,
}
export default ViewMonth = (props: any) => {
    const {title,onButtonLeft,onButtonRight}= props;  
    return (
        <View style={styles.container}>
        <TouchableOpacity
                onPress={()=>onButtonLeft()}
                style={styles.menu}
        >
                <Ionicons name="ios-arrow-back" size={30 * STYLES.heightScreen / 640} color={colors.lightBlack} />
        </TouchableOpacity> 
        <Text style={styles.text}>
        {title}
        </Text>
        <TouchableOpacity
                onPress={()=>onButtonRight()}
                style={[styles.menu,{alignItems:'flex-end'}]}
        >
                <Ionicons name="ios-arrow-forward" size={30 * STYLES.heightScreen / 640} color={colors.lightBlack} />
        </TouchableOpacity> 
      </View>
    )
}
const styles = StyleSheet.create({
      container: {
        width:0.95*STYLES.widthScreen,
        height:55*STYLES.heightScreen/640,
        paddingHorizontal: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection:'row',
        borderBottomColor: colors.backGray,
        borderBottomWidth: 1,
      },
      text: {
        color: colors.lightBlack,
        fontWeight: 'bold',
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
      },
      menu:{
          flex:0,
          width:55 * STYLES.heightScreen / 640
      }
})

