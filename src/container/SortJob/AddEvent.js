import React, { Component } from 'react'
import {
  Text, View, 
  BackHandler, Switch,  ScrollView
} from 'react-native'
import { connect } from 'react-redux';


//import config
import { onSave,outLineEnd,onChangeTimeBegin,onChangeTimeEnd } from './functionForAddEvent';

//import common
import HeaderReal from '../../common/HeaderReal';
import ListData from './ListData';
import { addMail, addEvent } from '../../actions/actionCreators'
import NotifService from '../PushNotifications/NotifService';
import MainAddEvent from '../../common/MainAddEvent';

var self;
export class AddEvent extends Component<Props> {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      theme: '',
      location: '',
      fullDay: false,
      frequency: '3',
      timeBegin: '',
      timeEnd: '',
      note: '',
      onKeyBoard: 0,
      movingScreen: false,
      onDefault: true,
      repeat: { id: -1, content: 'Không', format: '' },
      remind: { id: -1, content: 'Không', format: '' },
      invitePeople: { id: -1, content: 'Không có' },
      data: [],
      id: 0,
    }
    this.notif = new NotifService(this.onRegister.bind(this), this.onNotif.bind(this));
  }
  onRegister(token) {
  }
  onNotif(notif) {
  }
  componentWillMount() {
    let dateCurrent = new Date();
    self.setState({
      timeBegin: dateCurrent.getFullYear() + '-' + (dateCurrent.getMonth() + 1) + '-'
        + dateCurrent.getDate() + ' 00:00',
      timeEnd: dateCurrent.getFullYear() + '-' + (dateCurrent.getMonth() + 1) + '-'
        + dateCurrent.getDate() + ' 23:59'
    })
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
  onChangeTimeBegin=(val)=>{
    onChangeTimeBegin(self,val);
  }
  onChangeTimeEn=(val)=>{
    onChangeTimeEnd(self,val);
  }
  handleBackPress = () => {
    this.props.navigation.navigate('SortJob');
    return true;
  }
  changeTheme(val) {
    self.setState({ theme: val })
  }
  changeLocation(val) {
    self.setState({ location: val })
  }
  onButtonSave = () => {
    onSave(self);
  }
  outLineEnd=(id)=>{
    return outLineEnd(self,id);
  }
  onChangeNote(val) {
    self.setState({ note: val });
  }
  changeInvitePeople = async (val) => {
    await self.setState({ invitePeople: val });
  }
  changeRemind = async (val) => {
    await self.setState({ remind: val });
  }
  changeRepeat = async (val) => {
    await self.setState({ repeat: val });
  }
  changeFrequency = async (val) => {
    await self.setState({ frequency: val });
  }
  changeMovingScreen = async () => {
    await self.setState({ movingScreen: false });
  }
  onButtonEnd = async (val, id) => {
    await this.setState({ data: val.data, id: id });
    await this.setState({ movingScreen: true });
  }
  render() {
    if (self.state.movingScreen === false)
      return (
        <View style={{ flex: 1 }}>
          <HeaderReal
            title={'Tạo sự kiện'}
            onButton={() => this.props.navigation.navigate('SortJob')}
            linkImage={'check'}
            colorImage={this.state.theme != '' ? '#7BDEFE' : 'white'}
            onButtonRight={this.onButtonSave}
          />
          {
            MainAddEvent(self)
          }
        </View>
      )
    else
      return (
        <ListData
          title={outLineEnd(self,this.state.id).title}
          onBack={this.changeMovingScreen}
          onChangeFrequency={this.changeFrequency}
          stateFrequency={this.state.frequency}
          data={this.state.data}
          stateInput={outLineEnd(self,this.state.id).stateInput}
          onChange={outLineEnd(self,this.state.id).onChange}
        />
      )
  }
}
function mapStateToProps(state) {
  return {
    MailBox: state.rootReducer.MailBox,
    DataEvent: state.rootReducer.DataEvent,
    Account:state.rootReducer.Account,
  };
}
export default connect(mapStateToProps, {
  addMail, addEvent
})(AddEvent);
