import {
  StyleSheet,
} from 'react-native'
//import
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.lightGrayEEE,
  },
  header: {
    width: STYLES.widthScreen,
    paddingVertical: 25 * STYLES.heightScreen / 896,
    paddingHorizontal: 15 * STYLES.widthScreen / 360,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: "row",
  },
  cntBack: {
    flex: 0,
    flexDirection: "row",
  },
  cntDinalog: {
    flex: 0,
    elevation: 5,
    marginTop: 10 * STYLES.heightScreen / 896,
    paddingVertical: 20 * STYLES.heightScreen / 896,
    backgroundColor: colors.white,
    borderRadius: 30 * STYLES.heightScreen / 896,
    width: 0.95 * STYLES.widthScreen,
  },
  textHeader: {
    marginLeft: 8 * STYLES.widthScreen / 360,
    color: colors.colorBlack,
    fontSize: STYLES.fontSizeText,
    fontFamily: 'Roboto-Regular',
    textAlign: "center",
    textAlignVertical: "center"
  },
  text: {
    color: colors.colorBlack,
    fontSize: STYLES.fontSizeText,
    fontFamily: 'Roboto-Regular',
    textAlign: "center",
    textAlignVertical: "center"
  },
  cntDay: {
    width: 0.95 * STYLES.widthScreen,
    flex: 0,
    paddingHorizontal: 15 * STYLES.widthScreen / 360,
    flexWrap: 'wrap',
    flexDirection: 'row',
    // borderBottomWidth: 1,
    // borderBottomColor: colors.backGray,
  },
  viewDay: {
    width: 44.5 * STYLES.widthScreen / 360,
    height: 35 * STYLES.heightScreen / 640,
    justifyContent: 'center',
    //paddingVertical: 3*STYLES.heightScreen/640,
    alignItems: 'center',
    // borderBottomLeftRadius:35*STYLES.heightScreen/640,
    // borderTopLeftRadius:35*STYLES.heightScreen/640,
    marginVertical: 5 * STYLES.heightScreen / 640,
    // backgroundColor:'red'
  },
  btnDay: {
    width: 35 * STYLES.heightScreen / 640,
    height: 35 * STYLES.heightScreen / 640,
    borderRadius: 35 * STYLES.heightScreen / 640,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkEvent: {
    width: 5 * STYLES.heightScreen / 640,
    height: 5 * STYLES.heightScreen / 640,
    borderRadius: 5 * STYLES.heightScreen / 640,
    backgroundColor: colors.grayNew,
  },
  //stylePeopleInvited
  cnt: {
    flex: 1,
    alignItems: 'center'
  },
  cntSearch: {
    width: STYLES.widthScreen * 342 / 360,
    paddingTop: 8 * STYLES.heightScreen / 640
  },
  cntTab: {
    width: STYLES.widthScreen * 342 / 360,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textSl: {
    fontSize: STYLES.fontSizeNormalText,
    fontFamily: 'Roboto',
    color: '#494949'
  },
  cntFilter: { width: STYLES.widthScreen * 95 / 360, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
  cntsearch: {
    width: STYLES.widthScreen * 342 / 360, flexDirection: 'row', alignItems: 'center',
    justifyContent: 'space-between', paddingLeft: 0, paddingBottom: 7 * STYLES.heightScreen / 360
  },
  cntList: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: STYLES.widthScreen * (340 / 360),
    borderRadius: 15,
    backgroundColor: '#FFFFFF',
    marginTop: 11 * STYLES.heightScreen / 640,
  },
  CntAvatar: {
    width: 51 * STYLES.heightScreen / 640, position: 'absolute',
    height: 52.48 * STYLES.heightScreen / 640, left: 0 * STYLES.heightScreen / 640, elevation: 2
  },
  cntAvatarImg: { width: 51 * STYLES.heightScreen / 640, height: 52.48 * STYLES.heightScreen / 640, },
  cntInfor: {
    width: 305 * STYLES.widthScreen / 360, flexDirection: 'row', justifyContent: 'space-between',
    paddingLeft: 24 * STYLES.widthScreen / 360, paddingRight: 14 * STYLES.widthScreen / 360,
    paddingTop: 4 * STYLES.heightScreen / 640, paddingBottom: 14 * STYLES.heightScreen / 640,
    backgroundColor: '#F4F4F4', borderRadius: 13, elevation: 1, marginLeft: 35 * STYLES.widthScreen / 360
  },
  cntTextName: { fontWeight: '500', fontFamily: 'Roboto', fontSize: STYLES.fontSizeLabel, color: '#1C1C1C' },
  cntFrameid: { flexDirection: 'row', alignItems: 'center', width: 144 * STYLES.widthScreen / 360 },
  textMini: { fontSize: STYLES.fontSizeMiniText, color: '#494949', fontStyle: 'italic', fontFamily: 'Roboto', marginTop: 5 },
  CntButton: { width: 65 * STYLES.widthScreen / 360, alignItems: 'center', justifyContent: 'center', },
  cntButton: {
   
  }
})

