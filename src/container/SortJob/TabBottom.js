import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'

//import
import STYLES from '../../config/styles.config';
import {DA_THUC_HIEN,HOM_NAY,CAN_THUC_HIEN} from '../../config/default';
import colors from '../../config/Colors';
type Props = {
  onButtonLeft:Function,
  onButtonMid:Function,
  onButtonRight:Function,
  contentLeft:any,
  contentRight:any
}
export default TabBottom = (props: any) => {
    const{onButtonLeft,onButtonRight,contentLeft,contentRight}= props;
    return (
        <View style={styles.container}>
                <TouchableOpacity 
                        onPress={() => onButtonLeft() }
                        style={styles.btnLeft}
                >
                        <Text style={styles.text}>
                                {contentLeft}
                        </Text>
                </TouchableOpacity>
                {/* <TouchableOpacity 
                        onPress={() => onButtonMid() }
                        style={styles.btnLeft}
                >
                        <Text style={styles.text}>
                                {HOM_NAY}
                        </Text>
                </TouchableOpacity> */}
                <TouchableOpacity 
                        onPress={() => onButtonRight() }
                        style={styles.btnLeft}
                >
                        <Text style={styles.text}>
                                {contentRight}
                        </Text>
                </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
      container: {
        width:STYLES.widthScreen,
        height:(50 * STYLES.heightScreen / 830),
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: colors.colorPink,
      },
      text: {
        color: colors.white,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
      },
      btnLeft:{
          flex:0,
          height:(50 * STYLES.heightScreen / 830),
          width:180*STYLES.widthScreen/360,
          justifyContent: 'center',
          alignItems: 'center',
      }
})

