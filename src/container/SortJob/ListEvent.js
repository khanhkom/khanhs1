import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity, ScrollView
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ItemEvent from '../../common/ItemEvent';
type Props = {
    data: any,
    navigation: Function,
}
var dataImage = [IMAGE.bookmac, IMAGE.bookmac1, IMAGE.lich1];
export default class ListEvent extends Component<Props> {
    onButton(linkImage, id, value) {
        const { navigation, data } = this.props;
        navigation.navigate('DetailsForJob', { linkImage: linkImage, id: id + 1 + '/' + data.length, data: value })
    }
    render() {

        return (
            <ScrollView 
                showsVerticalScrollIndicator={false}
                style={styles.container}>
                <View style={styles.ctnScroll}>
                    {
                        this.props.data.map((value, index) => {
                            let time = value.dayStart != value.dayEnd ? value.dayStart + '-' + value.dayEnd : value.dayStart;
                            return (
                                <ItemEvent
                                    key={index}
                                    backGround={'#fff'}
                                    title={value.title}
                                    time={time}
                                    month={value.monthEnd}
                                    index={index}
                                    onButton={() => this.onButton(dataImage[index % 3], index, value)}
                                />
                            )
                        })
                    }
                </View>

            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    ctnScroll: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:5*STYLES.heightScreen/896,
        marginBottom:15*STYLES.heightScreen/896,
    },
    text: {
        color: colors.lightBlack,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        textAlignVertical: "center"
    },
    viewsChung: {
        flex: 0,
        height: 60 * STYLES.heightScreen / 640,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewDay: {
        marginTop: 10 * STYLES.heightScreen / 640,
        height: 60 * STYLES.heightScreen / 640,
        width: STYLES.widthScreen * 0.95,
        paddingHorizontal: 10 * STYLES.widthScreen / 360,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        borderWidth: 0.8,
        borderColor: '#E5E5E5',
        borderRadius: 8 * STYLES.widthScreen / 360,
        shadowColor: colors.colorGray,
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 3,
        shadowRadius: 2,
        elevation: 1,
    },
    viewImage: {
        height: 45 * STYLES.heightScreen / 640,
        width: 45 * STYLES.heightScreen / 640,
        justifyContent: 'center',
        marginRight: 15 * STYLES.widthScreen / 360,
    },
    imageMain: {
        height: 45 * STYLES.heightScreen / 640,
        width: 45 * STYLES.heightScreen / 640,
    },
    viewTime: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTitle: {
        flex: 0,
        height: 40 * STYLES.heightScreen / 640,
        justifyContent: 'space-between',
    }
})

