import React from 'react'
import {
  Text, View, StyleSheet
} from 'react-native'

//import
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';
import {LIST_DAY} from '../../config/default';
type Props = {
  
}
export default ViewDay = (props: any) => {
    return (
        <View style={styles.container}>
            {
                LIST_DAY.map((value,index)=>{
                    return (
                        <View 
                        key={index}
                        style={styles.viewDay}>
                            <Text style={styles.text}>{value}</Text>
                        </View>
                    )
                })
            }
        </View>
    )
}
const styles = StyleSheet.create({
      container: {
        width:0.95*STYLES.widthScreen,
        height:35*STYLES.heightScreen/640,
        paddingHorizontal: 15*STYLES.widthScreen/360,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection:'row',
        borderBottomColor: colors.backGray,
        borderBottomWidth: 1,
      },
      text: {
        color: colors.lightBlack,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        textAlign: "center",
        fontWeight:'500',
        textAlignVertical: "center"
      },
      viewDay:{
          flex:0,
          width:44.5*STYLES.widthScreen/360,
      }
})

