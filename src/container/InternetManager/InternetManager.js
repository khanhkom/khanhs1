import { Alert, Platform, NetInfo } from 'react-native'
class InternetManager {

  isShowPopupNetworkChecker: boolean;
  constructor() {
    this.isShowPopupNetworkChecker = false;
  }

  default() {
    this.isShowPopupNetworkChecker = false;
  }

  networkChecker(callBack?: (result) => void) {
    if (!this.isShowPopupNetworkChecker) {
      this.isShowPopupNetworkChecker = true;
      if (Platform.OS === 'ios') {
        callBack(true)
        // NetInfo.isConnected.addEventListener('connectionChange', (isConnected: boolean) => this.handleConnectivityChange(isConnected, callBack));
      } else {
        NetInfo.isConnected.fetch().then((isConnected: boolean) => {
          callBack(isConnected);
        })
      }
    }
  }
  handleConnectivityChange = (isConnected: boolean, callBack?: (result: boolean) => void) => {
    callBack(isConnected);
    NetInfo.isConnected.removeEventListener('connectionChange', (isConnected: boolean) => this.handleConnectivityChange(isConnected, callBack));
  }
  notificationEnableNetwork() {
    Alert.alert(
      'Internet',
      'Kết nối Internet để tiếp tục ứng dụng.',
      [
        { text: 'Cancel', onPress: () => { this.isShowPopupNetworkChecker = false }, style: 'cancel' },
        { text: 'OK', onPress: () => { this.isShowPopupNetworkChecker = false } },
      ],
      { cancelable: false }
    )
  }
}

const store = new InternetManager();
export default store;