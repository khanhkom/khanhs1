import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity,WebView,ScrollView,Alert
} from 'react-native'
import Loading from 'react-native-spinkit';

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import {TIN_TUC_SU_KIEN} from '../../config/default';
import Header from '../../common/Header';
import PrintNew from '../PrintNew'
//data
import data from '../data/dataHome.json';
import dataBonus from '../data/dataNew.json';
import dataDonVi from '../data/dataDonvi.json';

import {LoadingComponent} from '../../common/LoadingComponet';

type Props = {
  
}
var self;
export default class TrangChu extends Component<Props> {
  constructor(props){
    super(props);
    self=this;
    this.state={
      isLoading: true,
      stateData:data,
      stateDataBonus:dataBonus,
    }
  }
  componentWillMount()
  {
    clearTimeout(this.interval);
    this.interval = setTimeout(() => {
      self.setState({isLoading: false });
    }, 1000);
  }
  onButton = (id) =>{
    if (id == 0){
      this.setState({stateData:data,stateDataBonus:dataDonVi});
    }
    if (id == 1){
      this.setState({stateData:dataBonus,stateDataBonus:dataDonVi});
    }
    if (id == 2){
      this.setState({stateData:dataDonVi,stateDataBonus:data});
    }
  }
  onButtonSearch(){
  }
  render() {
   const {stateData,stateDataBonus}= this.state;
    return (
      <View style={styles.container}>
           <Header 
                title={TIN_TUC_SU_KIEN}  
                linkImage={'search'}
                onButton={this.onButtonSearch}
                navigation={this.props.navigation} />
           <PrintNew
              data={stateData}
              onButton={this.onButton}
              dataBonus={stateDataBonus}
              navigation={this.props.navigation}
           />
            <LoadingComponent isLoading={this.state.isLoading}></LoadingComponent>
      </View>  
  )
  }
}
const styles = StyleSheet.create({
      container: {
        flex:1,
        // justifyContent:'center',
        // alignItems:'center',
      },
      line:{
          height:5,
          width:STYLES.widthScreen,
          backgroundColor: '#dddddd',
      },
      loadingContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
})

