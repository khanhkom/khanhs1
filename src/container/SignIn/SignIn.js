import React, { Component } from 'react'
import {
  Text, View, Image, TouchableOpacity,StatusBar,
  TextInput, Keyboard, TouchableWithoutFeedback, Alert, AsyncStorage
} from 'react-native'
import { connect } from 'react-redux';

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import styles from './styles';
import dataExercises from '../data/ListThematic';
import { addMail, addEvent,setAccount,setLesson } from '../../actions/actionCreators';
import { INIT_STORGE, MAIL_BOX, DATA_MAIL_BOX, BO_QUA, DANG_NHAP, SET_LESSSON } from '../../config/default';
import { LOGIN_SERVICE, FetchJson, GetEventsByTime, GetStaffByKey } from '../../servers/serivce.config';
import Skip from '../../common/Skip';
import ButtonSignIn from '../../common/ButtonSignIn';
import FrameSignIn from '../../common/FrameSignIn';
import Colors from '../../config/Colors';

type Props = {
  title: string,
}
var self;
export class SignIn extends Component<Props> {
  constructor(props) {
    super(props);
    self=this;
    this.state = {
      username: '',
      password: '',
      bottom: 267 * STYLES.heightScreen / 640,
      heightImage: 181 * STYLES.heightScreen / 640,
    }
  }
  componentWillMount() {
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow);
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidHide', this._keyboardWillHide);
  }
  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillShowListener.remove();
  }
  _keyboardWillShow = (event) => {
    this.setState({
      bottom: 190 * STYLES.heightScreen / 640,
      heightImage: 80 * STYLES.heightScreen / 640
    });
  };

  _keyboardWillHide = (event) => {
    this.setState({
      bottom: 267 * STYLES.heightScreen / 640,
      heightImage: 181 * STYLES.heightScreen / 640
    });

  };
  onChangeUsername = (value) => {
    this.setState({ username: value });
  }
  onChangePassword = (value) => {
    this.setState({ password: value });
  }
  skip = () => {
    const { dispatch } = this.props.navigation;
    dispatch({
      type: 'Navigation/RESET',
      actions: [{
        type: 'Navigate',
        routeName: 'Home'
      }], index: 0
    });
  }
  onButton = async () => {
    const { dispatch } = this.props.navigation;
    var account = {
      userID: this.state.username.toLowerCase(),
      password: this.state.password,
    }
    await FetchJson(LOGIN_SERVICE, account).then(async (response: any) => {
      if (JSON.parse(response.d).status) {
        let json = {
          userID: account.userID,
          keysearch: account.userID,
          size: 1,
          pageindex: 1,
        }
        await FetchJson(GetStaffByKey, json)
          .then(async (response) => {
            self.props.setAccount(JSON.parse(response.d).rows[0]);
            await this.props.addMail([]);
            await this.props.addEvent([]);
            self.props.setLesson(dataExercises);
            await AsyncStorage.multiSet([
              [INIT_STORGE, JSON.stringify(JSON.parse(response.d).rows[0])],
              [SET_LESSSON, JSON.stringify(dataExercises)],
            ]);
            await dispatch({
              type: 'Navigation/RESET',
              actions: [{
                type: 'Navigate',
                routeName: 'Home'
              }], index: 0
            });
          })
      } else {
        Alert.alert(
          'Thông báo',
          'Sai tài khoản hoặc mật khẩu',
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )
      }
    })
  }
  render() {
    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }} onPress={() => Keyboard.dismiss()}>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.colorPink} />
          <View style={[styles.viewOfimage]}>
            <Image
              resizeMode={'contain'}
              source={IMAGE.ptitLogo}
              style={[styles.image, { height: this.state.heightImage }]}
            />
          </View>
          <FrameSignIn
            postion={this.state.bottom}
            password={this.state.password}
            username={this.state.username}
            onChangeUsername={this.onChangeUsername}
            onChangePassword={this.onChangePassword}
          />
          <ButtonSignIn
            title={DANG_NHAP}
            onButton={this.onButton}
          />
          <Skip
            title={BO_QUA}
            onButton={this.skip}
          />
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
function mapStateToProps(state) {
  return {
    MailBox: state.rootReducer.MailBox,

  };
}

export default connect(mapStateToProps, {
  addMail, addEvent,setAccount,setLesson
})(SignIn);
