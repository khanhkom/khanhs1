import { StyleSheet } from 'react-native';
//configs
import STYLES from '../../config/styles.config';
import colors from '../../config/Colors';
export default styles = StyleSheet.create({
    container: {
      flex:1,
    },
    text: {
      color: 'black',
      fontSize: STYLES.fontSizeText,
      fontFamily: 'Roboto-Regular',
  },
  image:{
    width:238*STYLES.widthScreen/360,
        height:181*STYLES.heightScreen/640,
  },
    viewOfimage:{
        width:STYLES.widthScreen,
    height:313*STYLES.heightScreen/640,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:colors.colorPink,
    shadowColor: '#E5E5E5',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 3,
    shadowRadius: 2,
    elevation: 2,
  }
})

