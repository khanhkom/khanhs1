
'use strict';
import { Image, StyleSheet, StatusBar, View, Text,AsyncStorage } from 'react-native';
import * as React from 'react';
import * as Animatable from 'react-native-animatable';
import { connect } from 'react-redux';
//import config

import {INIT_STORGE, MAIL_BOX, ADD_EVENT,SET_LESSSON} from '../../config/default';
import IMAGE from '../../assets/image'
import styles from './styles';
import { addMail,addEvent,setAccount,setLesson } from '../../actions/actionCreators';
import Colors from '../../config/Colors';

export class Intro extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.changeScreen = this.changeScreen.bind(this);
  }
  async componentWillMount(){
    let routeName='SignIn';
    await AsyncStorage.multiGet([
      INIT_STORGE,MAIL_BOX,ADD_EVENT,SET_LESSSON
  ], async (err, results) => {
      let account=JSON.parse(results[0][1]);
      if (account!=undefined){
        routeName='Home';
        await this.props.setAccount(account);
        await this.props.addMail(JSON.parse(results[1][1]));
        await this.props.addEvent(JSON.parse(results[2][1]));
        await this.props.setLesson(JSON.parse(results[3][1]));
      }
      else {
        await this.props.setAccount(undefined);
        await this.props.addMail([]);
        await this.props.addEvent([]);
        await this.props.setLesson([]);
      }
  });
    this.changeScreen(routeName);
  }
  changeScreen(routeName) {
    const { dispatch } = this.props.navigation;
      setTimeout(() => {
        dispatch({
          type: 'Navigation/RESET',
          actions: [{
              type: 'Navigate',
              routeName: routeName
          }], index: 0
      });
      }, 3500);
  }
  render() {
    return (
      <View style={styles.container}>
         <StatusBar backgroundColor={Colors.colorPink} />
        <Animatable.View
          animation="bounceIn"
          direction="alternate"
          duration={4000}
          style={styles.logoContainer}>
          <Image
            resizeMode={'contain'}
            source={IMAGE.ptitLogo}
            style={styles.image}
          />
        </Animatable.View>
        {/* <Animatable.View
          animation="bounceInLeft"
          direction="alternate"
          duration={3500}
          style={styles.label}>
          <Text style={styles.textLabel}>{'Tỉnh Thái Bình'}</Text>
        </Animatable.View> */}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return { 
    MailBox: state.rootReducer.MailBox ,
    DataEvent: state.rootReducer.DataEvent,
  };
}

export default connect(mapStateToProps, {
  addMail,addEvent,setAccount,setLesson
})(Intro);

