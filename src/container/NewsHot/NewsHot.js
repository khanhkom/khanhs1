/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView
} from 'react-native'
import { WebView } from 'react-native';
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { TIN_HOT } from '../../config/default';

//import common

import HeaderReal from '../../common/HeaderReal';

class NewsHot extends Component<Props> {
    render() {
        return (
            <View style={{flex:1}}>
                <HeaderReal
                    title={TIN_HOT}
                    onButton={() => this.props.navigation.goBack()}
                />
                <WebView
                    source={{ uri: 'http://34.73.92.252' }}
                    style={{ marginTop: 20 }}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(NewsHot);